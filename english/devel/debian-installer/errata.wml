#use wml::debian::template title="Debian-Installer errata"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Errata for <humanversion /></h1>

<p>
This is a list of known problems in the <humanversion /> release of the Debian
Installer. If you do not see your problem listed here, please send us an
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installation
report</a> describing the problem.
</p>

<dl class="gloss">
     <dt>Theme used in the installer</dt>
     <dd>There's no Bookworm artwork yet, and the installer is still
     using the Bullseye theme.
     <br />
     <b>Status:</b> Fully fixed in Bookworm RC 1.</dd>

     <dt>Firmwares required for some sound cards</dt>
     <dd>There seems to be a number of sound cards that require loading a
     firmware to be able to emit sound
     (<a href="https://bugs.debian.org/992699">#992699</a>).
     <br />
     <b>Status:</b> Fixed in Bookworm Alpha 1.</dd>

     <dt>Encrypted LVM might fail on low memory systems</dt>
     <dd>It's possible for systems with low memory (e.g. 1 GB) to encounter a
     failure to set up encrypted LVM: cryptsetup might trigger the out-of-memory
     killer when formatting the LUKS partition
     (<a href="https://bugs.debian.org/1028250">#1028250</a>,
     <a href="https://gitlab.com/cryptsetup/cryptsetup/-/issues/802">cryptsetup upstream</a>).
     <br />
     <b>Status:</b> Fixed in Bookworm RC 2.</dd>

     <dt>Broken “Guided - use entire disk and set up LVM” partitioning in UEFI mode</dt>
     <dd>Using this kind of guided partitioning results in a “Force
     UEFI installation?” prompt (even if there are no other operating
     systems), defaulting to “No”
     (<a href="https://bugs.debian.org/1033913">#1033913</a>).
     Sticking to the default answer is very likely to result in a
     non-booting installation if Secure Boot is enabled. Switching to
     “Yes” should avoid running into this issue.
     <br />
     <b>Status:</b> Fixed in Bookworm RC 2.</dd>
</dl>
