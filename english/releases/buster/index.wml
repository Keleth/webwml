#use wml::debian::template title="Debian &ldquo;buster&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"

<p>Debian <current_release_buster> was
released on <a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Debian 10.0 was initially released on <:=spokendate('2019-07-06'):>."
/>
The release included many major
changes, described in
our <a href="$(HOME)/News/2019/20190706">press release</a> and
the <a href="releasenotes">Release Notes</a>.</p>

<p><strong>Debian 10 has been superseded by
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
Security updates have been discontinued as of <:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>However, buster benefits from Long Term Support (LTS) until
30th June, 2024. The LTS is limited to i386, amd64, armhf and arm64.
All other architectures are no longer supported in buster.
For more information, please refer to the <a
href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
</strong></p>

<p>To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

<p>Architectures supported during Long Term Support:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
</ul>


<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
<a href="../reportingbugs">report other issues</a> to us.</p>
