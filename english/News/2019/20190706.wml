<define-tag pagetitle>Debian 10 <q>buster</q> released</define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news

##
## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# ← this one must be removed; not that one → #use wml::debian::translation-check translation="1.1" maintainer=""


<p>After 25 months of development the Debian project is proud to present
its new stable version 10 (code name <q>buster</q>), which will
be supported for the next 5 years thanks to the combined work of the 
<a href="https://security-team.debian.org/">Debian Security team</a> 
and of the <a href="https://wiki.debian.org/LTS">Debian Long Term
Support</a> team.
</p>

<p>
Debian 10 <q>buster</q> ships with several desktop applications and
environments. Amongst others it now includes the desktop environments:
</p>
<ul>
<li>Cinnamon 3.8,</li>
<li>GNOME 3.30,</li>
<li>KDE Plasma 5.14,</li>
<li>LXDE 0.99.2,</li>
<li>LXQt 0.14,</li>
<li>MATE 1.20,</li>
<li>Xfce 4.12.</li>
</ul>

<p>
In this release, GNOME defaults to using the Wayland display server instead
of Xorg. Wayland has a simpler and more modern design, which has advantages
for security. However, the Xorg display server is still installed by default
and the default display manager allows users to choose Xorg as the display
server for their next session.
</p>

<p>
Thanks to the Reproducible Builds project, over 91% of the source 
packages included in Debian 10 will build bit-for-bit identical binary
packages.
This is an important verification feature which protects users against
malicious attempts to tamper with compilers and build networks. Future Debian
releases will include tools and metadata so that end-users can validate
the provenance of packages within the archive.
</p>

<p>
For those in security-sensitive environments
AppArmor, a mandatory access control
framework for restricting programs' capabilities, is installed and enabled by default.
Furthermore, all methods provided by APT (except
cdrom, gpgv, and rsh) can optionally make use of <q>seccomp-BPF</q> sandboxing.
The https method for APT is included in the apt package and does not need
to be installed separately.
</p>

<p>
Network filtering is based on the nftables framework by default in
Debian 10 <q>buster</q>. Starting with iptables v1.8.2 the binary package
includes iptables-nft and iptables-legacy, two variants of the iptables
command line interface. The nftables-based variant uses the nf_tables
Linux kernel subsystem. The <q>alternatives</q> system can be used to choose
between the variants.
</p>

<p>
The UEFI (<q>Unified Extensible Firmware Interface</q>) support first 
introduced in Debian 7 (code name <q>wheezy</q>) continues to be greatly improved in Debian 10
<q>buster</q>. Secure Boot support is included in this release for amd64, i386 and arm64 architectures
and should work out of the box on most Secure Boot-enabled machines. This
means users should no longer need to disable Secure Boot support in the firmware
configuration.
</p>

<p>
The cups and cups-filters packages are installed by default in Debian 10
<q>buster</q>, giving users everything that is needed to take advantage of
driverless printing. Network print queues and IPP printers will be
automatically set up and managed by cups-browsed and
the use of non-free vendor printing drivers and plugins can be dispensed with.
</p>

<p>
Debian 10 <q>buster</q> includes numerous updated software packages (over
62% of all packages in the previous release), such as:
</p>
<ul>
<li>Apache 2.4.38</li>
<li>BIND DNS Server 9.11</li>
<li>Chromium 73.0</li>
<li>Emacs 26.1</li>
<li>Firefox 60.7 (in the firefox-esr package)</li>
<li>GIMP 2.10.8</li>
<li>GNU Compiler Collection 7.4 and 8.3</li>
<li>GnuPG 2.2</li>
<li>Golang 1.11</li>
<li>Inkscape 0.92.4</li>
<li>LibreOffice 6.1</li>
<li>Linux 4.19 series</li>
<li>MariaDB 10.3</li>
<li>OpenJDK 11</li>
<li>Perl 5.28</li>
<li>PHP 7.3</li>
<li>PostgreSQL 11</li>
<li>Python 3 3.7.2</li>
<li>Ruby 2.5.1</li>
<li>Rustc 1.34</li>
<li>Samba 4.9</li>
<li>systemd 241</li>
<li>Thunderbird 60.7.2</li>
<li>Vim 8.1</li>
<li>more than 59,000 other ready-to-use software packages, built from
nearly 29,000 source packages.</li>
</ul>

<p>
With this broad selection of packages and its traditional wide 
architecture support, Debian once again stays true to its goal of being 
the universal operating system. It is suitable for many different use 
cases: from desktop systems to netbooks; from development servers to 
cluster systems; and for database, web and storage servers. At the same
time, additional quality assurance efforts like automatic installation 
and upgrade tests for all packages in Debian's archive ensure that 
<q>buster</q> fulfills the high expectations that users have of a
stable Debian release.
</p>

<p>
A total of ten architectures are supported:
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
for ARM, <code>armel</code>
and <code>armhf</code> for older and more recent 32-bit hardware,
plus <code>arm64</code> for the 64-bit <q>AArch64</q> architecture,
and for MIPS, <code>mips</code> (big-endian) 
and <code>mipsel</code> (little-endian) architectures for 32-bit hardware 
and <code>mips64el</code> architecture for 64-bit little-endian hardware.
</p>

<h3>Want to give it a try?</h3>
<p>
If you simply want to try Debian 10 <q>buster</q> without installing it,
you can use one of the available <a href="$(HOME)/CD/live/">live images</a> which load and run the
complete operating system in a read-only state via your computer's memory.
</p>

<p>
These live images are provided for the <code>amd64</code> and
<code>i386</code> architectures and are available for DVDs, USB sticks,
and netboot setups. The user can choose among different desktop
environments to try: Cinnamon, GNOME, KDE Plasma, LXDE, MATE, Xfce and, new
in buster, LXQt. Debian Live Buster re-introduces the standard live
image, so it is also possible to try a base Debian system without any
graphical user interface.
</p>
<p>
Should you enjoy the operating system you have the option of installing
from the live image onto your computer's hard disk. The live image
includes the Calamares independent installer as well as the standard Debian Installer.
 More information is available in the 
<a href="$(HOME)/releases/buster/releasenotes">release notes</a> and the
<a href="$(HOME)/CD/live/">live install images</a> sections of
the Debian website.
</p>

<p>
To install Debian 10 <q>buster</q> directly onto your
computer's hard disk you can choose from a variety of installation media
such as Blu-ray Disc, DVD, CD, USB stick, or via a network connection.
Several desktop environments &mdash; Cinnamon, GNOME, KDE Plasma Desktop and 
Applications, LXDE, LXQt, MATE and Xfce &mdash; may be installed through those
images.
In addition, <q>multi-architecture</q> CDs are available which support
installation from a choice of architectures from a single disc. Or you can
always create bootable USB installation media 
(see the <a href="$(HOME)/releases/buster/installmanual">Installation Guide</a>
for more details).
</p>

<p>
For cloud users, Debian offers direct support for many of the
best-known cloud platforms. Official Debian images are easily
selected through each image marketplace. Debian also publishes <a
href="https://cloud.debian.org/images/openstack/current/">pre-built
OpenStack images</a> for the <code>amd64</code> and <code>arm64</code>
architectures, ready to download and use in local cloud setups.
</p>

<p>
Debian can now be installed in 76 languages, with most of them available 
in both text-based and graphical user interfaces.
</p>

<p>
The installation images may be downloaded right now via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (the recommended method),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, or
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; see
<a href="$(HOME)/CD/">Debian on CDs</a> for further information. <q>Buster</q> will
soon be available on physical DVD, CD-ROM, and Blu-ray Discs from
numerous <a href="$(HOME)/CD/vendors">vendors</a> too.
</p>


<h3>Upgrading Debian</h3>
<p>
Upgrades to Debian 10 from the previous release, Debian 9
(code name <q>stretch</q>) are automatically handled by the apt
package management tool for most configurations.
As always, Debian systems may be upgraded painlessly, in place,
without any forced downtime, but it is strongly recommended to read
the <a href="$(HOME)/releases/buster/releasenotes">release notes</a> as
well as the <a href="$(HOME)/releases/buster/installmanual">installation
guide</a> for possible issues, and for detailed instructions on 
installing and upgrading. The release notes will be further improved and
translated to additional languages in the weeks after the release.
</p>


<h2>About Debian</h2>

<p>
Debian is a free operating system, developed by
thousands of volunteers from all over the world who collaborate via the
Internet. The Debian project's key strengths are its volunteer base, its
dedication to the Debian Social Contract and Free Software, and its 
commitment to provide the best operating system possible. This new 
release is another important step in that direction.
</p>


<h2>Contact Information</h2>

<p>
For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.
</p>
