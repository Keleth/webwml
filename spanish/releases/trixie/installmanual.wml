#use wml::debian::template title="Debian trixie -- Guía de instalación" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#use wml::debian::translation-check translation="bd44c0367d403f4c6efaa83c6373a24a7991a1bb"

<if-stable-release release="buster">
<p>Esta es una <strong>versión beta</strong> de la guía de instalación para Debian
11, nombre en clave bullseye, que aún no se ha publicado. La información
que se presenta aquí podría estar desactualizada o ser inexacta debido a cambios en
el instalador. Podría interesarle la
<a href="../buster/installmanual">guía de instalación de Debian
10, nombre en clave buster</a>, que es la última versión publicada de
Debian, o la <a href="https://d-i.debian.org/manual/">versión de
los desarrolladores de la guía de instalación</a>, que es la versión más reciente
de este documento.</p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>Esta es una <strong>versión beta</strong> de la guía de instalación para Debian
13, nombre en clave trixie, que aún no se ha publicado. La información
que se presenta aquí podría estar desactualizada o ser inexacta debido a cambios en
el instalador. Podría interesarle la
<a href="../bullseye/installmanual">guía de instalación de Debian
11, nombre en clave bullseye</a>, que es la última versión publicada de
Debian, o la <a href="https://d-i.debian.org/manual/">versión de
los desarrolladores de la guía de instalación</a>, que es la versión más reciente
de este documento.</p>
</if-stable-release>

<p>Las instrucciones de instalación, junto con los ficheros que se pueden descargar,
están disponibles para cada una de las arquitecturas soportadas:</p>

<ul>
<:= &permute_as_list('', 'Guía de instalación'); :>
</ul>

<p>Si ha configurado adecuadamente las opciones de localización de su
navegador, podrá usar el enlace anterior para acceder automáticamente a la versión
HTML correcta &mdash; consulte la información sobre <a href="$(HOME)/intro/cn">negociación de contenido</a>.
Si no es así, elija en la tabla siguiente la arquitectura, formato e
idioma concretos que desee.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arquitectura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Idiomas</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[1].$_[2]" } ); :>
</table>
</div>
