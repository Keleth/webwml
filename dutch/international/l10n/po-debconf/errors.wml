#use wml::debian::template title="Fouten in vertaalde Debconf-sjablonen"
#include "$(ENGLISHDIR)/international/l10n/po-debconf/menu.inc"
#use wml::debian::translation-check translation="b339a77580e06b206bedadd0ee4df7dd5cae5ef8"

<p>
U kunt een <a href="errors-by-pkg">alfabetische lijst van pakketten</a> vinden met fouten in hun vertaalde Debconf-sjablonen, of dezelfde lijst gesorteerd op <a href="errors-by-maint">beheerders</a>.
</p>

<p>
Om ruimte te besparen bevatten beide lijsten trefwoorden in plaats van betekenisvolle berichten, die hier worden gedefinieerd. Sommige van deze fouten moeten worden hersteld door de vertalers, terwijl andere moeten worden hersteld door de beheerder.
</p>

<h3>Fouten die onder de verantwoordelijkheid van de vertalers vallen</h3>

<dl>
  <dt><a name="charsetname">ongeldige-tekensetnaam-in-po</a></dt>
  <dd>
    Po-bestanden moeten een geldige tekenset hebben in het veld Content-Type: van de koptekst. De tekenset is de keuze van de vertalers en het po-bestand moet door de vertalers zelf worden gerepareerd, tenzij de beheerders absoluut zeker weten wat ze doen.
  </dd>
  <dt><a name="charset">foute-tekenset</a></dt>
  <dd>
    De tekenset in het veld Content-Type: van de po-koptekst is niet dezelfde als die in het po-bestand. De tekenset is de keuze van de vertalers en het po-bestand moet door de vertalers zelf gerepareerd worden, tenzij de beheerders absoluut zeker zijn van wat ze doen. Deze bestanden zijn helemaal niet bruikbaar, de beheerders zouden daarom de vertalers vragen om de bestanden te repareren.
  </dd>
  <dt><a name="invalidpo">ongeldige-po</a></dt>
  <dd>
    Het po-bestand is niet geldig. De redenen hiervoor kunnen talrijk zijn. De uitvoer van msgfmt zou de vertalers moeten helpen om hun bestanden te repareren. Deze bestanden zijn helemaal niet bruikbaar, de beheerders zouden daarom de vertalers moeten vragen om de bestanden te repareren.
  </dd>
</dl>

<h3>Fouten die onder de verantwoordelijkheid van de beheerders vallen</h3>

<dl>
  <dt><a name="unknownlanguage">onbekende-taal</a></dt>
  <dd>
    Een po bestand heeft een onbekende taalcode. De basisnaam van het bestand zou een geldige taalcode moeten zijn. De kopregels zouden kunnen helpen om te achterhalen voor welke taal het bestand bedoeld is, anders heeft het geen zin om het in een pakket op te nemen omdat niemand het zal gebruiken.
  </dd>
  <dt><a name="missingfile">ontbrekend-bestand-in-POTFILES.in</a></dt>
  <dd>
    Het bestand debian/po/POTFILES.in verwijst naar niet-bestaande sjabloonbestanden. Deze fout treedt vaak op na het hernoemen of verwijderen van sjabloonbestanden. Het is de verantwoordelijkheid van de beheerders om dit bestand te repareren, de po-bestanden bij te werken en om bijgewerkte vertalingen te vragen. De vertalers zouden <strong>niet</strong> aan deze pakketten moeten werken, aangezien het bestand templates.pot meestal volledig verouderd is.
  </dd>
  <dt><a name="template">niet-up-to-date-templates.pot</a></dt>
  <dd>
    Het bestand debian/po/templates.pot is niet gesynchroniseerd met de sjablonenbestanden. De beheerders moeten hun pakketten repareren door debconf-updatepo toe te voegen aan het target "clean" van het bestand debian/rules. De vertalers moeten eerst debconf-updatepo uitvoeren als ze met het broncodepakket werken. De po- en pot-bestanden op de website zouden up-to-date moeten zijn.
  </dd>
  <dt><a name="po">niet-up-to-date-po-bestand</a></dt>
  <dd>
    De vermelde bestanden zijn niet gesynchroniseerd met de sjablonen. De beheerders moeten hun pakketten repareren door debconf-updatepo toe te voegen aan het target "clean" van het bestand debian/rules. De vertalers moeten eerst debconf-updatepo uitvoeren als ze met het broncodepakket werken. De po-bestanden op de website zouden up-to-date moeten zijn.
  </dd>
</dl>
