#use wml::debian::blend title="De ontwikkeling van de specifieke uitgave"
#use "navbar.inc"
#use wml::debian::translation-check translation="c3a3b8986d2ffef95ef7bb8f7d99f36678ff0e8f"

<h2>Verpakking</h2>

<p>Terwijl het verpakken van hamradiosoftware in Debian de verantwoordelijkheid is
van de <a href="https://pkg-hamradio.alioth.debian.org">Debian Hamradio-beheerders</a>,
biedt dit project voor het maken van de specifieke uitgave de volgende
hulpmiddelen die nuttig kunnen zijn voor verpakkers:</p>

<ul>
	<li><a href="https://blends.debian.org/hamradio/tasks/">Index van taken voor de specifieke uitgave</a></li>
	<li><a href="https://blends.debian.org/hamradio/bugs/">Overzicht van bugs in  de specifieke uitgave</a></li>
	<li><a href="https://blends.debian.org/hamradio/thermometer/">Thermometer van de specifieke uitgave</a></li>
</ul>

<h2>Broncode van metapakketten</h2>

<ul>
	<li><a href="https://salsa.debian.org/blends-team/hamradio">Webgebaseerde gitbrowser</a></li>
	<li><a href="https://salsa.debian.org/blends-team/hamradio.git">Anonieme git-kloon</a></li>
</ul>

<pre>git clone https://salsa.debian.org/blends-team/hamradio
cd hamradio ; make dist
gbp buildpackage</pre>

<p><i>Opmerking: u zult <a
href="https://packages.debian.org/unstable/git-buildpackage">git-buildpackage</a>
en <a href="https://packages.debian.org/unstable/blends-dev">blends-dev</a>
moeten installeren om de broncode te kunnen compileren.</i></p>

<p>Om meer te weten over hoe te werken met metapakketten van specifieke uitgaven,
kunt u <a href="https://blends.debian.org/blends/ch06.html#metapackages">§6.1</a>
van de <a href="https://blends.debian.org/blends/">Debian handleiding voor
specifieke uitgaven</a> raadplegen.</p>

<h2>Broncode van de live-dvd</h2>

<ul>
	<li><a href="https://salsa.debian.org/blends-team/blends-images">Webgebaseerde gitbrowser</a></li>
	<li><a href="https://salsa.debian.org/blends-team/blends-images.git">Anonieme git-kloon</a></li>
</ul>

<pre>git clone https://salsa.debian.org/blends-team/blends-images.git
cd blends-images/images/hamradio-amd64
lb config
sudo lb build</pre>

<p><i>Opmerking: u zult <a
 href="https://packages.debian.org/unstable/git-buildpackage">git-buildpackage</a>
	en <a href="https://packages.debian.org/unstable/live-build">live build</a>
	moeten installeren om de broncode te kunnen compileren.</i></p>

<p>Om meer te weten over het werken met de broncode van live-build, kunt u de <a
 href="http://live.debian.net/manual/unstable/html/live-manual/toc.en.html">Handleiding voor live-systemen</a> raadplegen.</p>

