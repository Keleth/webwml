#use wml::debian::template title="Een spiegelserver opzetten voor het Debian archhief"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/sid/archive.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="37505fd3e8d15e728afdde60b6ad9cc4362d075c"

<toc-display />

<toc-add-entry name="whether">Is een spiegelserver opzetten nuttig</toc-add-entry>

<p>Hoewel we nieuwe spiegelservers appreciëren, zou elke aanstaande
spiegelserver-onderhouder zeker op de volgende vragen moeten kunnen
antwoorden, vooraleer met een eigen spiegelserver te beginnen:</p>

<ul>
  <li>Is op mijn locatie een spiegelserver noodzakelijk? Misschien zijn
      er in de omgeving reeds spiegelservers.</li>
  <li>Heb ik de middelen om een spiegelserver te huisvesten?
      Spiegelservers gebruiken heel wat <a href="size">schijfruimte</a>
      en bandbreedte en men moet in staat zijn de
      kosten ervan te dragen.</li>
  <li>Is een spiegelserver de juiste keuze? Indien u in de eerste plaats
      gebruikers wilt ondersteunen bij uw internetprovider/voorziening,
      dan is het misschien beter te kiezen voor een cachende proxy, zoals
      apt-cacher-ng, squid, of varnish.</li>
</ul>

<toc-add-entry name="what">Wat gespiegeld kan worden</toc-add-entry>

<p>De <a href="./">hoofdpagina in verband met spiegelservers</a> vermeldt de
archieven die gespiegeld kunnen worden.</p>

<ul>
<li>
Gebruikers zullen zoeken naar het archief debian/ om Debian over het netwerk
te installeren, om er cd's mee te maken (met behulp van jigdo) of om
reeds geïnstalleerde systemen op te waarderen. <em>We bevelen u aan om
deze pakketbron te spiegelen.</em></li>

<li>
Het archief debian-cd/ is niet op alle spiegelservers identiek.
Op sommige locaties bevat het jigdo-sjablonen om er cd-images mee
te bouwen (wordt gebruikt in combinatie met bestanden uit debian/),
op andere bevat het reeds geconstrueerde cd-images, en op nog andere de beide.
<br />
Raadpleeg de pagina over <a href="$(HOME)/CD/mirroring/">de cd-images
spiegelen</a> voor bijkomende informatie hierover.</li>

<li>
Het archief debian-archive/ bevat het echte <em>archief</em>, de oude
en in onbruik geraakte versies van Debian. Over het algemeen is dit
slechts voor een klein segment gebruikers interessant. (Indien u eraan
twijfelt of u dit wel zou spiegelen, doet u het waarschijnlijk best niet.)</li>

</ul>

<p>Raadpleeg de pagina over <a href="size">grootte van een spiegelserver</a>
voor exactere informatie over de grootte ervan.</p>

<p>Het archief debian-security/ bevat de beveiligingsupdates die door
het veiligheidsteam van Debian uitgebracht worden. Hoewel dit voor iedereen
interessant zou kunnen lijken, raden we gebruikers niettemin af om
beveiligingsupdates via een spiegelserver op te halen, en vragen we hen
integendeel om deze rechtstreeks te downloaden van onze gedecentraliseerde
dienst security.debian.org. <em>We bevelen het spiegelen van debian-security
<strong>niet</strong> aan.</em></p>

<toc-add-entry name="wherefrom">Welke locatie spiegelen</toc-add-entry>

<p>Merk op dat <code>ftp.debian.org</code> niet de oorspronkelijke locatie is
van de pakketten van Debian. Het is eerder een van de vele servers die
bijgewerkt worden vanuit een interne server van Debian.

Er zijn vele <a href="list-full">publieke spiegelservers</a> die
ondersteuning bieden voor rsync en die geschikt zijn om gespiegeld te
worden. Gebruik er een die vanuit netwerkoogpunt bij u in de buurt is.</p>

<p>U moet voorkomen dat u spiegelt vanaf een dienst met een naam welke
naar meer dan één netwerkadres kan leiden (zoals <code>ftp.us.debian.org</code>). Als uw toeleverende spiegelservers niet synchroon lopen, kan dit tot
gevolg hebben dat u bij uw eigen spiegelingen (mirror-runs) probeert te
synchroniseren met verschillende toestanden.
#
Merk ook op dat HTTP de enige dienst is die gegarandeerd wordt op
<code>ftp.CC.debian.org</code>. Indien u met behulp van rsync (bij
voorkeur met ftpsync) wenst te spiegelen, raden we u aan
om de juiste sitenaam te gebruiken voor de machine die momenteel
<code>ftp.CC.debian.org</code> aanbiedt. (Raadpleeg de map
<code>/debian/project/trace</code> van die server om deze te kennen).

<toc-add-entry name="how">Hoe spiegelen</toc-add-entry>

<p>De aanbevolen methode van spiegelen is met behulp van de collectie scripts
van ftpsync. Deze zijn onder deze vormen beschikbaar:</p>
<ul>
    <li>als een tar-archief op <url "https://ftp-master.debian.org/ftpsync.tar.gz"></li>
    <li>als een git-opslagplaats: <kbd>git clone https://salsa.debian.org/mirror-team/archvsync.git</kbd> (zie <url "https://salsa.debian.org/mirror-team/archvsync/">)</li>
    <li>als een Debian-pakket: <a href="https://packages.debian.org/stable/ftpsync">ftpsync</a></li>
</ul>

<p>Gebruik geen eigen scripts en voer geen rsync met één enkele doorloop uit.
Met ftpsync bent u zeker dat de updates zodanig gebeuren dat ze apt niet in
verwarring brengen. Meer in het bijzonder verwerkt ftpsync
vertalingsbestanden, contents-bestanden en andere bestanden met metadata
in een dergelijke volgorde dat apt niet tegen validatiefouten kan aanlopen,
mocht een gebruiker de pakketlijst bijwerken terwijl een spiegelingsdoorloop
aan de gang is. Daarenboven maakt het ook trace-bestanden aan die extra
informatie bevatten die nuttig is om na te gaan of een spiegelserver
behoorlijk functioneert, welke architecturen deze bevat en waarmee
gesynchroniseerd wordt.</p>

<toc-add-entry name="partial">Partiële spiegeling</toc-add-entry>

<p>Rekening houdend met de reeds <a href="size">grote omvang van het Debian
archief</a>, kan het aangewezen zijn om slechts delen van het archief te
spiegelen. Publieke spiegelservers moeten alle suites (testing, unstable,
enz.) bevatten, maar zij kunnen eventueel de collectie architecturen
welke ze spiegelen, beperken. Met het oog hierop bevat het configuratiebestand
van ftpsync de instellingen ARCH_EXCLUDE en ARCH_INCLUDE.</p>

<toc-add-entry name="when">Wanneer spiegelen</toc-add-entry>

<p>Het hoofdarchief wordt viermaal per dag bijgewerkt.
Gewoonlijk beginnen de spiegelservers met updaten om 3:00, 9:00, 15:00 en 21:00 (alle tijden in UTC),
maar dit zijn nooit vaste tijdstippen en u dient uw spiegelserver daarop niet te fixeren.</p>

<p>Uw spiegelserver zou moeten updaten, enkele uren na een van de
spiegelimpulsen van het hoofdarchief. U kunt nagaan of de site die door u
gespiegeld wordt een bestand met tijdsaanduiding plaatst in haar map
<kbd>project/trace/</kbd>. De naam van het bestand met de tijdsaanduiding
is identiek aan deze van de site zelf en het bestand bevat het tijdstip
waarop de laatste bijwerking van de spiegelserver voltooid werd. Tel (voor
alle veiligheid) enkele uren bij en voer dan de spiegeling uit.</p>

<p><b>Het is van essentieel belang dat uw spiegelserver synchroon is
met het hoofdarchief</b>. Een minimum van 4 updates per etmaal zal
ervoor zorgen dat uw spiegelserver het archief betrouwbaar reflecteert.
U moet weten dat spiegelservers die niet synchroon zijn met het hoofdarchief,
niet vermeld zullen worden in de officiële lijst van spiegelservers.</p>

<p>De makkelijkste manier om de spiegelingsdoorloop iedere dag automatisch
te laten plaats vinden, is cron gebruiken. Zie <kbd>man crontab</kbd> voor details.</p>

<p>Merk op dat indien uw site getriggerd wordt door een push-mechanisme,
u zich over al deze zaken geen zorgen dient te maken.</p>

<h3>Spiegelen op een door push getriggerde manier</h3>

<p><q>Push</q>-spiegelen is een vorm van spiegelen die we ontwikkeld hebben
om de tijd te beperken die nodig is om wijzigingen in het archief tot bij de
spiegelservers te krijgen. Een toeleverende spiegelserver gebruikt een
SSH-trigger om een ontvangende spiegelserver te laten weten dat deze
zichzelf moet updaten.
Raadpleeg <a href="push_mirroring">de volledige uitleg</a> met een
meer gedetailleerde beschrijving van hoe dit werkt, waarom dit veilig is
en hoe u een push-spiegelserver kunt opzetten.</p>

<toc-add-entry name="settings">Aanbevolen extra instellingen</toc-add-entry>

<p>Publieke spiegelservers moeten het archief van Debian aanbieden over HTTP
op <code>/debian</code>.</p>

<p>Bovendien moet het mogelijk zijn om van een map een lijst met bestanden
op te vragen (met weergave van de volledige bestandsnamen) en moeten
symbolische koppelingen gevolgd worden.

Indien u Apache gebruikt, zal iets als hieronder werken:
<pre>
&lt;Directory <var>/pad/naar/uw/debian/spiegelserver</var>&gt;
   Options +Indexes +SymlinksIfOwnerMatch
   IndexOptions NameWidth=* +SuppressDescription
&lt;/Directory&gt;
</pre>

<toc-add-entry name="submit">Hoe u een spiegelserver aan de lijst van spiegelservers moet toevoegen</toc-add-entry>

<p>
Indien u wilt dat uw spiegelserver vermeld wordt in de officiële lijst
van spiegelservers, moet u ervoor zorgen
</p>

<ul>
<li>dat uw spiegelserver vier maal per etmaal synchroniseert met het archief</li>
<li>dat uw spiegelserver ook de broncodebestanden bevat voor de
architecturen waarvoor u als spiegelserver functioneert</li>
</ul>

<p>Eens de spiegelserver opgezet is, moet u deze <a href="submit">bij Debian
registreren</a> om om deze te laten opgenemen in de
<a href="list">lijst van officiële spiegelservers</a>.
Registraties kunnen gebeuren via ons <a href="submit">eenvoudig webformulier</a>.</p>


<p>Voor problemen of vragen kunt u terecht op het adres <email mirrors@debian.org>.</p>

<toc-add-entry name="mailinglists">Mailinglijsten</toc-add-entry>

<p>Er bestaan twee publieke <a href="../MailingLists/">mailinglijsten</a>
over Debian spiegelservers,
<a href="https://lists.debian.org/debian-mirrors-announce/">debian-mirrors-announce</a>
en
<a href="https://lists.debian.org/debian-mirrors/">debian-mirrors</a>.
We moedigen alle onderhouders van spiegelservers aan om in te tekenen op
de lijst voor aankondigingen, omdat daarop alle belangrijke aankondigingen
gemeld worden. Het is een gemodereerde lijst en deze kent slechts een gering verkeer.
De tweede mailinglijst is bedoeld voor algemene discussie en staat open
voor iedereen.</p>

<p>Indien u met vragen zit waarop deze web-pagina's geen antwoord bieden,
kunt u ons contacteren op <email mirrors@debian.org> of met IRC op het kanaal #debian-mirrors van <tt>irc.debian.org</tt>.</p>


<toc-add-entry name="private-mirror">Opmerkingen voor private (gedeeltelijke) spiegelservers</toc-add-entry>

<p>
Indien u enkel ten behoeve van uw eigen site een spiegelserver wilt
opzetten en u enkel een gedeelte van de suites (bijvoorbeeld stable) wilt
spiegelen, kan <a href="https://packages.debian.org/stable/debmirror">debmirror</a>
misschien ook iets voor u zijn.
</p>
