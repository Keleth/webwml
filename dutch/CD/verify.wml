#use wml::debian::cdimage title="De authenticiteit van Debian-images controleren" BARETITLE=true
#use wml::debian::translation-check translation="942f98b47dc32183d181b4c309c038a3e8c33f3d"

<p>
Officiële releases van Debian installatie- en live-images bevatten een ondertekend controlegetal-bestand.
U vindt dit samen met het image in de map <code>iso-cd</code>,
<code>jigdo-dvd</code>, <code>iso-hybrid</code>, enz.
Deze bestanden laten u toe na te gaan of de images welke u downloadt, correct zijn.
Vooreerst kan het controlegetal gebruikt worden om te controleren of de images
bij het downloaden niet beschadigd werden.
Bovendien bevestigt de ondertekening van het controlegetal-bestand dat de bestanden
wel degelijk deze zijn welke uitgegeven werden door
Debian en dat er niet mee geknoeid werd.
</p>

<p>
Om zekerheid te verkrijgen over de geldigheid van de inhoud van een image-bestand,
moet u het juiste controlegetal-gereedschap gebruiken.
Voor elke release zijn cryptografisch sterke controlegetal-algoritmes
(SHA256 and SHA512) beschikbaar. Om ermee te werken, moet u
gebruik maken van de bijbehorende hulpmiddelen <code>sha256sum</code> en <code>sha512sum</code>.
</p>

<p>
Om er zeker van te zijn dat de controlegetal-bestanden zelf correct zijn,
moet u OpenPGP-implementatie (zoals GnuPG, Sequoia-PGP, PGPainless of GopenPGP)
gebruiken om ze te verifiëren aan de hand van het begeleidende
ondertekeningsbestand (bijv. <code>SHA512SUMS.sign</code>).
De sleutels welke gebruikt worden voor deze ondertekening bevinden zich
allemaal in de <a href="https://keyring.debian.org">Debian OpenPGP-sleutelbos</a>,
en de beste manier om deze te controleren, is deze sleutelbos te gebruiken om
deze via het web van vertrouwen (the web of trust) geldig te laten verklaren.
Om het makkelijker te maken voor mensen die geen toegang hebben tot een bestaande Debian-machine, volgen hier details over de sleutels die de afgelopen jaren zijn gebruikt om releases te ondertekenen, en links om de publieke sleutels rechtstreeks te downloaden:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
