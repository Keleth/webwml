#use wml::debian::template title="Debian GNU/Linux 3.0 &ldquo;woody&rdquo; release-informatie" BARETITLE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6933c146d6b5dc1ef12d968ac264529d8ab5df51"

<p>Debian GNU/Linux 3.0 (ook bekend als <em>woody</em>) werd uitgebracht op 19
juli 2002. De nieuwe release bevat verschillende belangrijke wijzigingen, beschreven in
ons <a href="$(HOME)/News/2002/20020719">persbericht</a> en in de
<a href="releasenotes">Notities bij de release</a>.</p>

<p><strong>Debian GNU/Linux 3.0 werd vervangen door
<a href="../sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds eind juni 2006.</strong></p>

<p>Debian GNU/Linux 3.0 is verkrijgbaar <a href="$(DISTRIB)/">via
het internet</a> en <a href="$(HOME)/CD/vendors/">via cd-verkopers</a>.</p>

<p>Lees eerst de installatiehandleiding voordat u Debian installeert. De Installatiehandleiding voor uw doelarchitectuur bevat instructies en links voor alle bestanden die u moet installeren.</p>

<p>De volgende computerarchitecturen werden in deze release ondersteund:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bits pc (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mips/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release van woody, ondanks dat deze <em>stabiel</em> werd verklaard. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd andere problemen rapporteren.</p>
