#use wml::debian::template title="Overzetting naar PA-RISC -- Nieuws" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hppa/menu.inc"
#use wml::debian::translation-check translation="1f99c642fe56e0113f0730c6247f562992a7e25f"

<h1>Nieuws over Debian voor PA-RISC</h1>

<h3><:=spokendate ("2001-08-06"):></h3>

<p><strong>Debian accepteert dat hppa wordt uitgebracht met Debian 3.0 (woody)!</strong>

<p>
Het is met groot genoegen dat we de acceptatie van hppa aankondigen als een
architectuur voor de aankomende stabiele release Debian 3.0, codenaam woody.
Installatiegereedschappen voor hppa zitten nu in de boomstructuur van woody, en
pakketten die nu in unstable zitten beginnen door te schuiven naar de
boomstructuur van testing/woody.

<h3><:=spokendate ("2001-07-17"):></h3>

<p><strong>Officieel verzoek om uitgebracht te worden met Debian 3.0 (woody)</strong>

<p>
Bijna 70% van alle Debian-pakketten zijn gebouwd en bijgewerkt in het archief,
een geautomatiseerd bouwsysteem draait goed, installatiegereedschappen zijn
beschikbaar in het archief, en het aantal actieve systemen groeit gestaag.

<h3><:=spokendate ("2001-05-31"):></h3>

<p><strong>HP brengt 0.9 cd-images uit!</strong>

<p>
HP heeft een momentopname van de boomstructuur van de onstabiele Debian
distributie "sid" beschikbaar gesteld voor hppa in de vorm van een reeks
cd-images. Zie <a href="http://www.parisc-linux.org/release-0.9/">
de releasepagina</a> voor meer informatie. Deze release maakt het aannemelijk
dat de hppa-architectuur klaar zal zijn om uitgebracht te worden met
woody, hoewel er nog veel werk te doen blijft tussen nu en dan!

<h3><:=spokendate ("2000-10-16"):></h3>

<p><strong>Er werd een boomstructuur voor binaire pakketten gecreëerd</strong>

<p>
Vandaag werd de boomstructuur voor de binaire pakketten van de hppa-architectuur
toegevoegd aan de 'sid'-distributie op de hoofdsite van Debian. Bdale beheert
een actieve auto-builder en pakketten zouden binnenkort op de spiegelserversites
van Debian moeten verschijnen. Installatie-informatie en enkele kritieke
pakketten zijn echter nog steeds alleen beschikbaar via de website van
<a href="https://parisc.wiki.kernel.org/">PA-RISC Linux</a>.

<h3><:=spokendate ("2000-08-01"):></h3>

<p><strong>Naamsverandering</strong>

<p>
De kwestie of men als tekenreeks voor de Debian-architectuur 'parisc' dan wel
'hppa' zal gebruiken werd op OLS beslecht in het voordeel van 'hppa'. Deze
pagina over de overzetting wordt verplaatst om die verandering te weerspiegelen,
en er is een verzoek in de maak om de naam van de mailinglijst debian-parisc te
veranderen in debian-hppa.

<h3><:=spokendate ("2000-03-31"):></h3>

<p><strong>HP stelt als bijdrage een bouwmachine ter beschikking</strong>

<p>
HP heeft een J5000 op lange termijn aan Debian uitgeleend om te helpen bij het
compileren van pakketten als we zover zijn.
<p>
Ook werd de mailinglijst debian-parisc geactiveerd.

<h3><:=spokendate ("2000-03-04"):></h3>

<p><strong>De overzetting van Debian naar PA-RISC is officieel begonnen</strong>

<p>
Er werd een nieuwe mailinglijst, debian-parisc, aangevraagd, maar die is nog
niet actief.
