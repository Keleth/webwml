#use wml::debian::template title="Informatie over stemmingen in Debian" BARETITLE="true" NOHEADER="true" NOHOMELINK="true"
#use wml::debian::votebar
#use wml::debian::translation-check translation="a595ef278c9dec8c13fa8f350492b98ec20af3d0"

    <h1 class="title">Informatie over stemmingen in Debian</h1>

    <p>
       Het Debian project heeft een stemvolgsysteem
       (DEbian VOTe EnginE
       [<a href="https://vote.debian.org/~secretary/devotee.git/">devotee</a>])
       dat de status weergeeft van lopende Algemenen Resoluties en
       de resultaten van vorige stemmingen.
    </p>

    <p>
       Onder de status van lopende Algemene Resoluties vermelden we het
       resolutievoorstel en de lijst van personen die het ondersteunen,
       alle belangrijke data en wat de vereisten voor goedkeuring zijn.
       Natuurlijk vermeldt de status ook één van de volgende zaken:
    </p>
    <ul>
       <li>Het voorstel - in afwachting van medeondertekenaars.</li>
       <li>De discussie - een minimale periode van twee weken discussie.</li>
       <li>De stemming - als de stemming bezig is.</li>
       <li>Afgesloten - als de stemming voorbij is.</li>
    </ul>

    <p>
      Bij de resultaten van eerdere stemmingen (afgesloten resoluties)
      vermelden we de uitslag samen met een lijst van alle personen die hun
      stem uitbrachten en wat die stem was. Tenzij het een geheime stemming
      betrof, wordt ook de tekst van elke ingezonden stem vermeldt.
    </p>

    <p>
      Debian gebruikt de
      <a href="https://en.wikipedia.org/wiki/Condorcet_method">Condorcet-methode</a>
      bij de verkiezing van de projectleider (het wikipedia-artikel waarnaar de
      link doorverwijst, is behoorlijk informatief). Simpel gezegd, kan een
      eenvoudige Condorcet-methode als volgt uitgelegd worden
    </p>
      <blockquote>
        <p>
         Bekijk alle mogelijke tweekampen tussen kandidaten. De eventuele
         winnaar volgens de Condorcet-methode is de kandidaat die elke andere
         kandidaat verslaat in een tweestrijd met die kandidaat.
	 </p>
       </blockquote>
    <p>
       Het probleem is dat bij complexe verkiezingen een circulaire relatie
       mogelijk is, waarbij A de bovenhand haalt op B, B de bovenhand haalt
       op C en C de bovenhand haalt op A. De meeste variaties op Condorcet
       maken gebruik van verschillende manieren om die knoop te ontwarren.
       Raadpleeg <a href="https://en.wikipedia.org/wiki/Cloneproof_Schwartz_Sequential_Dropping">Cloneproof Schwartz Sequential Dropping</a>
       voor details. De variant die Debian gebruikt wordt in de
       <a href="$(HOME)/devel/constitution">constitutie</a> uitgelegd,
       meer in het bijzonder in § A.5.
    </p>
    <p>
      Voor meer informatie over hoe u de matrix moet lezen van wie wie verslaat,
      welke gepubliceerd wordt als uitkomst van de stemming, kunt u dit
      <a href="https://en.wikipedia.org/wiki/Schwartz_method">voorbeeld</a> bekijken.
    </p>
