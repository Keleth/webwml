#use wml::debian::translation-check translation="1c49456c19b64e777ff4c1303f2d1e5402da4775"
<define-tag pagetitle>Debian 12 is bijgewerkt: 12.4 werd uitgebracht</define-tag>
<define-tag release_date>2023-12-10</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Houd er rekening mee dat dit document zo goed mogelijk is bijgewerkt om aan te geven dat Debian 12.3 wordt vervangen door Debian 12.4. Deze veranderingen kwamen voort uit het bugadvies uit <a href=https://bugs.debian.org/1057843>#1057843</a> betreffende problemen met linux-image-6.1.0-14 (6.1.64-1) dat ons op het laatste nippertje bereikte.</p>

<p>Debian 12.4 werd uitgebracht met linux-image-6.1.0-15 (6.1.66-1), samen met enkele andere reparaties van bugs</p>

<p>Het Debian-project kondigt met genoegen de vierde update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction adequate "De test in verband met niet-overeenkomende symboolgroottes overslaan op architecturen waar array-symbolen geen specifieke lengte hebben; waarschuwingen die in Perl 5.38 gegeven worden over de afschaffing van smartmatch uitschakelen; waarschuwingen over het experimentele karakter van Smartmatch bij versievergelijkingen oplossen">
<correction amanda "Probleem van lokale rechtenuitbreiding oplossen [CVE-2023-30577]">
<correction arctica-greeter "Het logo bij de begroeting weg van de rand plaatsen">
<correction awstats "Bij een upgrade vragen voorkomen als gevolg van het opschonen van de logrotate-configuratie">
<correction axis "Niet-ondersteunde protocollen uitfilteren in de clientklasse ServiceFactory [CVE-2023-40743]">
<correction base-files "Update voor de tussenrelease 12.4">
<correction ca-certificates-java "Circulaire afhankelijkheden verwijderen">
<correction calibre "Crash in Get Books bij het regenereren van UIC-bestanden repareren">
<correction crun "Reparatie voor containers die systemd gebruiken als hun init-systeem, als recentere kernel-versies gebruikt worden">
<correction cups "Rekening houden met het feit dat op sommige printers de ColorModel optie voor kleurenafdrukken CMYK is en niet RGB.">
<correction dav4tbsync "Nieuwe bovenstroomse versie, die de compatibiliteit met nieuwere Thunderbird-versies herstelt">
<correction debian-edu-artwork "Voorzien in een grafische vormgeving gebaseerd op het Emerald-thema voor Debian Edu 12">
<correction debian-edu-config "Nieuwe bovenstroomse stabiele versie; reparatie voor het instellen en wijzigen van LDAP-wachtwoorden">
<correction debian-edu-doc "Update van meegeleverde documentatie en vertalingen">
<correction debian-edu-fai "Nieuwe bovenstroomse stabiele versie">
<correction debian-edu-router "Genereren van configuratie van dnsmasq voor netwerken via VLAN repareren; alleen UIF-filterregels voor SSH genereren als de 'Uplink'-interface is gedefinieerd; vertalingen bijwerken">
<correction debian-installer "ABI van de Linux kernel verhogen naar 6.1.0-15; opnieuw bouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction debootstrap "Wijzigingen in ondersteuning voor een samengevoegd /usr overnemen van trixie: een samengevoegd /usr implementeren via post-merging, standaard ingesteld op een samengevoegd /usr in alle profielen voor suites die recenter zijn dan bookworm">
<correction devscripts "Debchange: updaten naar de huidige Debian distributies">
<correction dhcpcd5 "Breaks/Replaces dhcpcd5 veranderen naar Conflicts">
<correction di-netboot-assistant "Reparatie aan de ondersteuning voor het live ISO-image van bookworm">
<correction distro-info "Bijwerken van tests voor distro-info-data 0.58+deb12u1, waarin de EoL-datum van Debian 7 aangepast werd">
<correction distro-info-data "Toevoegen van Ubuntu 24.04 LTS Noble Numbat; aanpassen van verschillende datums voor het einde van de levensduur">
<correction eas4tbsync "Nieuwe bovenstroomse versie, die de compatibiliteit met nieuwere Thunderbird-versies herstelt">
<correction exfatprogs "Problemen met geheugentoegang buiten het bereik oplossen [CVE-2023-45897]">
<correction exim4 "Beveiligingsproblemen met betrekking tot het proxyprotocol [CVE-2023-42117] en DNSDB-opzoekingen [CVE-2023-42119] oplossen; kwetsbaarheidsbeveiliging toevoegen voor SPF-opzoekingen; UTF-16-surrogaten van ${utf8clean:...} niet toestaan; crash oplossen met <q>tls_dhparam = none</q>; $recipients-uitbreiding oplossen bij gebruik binnen ${run...}; vervaldatum van automatisch gegenereerde SSL-certificaten oplossen; crash oplossen veroorzaakt door sommige combinaties van strings met nullengte en ${tr...}">
<correction fonts-noto-color-emoji "Ondersteuning voor Unicode 15.1 toevoegen">
<correction gimp "Toevoegen van Conflicts and Replaces: gimp-dds om oude versies van deze plug-in te verwijderen die door gimp wordt aangeleverd sinds 2.10.10">
<correction gnome-characters "Ondersteuning voor Unicode 15.1 toevoegen">
<correction gnome-session "Tekstbestanden openen in gnome-text-editor als gedit niet is geïnstalleerd">
<correction gnome-shell "Nieuwe bovenstroomse stabiele release; toestaan dat meldingen behalve met de delete-toets ook gesloten worden met de backspace-toets; reparatie voor het feit dat apparaten dubbel worden weergegeven wanneer opnieuw verbinding wordt gemaakt met PulseAudio; reparatie voor mogelijke crashes door gebruik na vrijgave bij opnieuw opstarten van PulseAudio/Pipewire; voorkomen dat schuifregelaars in snelle instellingen (volume, enz.) worden gerapporteerd aan toegankelijkheidshulpprogramma's als hun eigen bovenliggende object; weergavevensters waarmee gescrold wordt uitlijnen op het pixelraster om een zichtbaar bibbereffekt tijdens het scrollen te voorkomen">
<correction gnutls28 "Oplossen van een gevaar voor een aanval van het type timing sidechannel[CVE-2023-5981]">
<correction gosa "Nieuwe bovenstroomse stabiele release">
<correction gosa-plugins-sudo "Oplossing voor niet-geïnitialiseerde variabele">
<correction hash-slinger "Probleem met het genereren van TLSA-records oplossen">
<correction intel-graphics-compiler "Compatibiliteit herstellen met  versie van intel-vc-intrinsics in de stabiele release">
<correction iotop-c "De logica in de optie <q>only</q> herstellen; oplossen van een bezig-lus wanneer ESC wordt ingedrukt; reparatie voor de weergave van ASCII-grafieken">
<correction jdupes "Vragen updaten om keuzes te voorkomen die tot onverwacht gegevensverlies kunnen leiden">
<correction lastpass-cli "Nieuwe bovenstroomse stabiele release; bijwerken van certificaatfrommels; ondersteuning voor het lezen van versleutelde URL's toevoegen">
<correction libapache2-mod-python "Ervoor zorgen dat binNMU-versies voldoen aan PEP-440">
<correction libde265 "Oplossingen voor een probleem met segmentatieschending [CVE-2023-27102], problemen met bufferoverloop [CVE-2023-27103 CVE-2023-47471], een probleem met bufferoverlezen [CVE-2023-43887]">
<correction libervia-backend "Oplossing voor fout bij start zonder reeds bestaande configuratie; exec-pad in dbus service-bestand absoluut maken; oplossen van python3-txdbus/python3-dbus als vereisten">
<correction libmateweather "Locations: toevoegen van San Miguel de Tucuman (Argentina); voorspellingszones voor Chicago bijwerken; URL van gegevensserver bijwerken; enkele locatienamen aanpassen">
<correction libsolv "Ondersteuning voor zstd-compressie mogelijk maken">
<correction linux "Updaten naar de bovenstroomse stabiele release 6.1.66; ABI updaten naar 15; [rt] Updaten naar 6.1.59-rt16; X86_PLATFORM_DRIVERS_HP activeren; nvmet: de NQN's die in het connect-commando worden doorgegeven met nul afsluiten [CVE-2023-6121]">
<correction linux-signed-amd64 "Updaten naar de bovenstroomse stabiele release 6.1.66; ABI updaten naar 15; [rt] Updaten naar 6.1.59-rt16; X86_PLATFORM_DRIVERS_HP activeren; nvmet: de NQN's die in het connect-commando worden doorgegeven met nul afsluiten [CVE-2023-6121]">
<correction linux-signed-arm64 "Updaten naar de bovenstroomse stabiele release 6.1.66; ABI updaten naar 15; [rt] Updaten naar 6.1.59-rt16; X86_PLATFORM_DRIVERS_HP activeren; nvmet: de NQN's die in het connect-commando worden doorgegeven met nul afsluiten [CVE-2023-6121]">
<correction linux-signed-i386 "Updaten naar de bovenstroomse stabiele release 6.1.66; ABI updaten naar 15; [rt] Updaten naar 6.1.59-rt16; X86_PLATFORM_DRIVERS_HP activeren; nvmet: de NQN's die in het connect-commando worden doorgegeven met nul afsluiten [CVE-2023-6121]">
<correction llvm-toolchain-16 "Nieuw pakket met uit een recentere versie overgenomen functionaliteit ter ondersteuning van compilaties van nieuwere chroomversies">
<correction lxc "Oplossing van probleem met het maken van kortstondige kopieën">
<correction mda-lv2 "Installatielocatie van LV2-plug-in repareren">
<correction midge "Niet-vrije voorbeeldbestanden verwijderen">
<correction minizip "Problemen met overloop van gehele getallen en heap-overloop oplossen [CVE-2023-45853]">
<correction mrtg "Omgaan met verplaatst configuratiebestand; bijgewerkte vertalingen">
<correction mutter "Nieuwe bovenstroomse stabiele release; repareren van de mogelijkheid om libdecor-vensters bij hun titelbalk te verslepen op aanraakschermen; flikkering en onvolkomenheden in de beeldkwaliteit bij het gebruik van software-rendering oplossen; de prestaties verbeteren van het raster van GNOME Shell-apps door te voorkomen dat andere monitoren dan die waarop de app wordt weergegeven, opnieuw worden bijgewerkt">
<correction nagios-plugins-contrib "Detectie op schijf van de kernel-versie oplossen">
<correction network-manager-openconnect "User Agent toevoegen aan Openconnect VPN voor NetworkManager">
<correction node-undici "Cookie- en hostheaders verwijderen bij cross-origin redirect [CVE-2023-45143]">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse release; oplossen van probleem van null pointer dereference [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla "Nieuwe bovenstroomse release; oplossen van probleem van null pointer dereference [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "Nieuwe bovenstroomse release; oplossen van probleem van null pointer dereference  [CVE-2023-31022]">
<correction nvidia-open-gpu-kernel-modules "Nieuwe bovenstroomse release; oplossen van probleem van null pointer dereference  [CVE-2023-31022]">
<correction opendkim "Oplossen van verwijdering van inkomende Authentication-Results: headers [CVE-2022-48521]">
<correction openrefine "Oplossen van kwetsbaarheid voor uitvoeren van externe code [CVE-2023-41887 CVE-2023-41886]">
<correction opensc "Oplossen van probleem van lezen buiten het bereik [CVE-2023-4535], mogelijke omzeiling van de PIN-code [CVE-2023-40660], problemen met geheugenverwerking [CVE-2023-40661]">
<correction oscrypto "Ontleden van OpenSSL-versie repareren; autopkgtest repareren">
<correction pcs "Reparatie voor <q>resource move</q>">
<correction perl "Probleem met bufferoverschrijding oplossen [CVE-2023-47038]">
<correction php-phpseclib3 "Probleem van denial of service oplossen [CVE-2023-49316]">
<correction postgresql-15 "Nieuwe bovenstroomse stabiele release; probleem van SQL-injectie oplossen [CVE-2023-39417]; reparatie van MERGE om het rijenbeveiligingsbeleid op de juiste manier af te dwingen [CVE-2023-39418]">
<correction proftpd-dfsg "Reparatie van de grootte van de buffers voor het uitwisselen van SSH-sleutels">
<correction python-cogent "Alleen tests waarvoor meerdere CPU's nodig zijn overslaan wanneer deze op een systeem met één CPU worden uitgevoerd">
<correction python3-onelogin-saml2 "Reparatie voor verlopen testladingen">
<correction pyzoltan "Ondersteuning bieden voor bouwen op systeem met één kern">
<correction qbittorrent "UPnP voor web-UI standaard uitzetten in qbittorrent-nox">
<correction qemu "Updaten naar bovenstroomse stabiele release 7.2.7; hw/scsi/scsi-disk: blokgroottes kleiner dan 512 niet toestaan [CVE-2023-42467]">
<correction qpdf "Probleem met gegevensverlies bij sommige octale tekenreeksen tussen aanhalingstekens oplossen">
<correction redis "De vlag ProcSubset=pid die kwetsbaarheid moet verminderen verwijderen uit de systemd-unit omdat deze crashes veroorzaakt">
<correction rust-sd "Ervoor zorgen dat binaire pakketversies correct worden gesorteerd ten opzichte van oudere releases (waar deze werden gebouwd uit een ander bronpakket)">
<correction sitesummary "Timer van systemd gebruken voor het uitvoeren van sitesummary-client als deze beschikbaar is">
<correction speech-dispatcher-contrib "Voxin inschakelen op armhf en arm64">
<correction spyder "Automatische configuratie van de interfacetaal repareren">
<correction symfony "Probleem met sessiefixatie oplossen [CVE-2023-46733]; ontbrekend maskeerteken toevoegen [CVE-2023-46734]">
<correction systemd "Nieuwe bovenstroomse stabiele release">
<correction tbsync "Nieuwe bovenstroomse versie, die de compatibiliteit met nieuwere Thunderbird-versies herstelt">
<correction toil "Slechts één kern aanvragen voor tests">
<correction tzdata "Schrikkelsecondenlijst bijwerken">
<correction unadf "Oplossen van probleem van bufferoverloop [CVE-2016-1243]; oplossen van probleem van codeuitvoering [CVE-2016-1244]">
<correction vips "Oplossen van probleem van null pointer dereference [CVE-2023-40032]">
<correction weborf "Probleem van denial of service oplossen">
<correction wormhole-william "Zwakke tests uitschakelen om bouwfouten te verhelpen">
<correction xen "Nieuwe bovenstroomse stabiele update; oplossen van verschillende beveiligingsproblemen [CVE-2022-40982 CVE-2023-20569 CVE-2023-20588 CVE-2023-20593 CVE-2023-34320 CVE-2023-34321 CVE-2023-34322 CVE-2023-34323 CVE-2023-34325 CVE-2023-34326 CVE-2023-34327 CVE-2023-34328 CVE-2023-46835 CVE-2023-46836]">
<correction yuzu "De bouwvereisten van glslang-tools ontdoen van :native om het mislukken van de compilatie op te lossen">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies ID</th>  <th>Pakket</th></tr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5521 tomcat10>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5525 samba>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5529 slurm-wlm-contrib>
<dsa 2023 5529 slurm-wlm>
<dsa 2023 5531 roundcube>
<dsa 2023 5532 openssl>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5541 request-tracker5>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 jtreg6>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5552 ffmpeg>
<dsa 2023 5553 postgresql-15>
<dsa 2023 5555 openvpn>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5559 wireshark>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5562 tor>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5568 fastdds>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction gimp-dds "Niet langer vereist; geïntegreerd in GIMP">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


