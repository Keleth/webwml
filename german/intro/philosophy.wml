#use wml::debian::template title="Unsere Philosophie: Warum wir es machen und wie wir es machen" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Erik Pfannenstein"

#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">Unsere Mission: Ein freies Betriebssystem zu erschaffen</a></li>
    <li><a href="#how">Unsere Werte: So funktioniert die Debian-Gemeinschaft</a></li>
  </ul>
</div>

<h2><a id="freedom">Unsere Mission: Ein freies Betriebssystem zu erschaffen</a></h2>

<p>
Das Debian-Projekt ist ein Zusammenschluss von Individuen, die auf ein
gemeinsames Ziel hinarbeiten: Wir möchten ein freies Betriebssystem erschaffen,
das frei für alle verfügbar ist. Wenn wir dabei das Wort <q>frei</q> benutzen,
reden wir aber nicht über Geld, sondern wir beziehen uns auf die
<em>Software-Freiheit</em>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">Unsere Definition von Freier Software</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">Was die Free Software Foundation dazu sagt</a></button></p>

<p>
Vielleicht wundern Sie sich, warum sich so viele Leute dafür entscheiden,
immens viel Freizeit in das Schreiben, Verpacken und Betreuen von Software
zu investieren und sie dann zu verschenken. Dafür gibt es eine ganze Reihe von
Gründen, diese hier gehören dazu:
</p>

<ul>
  <li>Manche Leute sind einfach hilfsbereit und haben mit der Einbringung in ein Freie-Software-Projekt ihren Weg gefunden, diese Hilfsbereitschaft auszuleben.</li>
  <li>Viele Entwickler schreiben Programme, um Computer sowie verschiedene Architekturen und Programmiersprachen besser kennenzulernen.</li>
  <li>Einige Unterstützer möchten sich für all die tolle freie Software revanchieren, die sie benutzen dürfen.</li>
  <li>Viele Akademikerinnen und Akademiker schreiben freie Software, um die Ergebnisse ihrer Forschungen bekannt zu machen.</li>
  <li>Unternehmen helfen bei der Betreuung von freier Software, um Einfluss darauf zu nehmen, wie sich die Software weiterentwickelt, oder schnell neue Features zu implementieren.</li>
  <li>Natürlich beteiligen sich die meisten Debian-Entwickler aus Spaß am Gerät!</li>
</ul>

<p>
Obwohl wir an freie Software glauben, respektieren wir, dass Menschen
manchmal unfreie Software auf ihren Computern installieren müssen – ob aus
freien Stücken oder nicht. Wir haben uns entschlossen, diese Menschen nach
Möglichkeit zu unterstützen, daher gibt es eine größer werdende Anzahl von
Paketen, die unfreie Software auf einem Debian-System nachinstallieren.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Wir sind der freien Software verpflichtet und haben diese Verpflichtung in einer Urkunde niedergelegt: unserem <a href="$(HOME)/social_contract">Sozialvertrag</a></p>
</aside>

<h2><a id="how">Unsere Werte: So funktioniert die Gemeinschaft</a></h2>

<p>
Das Debian-Projekt hat mehr als eintausend aktive <a href="people">Entwickler und Unterstützer</a> <a
href="$(DEVEL)/developers.loc">in aller Welt</a>. Ein Projekt mit dieser Größe
braucht eine gute <a href="organization">Orgnaisationsstruktur</a>.
Falls Sie sich also fragen, wie das Debian-Projekt funktioniert und ob
das Debian-Projekt Regeln und Richtlinien besitzt, werfen Sie einen Blick auf
die folgende Stellungnahmen:
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">Die Debian-Satzung</a>: <br>
          Dieses Dokument beschreibt die Organisationsstruktur und erklärt, wie das Debian-Projekt formale Entscheidungen fällt.</li>
        <li><a href="../social_contract">Der Sozialvertrag und die Freie-Software-Richtlinien</a>: <br>
          Der Debian-Sozialvertrag und die Freie-Software-Richtlinien (Debian Free Software Guidelines, DFSG), die Teil des Vertrags sind, beschreiben unsere Verpflichtungen gegenüber der freien Software und der Freie-Software-Gemeinschaft.</li>
        <li><a href="diversity">Die Stellungnahme zur Vielfalt:</a> <br>
          Das Debian-Projekt begrüßt und lädt alle Menschen zur Mitarbeit ein, egal wie sie sich identifizieren oder wie andere sie wahrnehmen.</li>  
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
	<li><a href="../code_of_conduct">Der Verhaltenskodex:</a> <br>
          Wir haben einen Verhaltenskodex für die Teilnehmer unserer Mailinglisten, IRC-Kanäle und allem anderen aufgestellt.</li>
        <li><a href="../doc/developers-reference/">Die Entwicklerreferenz:</a> <br>
          Dieses Dokument bietet einen Überblick über empfohlene Prozeduren und die verfügbaren Ressourcen für Debian-Entwickler und -Betreuer.</li>
        <li><a href="../doc/debian-policy/">Die Debian-Richtlinie:</a> <br>
          Ein Handbuch, welches die Richtlinienanforderungen für die Debian-Distribution beschriebt, bspw. Struktur und Inhalte des Debian-Archivs und technische Ansprüche, denen jedes Paket gerecht werden muss, das aufgenommen werden will.</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span> Debian von innen: <a href="$(DEVEL)/">Die Entwickler-Ecke</a></button></p>
