#use wml::debian::template title="Debian Merchandise" GEN_TIME="yes" BARETITLE=true
#use wml::debian::translation-check translation="3c06310c5d261bd2102e562e1b811d587413a5e4"
# Translator: Martin Schulze <joey@debian.org>
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.
# $Id$

<p>Da Debians Popularität gewachsen ist, haben uns mehrere Anfragen
nach Merchandise-Artikeln erreicht. Da wir eine nicht-kommerzielle
Organisation sind, stellen wir solche Artikel nicht selbst her und vertreiben
sie auch nicht selbst. Glücklicherweise sehen eine Reihe von Firmen
einen potenziellen Markt darin. Die untenstehenden Informationen werden
als ein Service an unsere Benutzer angeboten. Die Sortierung ist rein
alphabetisch und stellt keine Reihung oder Bevorzugung eines speziellen
Händlers dar.</p>

<p>
Einige Händler spenden einen Teil der Erlöse aus dem Verkauf von
Merchandise-Artikeln zurück an Debian. Dies wird unter jedem
Händlereintrag angezeigt. Wir wünschen uns, dass möglichst
viele Händler an Debian spenden.
</p>

<p>
Außerdem haben einige Leute, die an Debian mitarbeiten, sowie lokale
Gruppen Merchandise-Artikel hergestellt, die mit Debian-Logos u.&nbsp;ä.
versehen sind. Manchmal sind hier noch Restbestände vorhanden.
Informationen hierüber finden Sie im
<a href="https://wiki.debian.org/Merchandise">Wiki</a>.
</p>

#include "$(ENGLISHDIR)/events/merchandise.data"

<p>Falls Sie Merchandise-Artikel mit Debian-Ausrichtung vertreiben und
in dieser Liste vertreten sein möchten, schreiben Sie eine E-Mail <strong>auf
Englisch</strong> an &lt;<a href="mailto:events@debian.org">\
events@debian.org</a>&gt;<br />
Bitte teilen Sie uns die folgenden Informationen mit:</p>
<ul>
  <li>Firmenname,</li>
  <li>URL zur Hauptseite,</li>
  <li>URL zu Debian-Merchandise,</li>
  <li>die vertriebenen Artikel,</li>
  <li>Liste der verfügbaren Sprachen auf der Website,</li>
  <li>das Ursprungsland,</li>
  <li>Informationen über Lieferbedingungen (international oder nicht).</li>
</ul>
<p>Wir sind nur an den Debian-orientierten Artikeln interessiert.</p>

<p>Wir nehmen unsere Benutzerdienstleistungen ernst. Wenn wir Beschwerden
zu Ihren Dienstleistungen erhalten, werden Sie von dieser Seite
gelöscht.</p>
