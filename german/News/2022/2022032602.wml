#use wml::debian::translation-check translation="9264c23e43e374bf590c29166dde81515943b562" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 10 aktualisiert: 10.12 veröffentlicht </define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Das Debian-Projekt freut sich, die zwölfte Aktualisierung seiner Oldstable-
Distribution Debian <release> (Codename <q><codename></q>) ankündigen zu dürfen.
Diese Aktualisierung behebt hauptsächlich Sicherheitslücken der 
Oldstable-Veröffentlichung sowie einige ernste Probleme. Für sie sind bereits 
separate Sicherheitsankündigungen veröffentlicht worden, auf die, wenn möglich, 
verwiesen wird. 
</p>

<p>Bitte beachten Sie, dass diese Aktualisierung keine neue Version von Debian <release> 
darstellt, sondern nur einige der enthaltenen Pakete auffrischt. Es gibt keinen Grund, 
<q><codename></q>-Medien zu entsorgen, da deren Pakete nach der Installation mit Hilfe 
eines aktuellen Debian-Spiegelservers auf den neuesten Stand gebracht werden können. 
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele 
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind 
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen 
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Die OpenSSL-Signaturalgorithmus-Prüfung wird strenger</h2>


<p>Die mit dieser Zwischenveröffentlichung mitgelieferte 
OpenSSL-Aktualisierung enthält eine Änderung, die sicherstellt, dass 
die aktive Sicherheitsstufe den angeforderten Signaturalgorithmus 
unterstützt.</p>

<p>Obwol die meisten Anwendungsfälle davon nicht betroffen sein 
werden, ist es trotzdem möglich, dass Fehlermeldungen wegen eines 
nicht unterstützten Signaturalgorithmus erzeugt werden. Das ist 
zum Beispiel der Fall, wenn mit der vorgegebenen Sicherheitsstufe 2 
RSA+SHA1-Signaturen verwendet werden.</p>

<p>In solchen Fällen muss für die einzelne Anfrage oder auch 
allgemeiner explizit auf eine niedrigere Sicherheitsstufe 
gewechselt werden, wofür Konfigurationsänderungen an den 
Anwendungen notwendig sind. Für OpenSSL selbst kann ein 
solcher Stufenwechsel pro Anfrage mit einer Befehlszeile 
wie der folgenden bewerkstelligt werden:</p>

<p>-cipher <q>ALL:@SECLEVEL=1</q></p>

<p>Die entsprechende systemweite Konfiguration befindet sich in
/etc/ssl/openssl.cnf</p>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Oldstable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction apache-log4j1.2 "Sicherheitsprobleme behoben [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307], indem die Unterstützung für das JMSSink-, das JDBCAppender-, das JMSAppender- und das Apache-Chainsaw-Modul entfernt worden sind">
<correction apache-log4j2 "Problem mit Codeausführung aus der Ferne behoben [CVE-2021-44832]">
<correction atftp "Informationsleck abgedichtet [CVE-2021-46671]">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung 10.12">
<correction beads "Neukompilierung gegen das aktualisierte cimg, um mehrere Heap-Puffer-Überläufe zu beseitigen [CVE-2020-25693]">
<correction btrbk "Regression in der Aktualisierung für CVE-2021-38173 beseitigt">
<correction cargo-mozilla "Neues aus Debian 11 zurückportiertes Paket, um beim Kompilieren neuer rust-Versionen zu helfen">
<correction chrony "Einlesen der chronyd-Konfigurationsdatei, die timemaster(8) erzeugt, erlauben">
<correction cimg "Probleme mit Heap-Puffer-Überläufen behoben [CVE-2020-25693]">
<correction clamav "Neue stabile Veröffentlichung der Originalautoren; Problem mit Dienstblockade behoben [CVE-2022-20698]">
<correction cups "<q>an input validation issue might allow a malicious application to read restricted memory</q> (eine Lücke in der Eingabeüberprüfung kann einem Schadprogramm ermöglichen, geschützten Speicher zu lesen) behoben [CVE-2020-10001]">
<correction debian-installer "Neukompilierung gegen oldstable-proposed-updates; Kernel-ABI auf -20 aktualisiert">
<correction debian-installer-netboot-images "Neukompilierung gegen oldstable-proposed-updates">
<correction detox "Verarbeitung großer Dateien auf ARM-Architekturen korrigiert">
<correction evolution-data-server "Absturz bei fehlerhafter Server-Antwort behoben [CVE-2020-16117]">
<correction flac "Lesezugriff außerhalb der Grenzen behoben [CVE-2020-0499]">
<correction gerbv "Anfälligkeit für Codeausführung behoben [CVE-2021-40391]">
<correction glibc "Mehrere Sicherheitskorrekturen des stabilen Zweigs der Originalautoren übernommen; Prüfung auf unterstützte Kernel-Versionen vereinfacht, weil 2.x-Kernel nicht mehr unterstützt werden; Unterstützung für Installation auf Kerneln mit einer Veröffentlichungsnummer oberhalb 255 hinzugefügt">
<correction gmp "Ganzzahl- und Pufferüberlauf behoben [CVE-2021-43618]">
<correction graphicsmagick "Pufferüberlauf behoben [CVE-2020-12672]">
<correction htmldoc "Lesezugriff außerhalb der Grenzen [CVE-2022-0534] und Pufferüberlauf behoben [CVE-2021-43579 CVE-2021-40985]">
<correction http-parser "Unbeabsichtigten ABI-Bruch behoben">
<correction icu "<q>pkgdata</q>-Dienstprogramm wiederhergestellt">
<correction intel-microcode "Enthaltenen Microcode aktualisiert; einige Sicherheitsprobleme umgangen [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction jbig2dec "Pufferüberlauf behoben [CVE-2020-12268]">
<correction jtharness "Neue Version der Originalautoren, um Kompilate neuerer OpenJDK-11-Versionen zu unterstützen">
<correction jtreg "Neue Version der Originalautoren, um Kompilate neuerer OpenJDK-11-Versionen zu unterstützen">
<correction lemonldap-ng "Authentifizierungsprozess in Passwort-Tester-Plugins überarbeitet [CVE-2021-20874]; Empfehlung für gsfonts hinzugefügt, um Captcha zu reparieren">
<correction leptonlib "Dienstblockade [CVE-2020-36277] und Puffer-Überauslese-Probleme (buffer overread) behoben [CVE-2020-36278 CVE-2020-36279 CVE-2020-36280 CVE-2020-36281]">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libencode-perl "Speicherleck in Encode.xs behoben">
<correction libetpan "Anfälligkeit für STARTTLS-Antwort-Injektionen behoben [CVE-2020-15953]">
<correction libextractor "Problem mit ungültigen Lesezugriffen behoben [CVE-2019-15531]">
<correction libjackson-json-java "Anfälligkeit für Codeausführung [CVE-2017-15095 CVE-2017-7525] und externe XML-Entitäten behoben [CVE-2019-10172]">
<correction libmodbus "Probleme mit Lesezugriffen außerhalb der Grenzen behoben [CVE-2019-14462 CVE-2019-14463]">
<correction libpcap "PHB-Kopfzeilenlänge prüfen, bevor sie zum Allozieren von Speicher herangezogen wird [CVE-2019-15165]">
<correction libsdl1.2 "Richtig mit Eingabefokus-Ereignissen umgehen; Pufferüberlaufprobleme [CVE-2019-13616 CVE-2019-7637], Puffer-Überauslese-Probleme behoben [CVE-2019-7572 CVE-2019-7573 CVE-2019-7574 CVE-2019-7575 CVE-2019-7576 CVE-2019-7577 CVE-2019-7578 CVE-2019-7635 CVE-2019-7636 CVE-2019-7638]">
<correction libxml2 "Problem mit Weiternutzung von abgegebenem Speicher (use-after-free) behoben [CVE-2022-23308]">
<correction linux "Neue stabile Veröffentlichung der Originalautoren; [rt] Aktualisierung auf 4.19.233-rt105; ABI auf 20 angehoben">
<correction linux-latest "Aktualisierung auf 4.19.0-20er ABI">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren; [rt] Aktualisierung auf 4.19.233-rt105; ABI auf 20 angehoben">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren; [rt] Aktualisierung auf 4.19.233-rt105; ABI auf 20 angehoben">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren; [rt] Aktualisierung auf 4.19.233-rt105; ABI auf 20 angehoben">
<correction llvm-toolchain-11 "Neues aus Debian 11 zurückportiertes Paket, um beim Kompilieren neuer rust-Versionen zu helfen">
<correction lxcfs "Fehlerhafte Berichterstattung der Swap-Verwendung korrigiert">
<correction mailman "Anfälligkeit für seitenübergreifendes Skripting beseitigt [CVE-2021-43331]; <q>a list moderator can crack the list admin password encrypted in a CSRF token</q> (ein Listenmoderator kann das in einem CSRF-Token verschlüsselte Listen-Adminpasswort knacken) behoben [CVE-2021-43332]; potenziellen CSRF-Angriff eines Listen-Mitglieds oder -Moderatoren gegen einen Listen-Administrator unterbunden [CVE-2021-44227]; Regressionen in den Korrekturen für CVE-2021-42097 und CVE-2021-44227 beseitigt">
<correction mariadb-10.3 "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction node-getobject "Anfälligkeit für Prototype Pollution beseitigt [CVE-2020-28282]">
<correction opensc "Zugriff außerhalb der Grenzen [CVE-2019-15945 CVE-2019-15946], Absturz durch Lesen unbekannten Speichers [CVE-2019-19479], Doppel-free [CVE-2019-20792], Pufferüberläufe behoben [CVE-2020-26570 CVE-2020-26571 CVE-2020-26572]">
<correction openscad "Pufferüberläufe im STL-Parser behoben [CVE-2020-28599 CVE-2020-28600]">
<correction openssl "Neue Veröffentlichung der Originalautoren">
<correction php-illuminate-database "Problem beim Query-Binding behoben [CVE-2021-21263] sowie eine SQL-Injection-Lücke bei Verwendung mit Microsoft SQL Server">
<correction phpliteadmin "Problem mit seitenübergreifendem Skripting behoben [CVE-2021-46709]">
<correction plib "Ganzzahlüberlauf behoben [CVE-2021-38714]">
<correction privoxy "Speicherleck [CVE-2021-44540] und Problem mit seitenübergreifendem Skripting behoben [CVE-2021-44543]">
<correction publicsuffix "Enthaltene Daten aktualisiert">
<correction python-virtualenv "Nicht versuchen, pkg_resources von PyPI zu installieren">
<correction raptor2 "Array-Zugriff außerhalb der Grenzen behoben [CVE-2020-25713]">
<correction ros-ros-comm "Anfälligkeit für Dienstblockaden abgestellt [CVE-2021-37146]">
<correction rsyslog "Heap-Überläufe beseitigt [CVE-2019-17041 CVE-2019-17042]">
<correction ruby-httpclient "Zertifikatsspeicher des Systems benutzen">
<correction rust-cbindgen "Neue stabile Veröffentlichung der Originalautoren, um die Kompilierung neuerer Versionen von firefox-esr und thunderbird zu unterstützen">
<correction rustc-mozilla "Neues Quellpaket, um die Kompilierung neuerer Versionen von firefox-esr und thunderbird zu unterstützen">
<correction s390-dasd "Aufhören, die veraltete Option -f an dasdfmt durchzureichen">
<correction spip "Anfälligkeit für seitenübergreifendes Skripting beseitigt">
<correction tzdata "Daten für Fiji und Palästina aktualisiert">
<correction vim "Fähigkeit zum Auführen von Code im <q>restricted mode</q> beseitigt [CVE-2019-20807], außerdem Anfälligkeiten für Pufferüberläufe [CVE-2021-3770 CVE-2021-3778 CVE-2021-3875], Verwendung von abgegebenem Speicher (use after free) [CVE-2021-3796]; versehentlich mitgelieferte Korrektur wieder entfernt">
<correction wavpack "Verwendung von uninitialisierten Werten verhindert [CVE-2019-1010317 CVE-2019-1010319]">
<correction weechat "Mehrere Probleme mit Dienstblockaden beseitigt [CVE-2020-8955 CVE-2020-9759 CVE-2020-9760 CVE-2021-40516]">
<correction wireshark "Mehrere Sicherheitsprobleme in dissectors behoben [CVE-2021-22207 CVE-2021-22235 CVE-2021-39921 CVE-2021-39922 CVE-2021-39923 CVE-2021-39924 CVE-2021-39928 CVE-2021-39929]">
<correction xterm "Pufferüberlauf behoben [CVE-2022-24130]">
<correction zziplib "Dienstblockade behoben [CVE-2020-18442]">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>
Diese Revision fügt der Oldstable-Veröffentlichung die folgenden Sicherheitsaktualisierungen 
hinzu. Das Sicherheitsteam hat bereits für jede davon eine Ankündigung veröffentlicht:
</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2019 4513 samba>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4989 strongswan>
<dsa 2021 4990 ffmpeg>
<dsa 2021 4991 mailman>
<dsa 2021 4993 php7.3>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4997 tiff>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5005 ruby-kaminari>
<dsa 2021 5006 postgresql-11>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5014 icu>
<dsa 2021 5015 samba>
<dsa 2021 5016 nss>
<dsa 2021 5018 python-babel>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5021 mediawiki>
<dsa 2021 5022 apache-log4j2>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5032 djvulibre>
<dsa 2022 5035 apache2>
<dsa 2022 5036 sphinxsearch>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5043 lxml>
<dsa 2022 5047 prosody>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5065 ipython>
<dsa 2022 5066 ruby2.5>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5078 zsh>
<dsa 2022 5081 redis>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5093 spip>
<dsa 2022 5096 linux-latest>
<dsa 2022 5096 linux-signed-amd64>
<dsa 2022 5096 linux-signed-arm64>
<dsa 2022 5096 linux-signed-i386>
<dsa 2022 5096 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5103 openssl>
<dsa 2022 5105 bind9>
</table>


<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer Kontrolle liegen:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction angular-maven-plugin "Nützt nichts mehr">
<correction minify-maven-plugin "Nützt nichts mehr">
</table>

<h2>Debian-Installer</h2>
<p>Der Installer wurde aktualisiert, damit er die Änderungen enthält, die mit 
dieser Zwischenveröffentlichung in Oldstable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Oldstable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Änderungen für die Oldstable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsaktualisierungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier 
Software, die ihre Zeit und Mühen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter 
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail an
&lt;press@debian.org&gt; oder kontaktieren Sie das 
Stable-Veröffentlichungs-Team unter &lt;debian-release@lists.debian.org&gt;.</p>
