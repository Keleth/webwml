#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 12 aktualisiert: 12.5 veröffentlicht</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die fünfte Aktualisierung seiner Stable-Distribution
Debian <release> (Codename <q><codename></q>) ankündigen zu dürfen. Diese
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken der Stable-Veröffentlichung
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen
veröffentlicht worden, auf die, wo möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt.
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Stable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction apktool "Eigenmächtige Dateischreibzugriffe mit schädlichen Ressourcen-Namen verhindert [CVE-2024-21633]">
<correction atril "Absturz beim Öffnen einiger epub-Dateien behoben; Laden des Index für gewisse epub-Dokumente überarbeitet; Ausweichmethode in check_mime_type für fehlerhafte epub-Dateien eingeführt; zum Extrahieren von Dokumenten libarchive statt externem Befehl verwenden [CVE-2023-51698]">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung 12.5">
<correction caja "Desktop-Renderartefakte nach Änderungen der Auflösung behoben; Verwendung des <q>informellen</q> Datumsformats überarbeitet">
<correction calibre "<q>HTML-Eingabe: standardmäßig keine Ressourcen von außerhalb der Verzeichnisstruktur, welche ihre Wurzel im Elternverzeichnis der HTML-Datei hat, hinzufügen</q> [CVE-2023-46303]">
<correction compton "Empfehlung für picom entfernt">
<correction cryptsetup "cryptsetup-initramfs: Unterstützung für komprimierte Kernelmodule hinzugefügt; cryptsetup-suspend-wrapper: Bei fehlendem Verzeichnis /lib/systemd/system-sleep nicht mit Fehler aussteigen; add_modules(): Logik zum Weglassen des Suffixes an die von initramfs-tools angepasst">
<correction debian-edu-artwork "Illustrationen mit Emerald-Thema für Debian Edu 12 bereitgestellt">
<correction debian-edu-config "Neue Version der Originalautoren">
<correction debian-edu-doc "Update included documentation and translations">
<correction debian-edu-fai "Neue Version der Originalautoren">
<correction debian-edu-install "Neue Version der Originalautoren; Sicherheits-sources.list korrigiert">
<correction debian-installer "Linux-Kernel-ABI auf 6.1.0-18 angehoben; Neukompilierung gegen proposed-updates">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-ports-archive-keyring "Signierschlüssel Debian Ports Archive Automatic Signing Key (2025) hinzugefügt">
<correction dpdk "Neue stabile Version der Originalautoren">
<correction dropbear "<q>Terrapin-Angriff</q> abgewehrt [CVE-2023-48795]">
<correction engrampa "Mehrere Speicherlecks geflickt; <q>Speichern als</q>-Funktionalität des Archivs überarbeitet">
<correction espeak-ng "Pufferüberlauf [CVE-2023-49990 CVE-2023-49992 CVE-2023-49993], Pufferunterlauf [CVE-2023-49991], Fließkomma-Ausnahme [CVE-2023-49994] behoben">
<correction filezilla "<q>Terrapin</q>-Exploit blockiert [CVE-2023-48795]">
<correction fish "Sicher mit nicht-druckbaren Unicode-Zeichen umgehen, wenn sie als Befehlsersatz eingegeben werden [CVE-2023-49284]">
<correction fssync "Unzuverlässige Tests abgeschaltet">
<correction gnutls28 "Assertionsfehler behoben, der auftrat, wenn eine Zertifikatskette mit vielen über Kreuz gehenden Signierungen überprüft wurde [CVE-2024-0567]; Timing-Seitenkanal geschlossen [CVE-2024-0553]">
<correction indent "Heap-basierten Pufferüberlauf behoben [CVE-2024-0911]">
<correction isl "Verwendung auf älteren CPUs überarbeitet">
<correction jtreg7 "Neues Quellpaket, um Kompilierungen von openjdk-17 zu unterstützen">
<correction libdatetime-timezone-perl "Enthaltene Zeitzonendaten aktualisiert">
<correction libde265 "Pufferüberläufe behoben [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libfirefox-marionette-perl "Kompatibilität mit neueren firefox-esr-Versionen verbessert">
<correction libmateweather "URL für aviationweather.gov korrigiert">
<correction libspreadsheet-parsexlsx-perl "Mögliche Speicherbombe entschärft [CVE-2024-22368]; externe XML-Entität (XEE) behoben [CVE-2024-23525]">
<correction linux "Neue stabile Version der Originalautoren; ABI auf 18 angehoben">
<correction linux-signed-amd64 "Neue stabile Version der Originalautoren; ABI auf 18 angehoben">
<correction linux-signed-arm64 "Neue stabile Version der Originalautoren; ABI auf 18 angehoben">
<correction linux-signed-i386 "Neue stabile Version der Originalautoren; ABI auf 18 angehoben">
<correction localslackirc "Autorisierungs- und Cookie-Kopfzeilen an den Websocket senden">
<correction mariadb "Neue stabile Version der Originalautoren; Dienstblockade behoben [CVE-2023-22084]">
<correction mate-screensaver "Speicherlecks behoben">
<correction mate-settings-daemon "Speicherlecks behoben; Grenzwerte für hohe DPI-Werte entschärft; Umgang mit mehreren rfkill-Ereignissen überarbeitet">
<correction mate-utils "Diverse Speicherlecks behoben">
<correction monitoring-plugins "Verhalten des check_http-Plugins korrigiert, wenn <q>--no-body</q> verwendet wird und die Antwort der Gegenstelle abgehackt ist">
<correction needrestart "Regression bei Microcode-Prüfung bei AMD-CPUs behoben">
<correction netplan.io "autopkgtests mit neueren systemd-Versionen überarbeitet">
<correction nextcloud-desktop "<q>Synchronisierungsfehlschläge bei Sonderzeichen wie ':'</q> behoben; Benachrichtigungen wegen Zwei-Faktor-Authentifizierung überarbeitet">
<correction node-yarnpkg "Zusammenspiel mit Commander 8 verbessert">
<correction onionprobe "Initialisierung von Tor bei Verwendung gehashter Passwörter repariert">
<correction pipewire "Wenn verfügbar, malloc_trim() zur Freigabe von Speicher verwenden">
<correction pluma "Speicherlecks geflickt; Doppel-Aktivierung von Erweiterungen abgestellt">
<correction postfix "Neue stabile Version der Originalautoren; SMTP-Schmuggel angegangen [CVE-2023-51764]">
<correction proftpd-dfsg "Abwehr für Terrapin-Angriff eingebaut [CVE-2023-48795]; L[CVE-2023-51713]">
<correction proftpd-mod-proxy "Abwehr für Terrapin-Angriff eingebaut [CVE-2023-48795]">
<correction pypdf "Endlosschleife behoben [CVE-2023-36464]">
<correction pypdf2 "Endlosschleife behoben [CVE-2023-36464]">
<correction pypy3 "rpython-Assertionsfehler in der JIT vermeiden, der auftritt, wenn sich Ganzzahlbereiche in einer Schleife nicht überlappen">
<correction qemu "Neue stabile Version der Originalautoren; virtio-net: vnet-Kopfzeilen beim Leeren des TX richtig kopieren [CVE-2023-6693]; Nullzeiger-Dereferenzierung behoben [CVE-2023-6683]; Korrektur, die Probleme in der Anhalten/Fortsetzen-Funktion verursacht hat, zurückgerollt">
<correction rpm "BerkeleyDB-Nur-Lese-Backend aktiviert">
<correction rss-glx "Bildschirmschoner in /usr/libexec/xscreensaver abspeichern; GLFinish() vor glXSwapBuffers() aufrufen">
<correction spip "Zwei Anfälligkeiten für seitenübergreifendes Skripting behoben">
<correction swupdate "Erlangen von root-Rechten durch unpassenden Socket-Modus unterbunden">
<correction systemd "Neue stabile Version der Originalautoren; Fehlen von Verifikation in systemd-resolved korrigiert [CVE-2023-7008]">
<correction tar "Begrenzungsprüfung im base-256-Dekodierer [CVE-2022-48303], Umgang mit erweiterten Kopfzeilen-Präfixen [CVE-2023-39804] überarbeitet">
<correction tinyxml "Assertionsproblem behoben [CVE-2023-34194]">
<correction tzdata "Neue stabile Version der Originalautoren">
<correction usb.ids "Enthaltene Datenliste aktualisiert">
<correction usbutils "usb-devices überarbeitet, welches nicht alle USB-Geräte aufgelistet hat">
<correction usrmerge "Biarch-Verzeichnisse löschen, wenn sie nicht gebraucht werden; convert-etc-shells auf umgestellten Systemen kein weiteres Mal ausführen; eingehängtes /lib/modules auf Xen-Systemen verarbeitetn; Fehlerberichterstattung überarbeitet; versionsabhängige Konflikte mit libc-bin, dhcpcd, libparted1.8-10 und lustre-utils hinterlegt">
<correction wolfssl "Sicherheitsproblem, bei dem der Client weder PSK- noch KSE-Erweiterungen gesendet hat, behoben [CVE-2023-3724]">
<correction xen "Neue stabile Version der Originalautoren; Sicherheitskorrekturen [CVE-2023-46837 CVE-2023-46839 CVE-2023-46840]">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision nimmt an der Stable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen vor. Das Sicherheitsteam hat bereits für
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5578 ghostscript>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5583 gst-plugins-bad1.0>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5589 node-undici>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5593 linux-signed-amd64>
<dsa 2024 5593 linux-signed-arm64>
<dsa 2024 5593 linux-signed-i386>
<dsa 2024 5593 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5601 php-phpseclib3>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5607 chromium>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5609 slurm-wlm>
<dsa 2024 5610 redis>
<dsa 2024 5611 glibc>
<dsa 2024 5612 chromium>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>

<h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält,
die mit dieser Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern
Freier Software, die ihre Zeit und Mühen einbringen, um das
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das
Stable-Veröffentlichungs-Team (auf Englisch)
unter &lt;debian-release@lists.debian.org&gt;.</p>
