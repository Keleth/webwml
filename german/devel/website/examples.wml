#use wml::debian::template title="Beispiele"
#use wml::debian::translation-check translation="ceca0da4950426a38fa87b51efcfd25749d8995a"
# $Id$
# Translator: Gerfried Fuchs <alfie@debian.org> 2002-04-03
# Updated: Holger Wansing <hwansing@mailbox.org>, 2021.

<h3>Beispiel, wie man eine Übersetzung beginnt</h3>

<p>Für dieses Beispiel wird Deutsch verwendet:</p>

<pre>
   git pull
   cd webwml
   mkdir german
   cd german
   cp ../english/.wmlrc ../english/Make.* .
   echo '<protect>include $(subst webwml/german,webwml/english,$(CURDIR))/Makefile</protect>' &gt; Makefile
   mkdir po
   git add Make* .wmlrc
   cp Makefile po
   make -C po init-po
   git add po/Makefile po/*.de.po
</pre>

<p>Editieren Sie die <tt>.wmlrc</tt> Datei und ändern Sie Folgendes:
<ul>
  <li>'-D CUR_LANG=English' auf '-D CUR_LANG=German'</li>
  <li>'-D CUR_ISO_LANG=en' auf '-D CUR_ISO_LANG=de'</li>
  <li>'-D CUR_LOCALE=en_US' auf '-D CUR_LOCALE=de_DE.UTF-8'</li>
  <li>'-D CHARSET=iso-8859-1' auf '-D CHARSET=utf-8'<br />
      Deutsch verwendet als Zeichensatz UTF-8, da damit wesentlich mehr
      Zeichen darstellbar sind als mit ISO-8859-1. Es ist wahrscheinlich,
      dass neue Sprachen diese Einstellung ebenfalls anpassen müssen.</li>
</ul>

<p>Editieren Sie Make.lang und ändern Sie 'LANGUAGE := en' auf 'LANGUAGE :=
de'. Falls Sie in eine Sprache übersetzen, die einen Multi-Byte Zeichensatz
verwendet, möchten Sie vielleicht einige andere Einstellungen in dieser Datei
ändern, für weitere Informationen lesen Sie ../Makefile.common und eventuell
andere funktionstüchtige Beispiele (Übersetzungen wie Chinesisch).</p>

<p>Gehen Sie nach german/po und übersetzen Sie die Einträge in den PO-Dateien.
Dies sollte recht geradlinig sein.</p>

<p>Vergessen Sie nie, das Makefile in jedes Verzeichnis zu kopieren, das Sie
übersetzen. Das ist notwendig, da das Programm <code>make</code> dazu
verwendet wird, die .wml Dateien nach HTML zu übersetzen, und
<code>make</code> die Makefiles verwendet.</p>

<p>Wenn Sie mit dem Hinzufügen und Editieren der Seiten fertig sind, führen
Sie</p>
<pre>
   git commit -m "Add your comment here"
   git push
</pre>
<p>im webwml-Verzeichnis aus. Sie können nun anfangen, die Seiten zu
übersetzen.</p>


<h3>Beispielsübersetzung einer Seite</h3>

<p>Eine deutsche Übersetzung des Gesellschaftsvertrags wird in diesem Beispiel
verwendet:</p>

<pre>
   cd webwml/german
   ./copypage.pl english/social_contract.wml
   cd german
</pre>

<p>Dies wird automatisch die Kopfzeile translation-check hinzufügen, die auf
die Version der ursprünglich kopierten Datei zeigt. Es wird dabei auch das
Zielverzeichnis und das Makefile erstellt, falls noch nicht vorhanden.</p>

<p>Editieren Sie social_contract.wml und übersetzen Sie den Text. Versuchen
Sie nicht, Links zu übersetzen oder irgendwie zu verändern &ndash; falls Sie dort
etwas ändern wollen, bitten Sie darum auf der debian-www Liste. Wenn Sie
fertig sind, geben Sie Folgendes ein:</p>

<pre>
   git add social_contract.wml
   git commit -m "Translated social contract to german"
   git push

</pre>


<h3>Beispiel des Hinzufügens eines neuen Verzeichnisses</h3>

<p>Dieses Beispiel zeigt, wie der deutschen Übersetzung das intro/ Verzeichnis
hinzugefügt wird:</p>

<pre>
   cd webwml/german
   mkdir intro
   cd intro
   cp ../Makefile .
   git add Makefile
   git commit -m "Added the intro dir to git"
   git push

</pre>

<p>
Vergewissern Sie sich, dass das neue Verzeichnis das Makefile hat und dass es
ins git übergeben wurde. Anderenfalls wird der Aufruf von make für jeden einen
Fehler liefern, der dies versucht.</p>


 <h3>Beispiel eines Konflikts</h3>

 <p>Dieses Beispiel zeigt die Übergabe einer Datei, die nicht funktioniert,
 da die Kopie im Depot geändert wurde, seit Sie das letzte Mal <kbd>git
 pull</kbd> ausgeführt haben.</p>

 <p>Sie haben Änderungen an der Datei foo.wml durchgeführt. Dann:</p>

 <pre>
    git add foo.wml
    git commit -m "fixed a broken link"
    git push
 </pre>

 <p>gibt folgendes aus:</p>

 <pre>
To salsa.debian.org:webmaster-team/webwml.git
 ! [rejected]                master -> master (fetch first)
error: failed to push some refs to 'git@salsa.debian.org:webmaster-team/webwml.git'
 </pre>

 <p>oder etwas Ähnliches :)
       <br />
       <br />
    Dies bedeutet, dass Ihre Änderungen <strong>nicht</strong> an das
    git-Repository übergeben wurden, und zwar aufgrund von Konflikten.
       <br />
    Sie müssen kontrollieren, was schief gelaufen ist, die Konflikte beseitigen, und dann
    erneut versuchen, die Änderungen zu übergeben
    (mittels git add ...; git commit -m "Your comment"; git push).</p>



