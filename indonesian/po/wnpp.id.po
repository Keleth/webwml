# Mahyuddin Susanto <udienz@gmail.com>, 2010.
# Izharul Haq <atoz.chevara@yahoo.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml wnpp\n"
"PO-Revision-Date: 2012-02-02 11:32+0700\n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: Indonesian <debian-l10n-indonesian@lists.debian.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Virtaal 0.6.1\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Tidak ada permintaan untuk adopsi"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "paket yang tidak mempunyai keterkaitan"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Tidak ada antrian paket untuk diadopsi"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Tidak ada antrian paket untuk dipaketkan"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Tidak ada permintaan paket"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Tidak ada permintaan bantuan"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "Diadopsi sejak hari ini."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "Diadopsi sejak kemarin."

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "Diadopsi dalam %s hari."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr "Diadopsi dalam %s hari, aktivitas terakhir hari ini."

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "Diadopsi dalam %s hari, aktivitas terakhir kemarin."

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr "Diadopsi dalam %s hari, aktivitas terakhir %s hari yang lalu."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "dipersiapkan mulai hari ini."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "dipersiapkan sejak kemarin."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "dipersiapkan dalam %s hari."

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "dipersiapkan dalam %s hari, aktivitas terakhir hari ini."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "dipersiapkan dalam %s hari, aktivitas kemarin."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr "dipersiapkan dalam %s hari, aktivitas terakhir %s hari yang lalu."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
#, fuzzy
msgid "adoption requested since today."
msgstr "Diadopsi sejak hari ini."

#: ../../english/template/debian/wnpp.wml:89
#, fuzzy
msgid "adoption requested since yesterday."
msgstr "Diadopsi sejak kemarin."

#: ../../english/template/debian/wnpp.wml:93
#, fuzzy
msgid "adoption requested since %s days."
msgstr "Diadopsi sejak hari ini."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
#, fuzzy
msgid "orphaned since today."
msgstr "Diadopsi sejak hari ini."

#: ../../english/template/debian/wnpp.wml:102
#, fuzzy
msgid "orphaned since yesterday."
msgstr "Diadopsi sejak kemarin."

#: ../../english/template/debian/wnpp.wml:106
#, fuzzy
msgid "orphaned since %s days."
msgstr "dipersiapkan mulai hari ini."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "diminta hari ini."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "diminta kemarin."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "diminta sejak %s hari yang lalu."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "informasi paket"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr ""
