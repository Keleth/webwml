#use wml::debian::template title="Debian &ldquo;bullseye&rdquo;-udgivelsesoplysninger"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e"

<p>Debian <current_release_bullseye> blev udgivet <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 blev oprindelig udgivet den <:=spokendate('2021-08-14'):>."
/>
Udgivelsen indeholdt mange større ændringer, som er beskrevet i vores 
<a href="$(HOME)/News/2021/20210814">pressemeddelelse</a> og i 
<a href="releasenotes">Udgivelsesbemærkningerne</a>.</p>

<p><strong>Debian 11 er blevet erstattet af 
<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Sikkerhedsopdateringer er ophørt pr. <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Bullseye drager også nytte af Long Term Support (LTS) indtil 
#udgangen af xxxxx 20xx.  LTS er begrænset til i386, amd64, armel, armhf 
#og arm64.
#Alle andre arkitekturer er ikke længere understøttet i bullseye.
#For flere oplysninger, se <a href="https://wiki.debian.org/LTS">LTS-afsnittet 
#i Debians wiki</a>.
#</strong></p>

<p>For at hente og installere Debian, se siden med 
<a href="debian-installer/">installeringsoplysninger</a> og 
<a href="installmanual">Installeringsvejledningen</a>.  For at opgradere fra 
en ældre Debian-udgive, se vejledningen i <a href="releasenotes">\
Udgivelsesbemærkningerne</a>.</p>

### Activate the following when LTS period starts.
#<p>Arkitekturer understøttet under Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Følgende computerarkitekturer er understøttet i denne udgave:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Imod vores ønsker, kan der være tilbageværende problemer i udgivelsen, selv 
om den er blevet erklæret <em>stabil</em>.  Vi har lavet en <a href="errata">\
liste over de største kendte problemer</a>, og du kan kan altid 
<a href="../reportingbugs">rapportere andre problemer</a> til os.</p>

<p>Sidst men ikke mindst, har vi en liste over <a href="credits">folk der har gjort 
denne udgave mulig</a>.</p>
