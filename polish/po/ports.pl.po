# translation of ports.pl.po to polski
# Wojciech Zareba <wojtekz@comp.waw.pl>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: ports.pl\n"
"PO-Revision-Date: 2012-08-17 18:09+0200\n"
"Last-Translator: Marcin Owsiany <porridge@debian.org>\n"
"Language-Team: polski <debian-l10n-polish@lists.debian.org>\n"
"Language: polish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian dla Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian dla PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "Płyty CD"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian dla IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Kontakt"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "Procesory"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Podziękowania"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Rozwój"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Dokumentacja"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Instalacja"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr "Konfiguracja"

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Odsyłacze"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Wiadomości"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Proces adaptacji"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Adaptacje"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Problemy"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Mapa oprogramowania"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Status"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Dostawa"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Systemy"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian dla PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian dla Sparc"

#~ msgid "Debian GNU/NetBSD for Alpha"
#~ msgstr "Debian GNU/NetBSD dla Alpha"

#~ msgid "Debian GNU/NetBSD for i386"
#~ msgstr "Debian GNU/NetBSD dla i386"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian na laptopach"

#~ msgid "People"
#~ msgstr "Ludzie"

#~ msgid "Why"
#~ msgstr "Dlaczego"
