#use wml::debian::template title="Les miroirs de Debian"

#use wml::debian::translation-check translation="34422e94132789c2bcd0f6d41dc8680b49c7f3ab" maintainer="Jean-Paul Guillonneau"
#traducteurs : voir journal

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Les miroirs Debian sont entretenus par des volontaires. Si vous avez la possibilité de donner de l'espace disque et de la connectivité, vous pouvez envisager de créer un miroir pour rendre Debian plus accessible. Consultez la page sur <a href="ftpmirror">la création d'un miroir Debian</a> pour plus d'informations.</p>
</aside>

<p>Debian est distribuée (c'est-à-dire copiée) sur des centaines de serveurs
tout autour du monde fournissant tous le même contenu. De cette manière, nous
pouvons fournir le meilleur accès possible à notre archive.</p>

<p>Vous trouverez le contenu suivant sur nos serveurs miroir :</p>

<dl>
<dt><strong>Les paquets Debian</strong> (<code>debian/</code>)</dt>
  <dd>L'ensemble des paquets Debian : cela comprend la vaste majorité
      des paquets <code>.deb</code>, le nécessaire à l'installation et les
      sources.
      <br>
      Consultez la liste des <a href="list">miroirs Debian</a> qui contiennent
      l'archive <code>debian/</code>.
  </dd>
<dt><strong>Les images de CD-ROM</strong> (<code>debian-cd/</code>)</dt>
  <dd>Le référentiel des images de CD-ROM&nbsp;: fichiers Jigdo et fichiers
      d'images ISO.
      <br>
      Consultez la liste des <a href="$(HOME)/CD/http-ftp/#mirrors">miroirs Debian</a>
      qui contiennent l'archive <code>debian-cd/</code>.
  </dd>
<dt><strong>Les anciennes versions</strong> (<code>debian-archive/</code>)</dt>
  <dd>Les archives des versions anciennes de Debian, publiées dans le passé.
      <br>
      Consultez les <a href="$(HOME)/distrib/archive">archives de la distribution</a>
      pour plus d'informations.
  </dd>
</dl>

<p>Aperçu de l’<a href="https://mirror-master.debian.org/status/mirror-status.html">
état des miroirs Debian</a></p>
