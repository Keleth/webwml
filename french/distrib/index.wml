#use wml::debian::template title="Obtenir Debian"
#use wml::debian::translation-check translation="47e0a86f4e71194a409b72e7ed1faaa3dc3160d5" maintainer="Jean-Paul Guillonneau"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Cette page présente les options d’installation pour la version stable de Debian.</p>

<ul>
<li> <a href="../CD/http-ftp/#mirrors">Téléchargement depuis les miroirs</a> des images
d’installation
<li> <a href="../releases/stable/installmanual">Manuel d’installation</a>
avec des instructions détaillées
<li> <a href="../releases/stable/releasenotes">Notes de publication</a>
<li> <a href="../devel/debian-installer/">Images pour Debian testing</a>
<li> <a href="../CD/verify">Vérification de l'authenticité des images Debian</a>
</ul>


<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Télécharger une image d’installation</a></h2>
    <ul>
      <li>Une <a href="netinst"><strong>image d’installation de taille
réduite</strong></a> peut être téléchargée rapidement et enregistrée sur disque amovible.
Pour cela, une machine avec une connexion à Internet est nécessaire.
	<ul class="quicklist downlist">
	  <li><a title="Télécharger l’installateur pour les PC 64 bits d’Intel et d’AMD"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">ISO « netinst » pour PC 64 bits</a></li>
	  <li><a title="Télécharger l’installateur pour les PC normaux 32 bits d’Intel et d’AMD"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">ISO « netinst » pour PC 32 bits</a></li>
	  <li><a title="Télécharger les torrents de CD pour les PC 64 bits d’Intel et d’AMD"
	         href="<stable-images-url/>/amd64/bt-cd/">Torrents « netinst » pour PC 64 bits</a></li>
	  <li><a title="Télécharger les torrents de CD pour les PC normaux 32 bits d’Intel et d’AMD"
		 href="<stable-images-url/>/i386/bt-cd/">Torrents « netinst » pour PC 32 bits</a></li>
	</ul>
      </li>
      <li>Une <a href="../CD/"><strong>image d’installation complète</strong></a>
de taille plus importante contient plus de paquets et facilite l'installation
sur des machines sans accès à Internet.
	<ul class="quicklist downlist">
	  <li><a title="Télécharger les torrents de DVD pour les PC 64 bits d’Intel et d’AMD"
	         href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">ISO DVD-1 pour PC 64 bits</a></li>
	  <li><a title="Télécharger les torrents de DVD pour les PC 32 bits d’Intel et d’AMD"
		 href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">ISO DVD-1 pour PC 32 bits</a></li>
 <li><a title="Télécharger les torrents de DVD pour les PC 64 bits d’Intel et d’AMD"
	         href="<stable-images-url/>/amd64/bt-dvd/">torrents pour PC 64 bits (DVD)</a></li>
	  <li><a title="Télécharger les torrents de DVD pour les PC normaux 32 bits d’Intel et d’AMD"
		 href="<stable-images-url/>/i386/bt-dvd/">torrents pour PC 32 bits (DVD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
<div class="item col50 lastcol">
    <h2><a href="../CD/live/">Essayer Debian en autonome avant l'installation</a></h2>
<p>
Vous pouvez essayer Debian en amorçant un système autonome à partir d'un CD,
d'un DVD ou d'une clef USB sans installer un seul fichier sur l'ordinateur.
Vous pouvez aussi exécuter l’<a href="https://calamares.io">installateur
Calamares</a>, seulement disponible pour les PC 64 bits. Consultez
<a href="../CD/live#choose_live">les explications sur cette méthode</a>.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Télécharger une image autonome de Gnome pour les PC 64 bits d’Intel et d’AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">image autonome de Gnome</a></li>
      <li><a title="Télécharger une image autonome d’Xfce pour les PC 64 bits d’Intel et d’AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">image autonome d’Xfce</a></li>
      <li><a title="Télécharger une image autonome de KDE pour les PC 64 bits d’Intel et d’AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">image autonome de KDE</a></li>
      <li><a title="Télécharger d’autres images autonomes pour les PC 64 bits d’Intel et d’AMD"
            href="<live-images-url/>/amd64/iso-hybrid/">autres images autonomes</a></li>
      <li><a title="Télécharger des torrents d’image autonome pour les PC 64 bits d’Intel et d’AMD"
          href="<live-images-url/>/amd64/bt-hybrid/">torrents d’images autonomes</a></li>
</ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">
Acheter un jeu de CD ou DVD ou une clé USB auprès d'un des distributeurs de
médias d’installation de Debian</a></h2>

<p>
Beaucoup de distributeurs vendent la distribution pour moins de 5 dollars
américains plus les frais de port (voyez sur leurs pages web s'ils livrent à
l'étranger).
</p>

<p>Voici les avantages fondamentaux des CD :</p>

  <ul>
  <li>vous pouvez installer Debian sur des machines sans connexion Internet ;</li>
  <li>vous pouvez installer Debian sans avoir à télécharger tous les paquets.</li>
   </ul>

   <h2><a href="pre-installed">Acheter un ordinateur avec Debian préinstallée</a></h2>
   <p>Il y a de nombreux avantages à cela :</p>
   <ul>
    <li>vous n'avez pas besoin d'installer Debian ;</li>
    <li>l'installation est préconfigurée pour correspondre au matériel ;</li>
    <li>le marchand peut vous fournir une aide technique.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Utiliser une image
     pour l’informatique dématérialisée</a></h2>
    <p>Une <a href="https://cloud.debian.org/images/cloud/"><strong> image
     pour l’informatique dématérialisée</strong></a>, construite par l’équipe
     Debian pour l’informatique dématérialisée, peut être utilisée directement sur :
    </p>
    <ul>
      <li>votre fournisseur OpenStack, aux formats qcow2 ou raw ;
      <ul class="quicklist downlist">
	   <li>AMD/Intel 64 bits (<a title="Image qcow2 OpenStack pour AMD/Intel 64 bits" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="Image raw OpenStack pour AMD/Intel 64 bits" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
       <li>ARM 64 bits (<a title="Image qcow2 OpenStack pour ARM 64 bits" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="Image raw OpenStack pour ARM 64 bits" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
	   <li>PowerPC 64 bits petit-boutiste (<a title="Image qcow2 OpenStack pour PowerPC 64 bits petit-boutiste" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="Image raw OpenStack pour PowerPC 64 bits petit-boutiste" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>une machine virtuelle QEMU locale aux formats qcow2 ou raw ;
      <ul class="quicklist downlist">
	   <li>AMD/Intel 64 bits (<a title="Image qcow2 QEMU pour AMD/Intel 64 bits" href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a title="Image raw QEMU pour AMD/Intel 64 bits" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
       <li>ARM 64 bits (<a title="Image qcow2 QEMU pour ARM 64 bits" href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a title="Image raw QEMU pour ARM 64 bits" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
	   <li>PowerPC 64 bits petit-boutiste (<a title="Image qcow2 QEMU pour PowerPC 64 bits petit-boutiste" href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a title="Image raw QEMU pour PowerPC 64 bits petit-boutiste" href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, soit comme image de machine, soit à l'aide de AWS Marketplace ;
	   <ul class="quicklist downlist">
	    <li><a title="Images de Machine Amazon" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Images de machine Amazon</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, sur Azure Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 12 sur Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
	    <li><a title="Debian 11 sur Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
	   </ul>
      </li>
    </ul>
 </div>
</div>

