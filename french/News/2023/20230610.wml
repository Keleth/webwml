<define-tag pagetitle>Publication de Debian 12 <q>Bookworm</q></define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="5fc831c6a77ce47c0c4e44da8a3b594b8280e3dc" maintainer="Jean-Pierre Giraud"


<p>Après un an, neuf mois et vingt-huit jours de développement, le projet
Debian est fier d'annoncer sa nouvelle version stable n°12 (nom de code
<q>Bookworm</q>).</p>

<p><q>Bookworm</q> sera suivie pendant les cinq prochaines années grâce à
l'effort combiné de
<a href="https://security-team.debian.org/">l'équipe de sécurité de Debian</a>
et de <a href="https://wiki.debian.org/LTS">l'équipe de gestion à long terme de
Debian</a>.</p>

<p>À la suite du vote de la <a href="$(HOME)/vote/2022/vote_003">Résolution
générale sur les microprogrammes non libres de 2022</a>, une nouvelle section
d'archive a été introduite permettant de séparer les microprogrammes non libres
des autres programmes non libres :
</p>
<ul>
<li>non-free-firmware</li>
</ul>

<p>La plupart des paquets de microprogrammes non libres ont migré de 
<b>non-free</b> à <b>non-free-firmware</b>. Cette séparation permet de
construire une variété d'images d'installation officielles.
</p>

<p>Debian 12 <q>Bookworm</q> propose de nombreux environnements de bureau
tels que :
</p>
<ul>
<li>Gnome 43 ;</li>
<li>KDE Plasma 5.27 ;</li>
<li>LXDE 11 ;</li>
<li>LXQt 1.2.0 ;</li>
<li>MATE 1.26 ;</li>
<li>Xfce 4.18.</li>
</ul>

<p>Cette version contient plus de <b>11 089</b> nouveaux paquets pour un total
de <b>64 419</b> paquets, avec un nombre significatif de paquets (6 296) marqués
comme « obsolètes » et supprimés. <b>43 254</b> paquets ont été mis à jour dans
cette version.</p>

<p>L'utilisation de disque totale pour <q>Bookworm</q> est de <b>365 016 420 Ko
(365 Go)</b> et comprend <b>1 341 564 204</b> lignes de code.</p>

<p><q>Bookworm</q> contient plus de pages de manuel traduites que jamais grâce
aux traducteurs qui ont rendu les <b>pages de manuel</b> disponibles dans de
nombreuses langues telles que le tchèque, le danois, le grec, le finnois,
l'indonésien, le macédonien, le norvégien bokmål, le russe, le serbe, le
suédois, l'ukrainien et le vietnamien. En outre, toutes les pages de manuel
concernant <b>systemd</b> sont désormais traduites en allemand.</p>

<p>Le mélange Debian Med introduit un nouveau paquet, <b>shiny-server</b>, qui
simplifie les applications web scientifiques utilisant <b>R</b>. Notre effort
pour le maintien de la prise en charge de l'intégration continue pour les
paquets de l'équipe Debian Med. Installez la version 3.8 des métapaquets pour
Debian Bookworm.</p>

<p>Le mélange Debian Astro continue à fournir une solution complète pour les
astronomes professionnels ou amateurs avec des mises à jour pour presque toutes
les versions des paquets logiciels du mélange. <b>astap</b> et
<b>planetary-system-stacker</b> aident à la superposition d'images et à la
résolution astrométrique. <b>openvlbi</b>, le corrélateur à code source libre,
est désormais inclus.</p>

<p>La prise en charge de Secure Boot sur ARM64 a été réintroduite. Les
utilisateurs de matériel compatible avec UEFI peuvent amorcer avec Secure Boot
activé pour tirer pleinement avantage de ses fonctionnalités de sécurité.</p>

<p>
Debian 12 <q>Bookworm</q> inclut de nombreux paquets logiciels mis à jour (plus
de 67 % des paquets de la distribution précédente), par exemple :</p>

<ul>
<li>Apache 2.4.57</li>
<li>Serveur DNS BIND 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (serveur de courriel par défaut) 4.96</li>
<li>GIMP 2.10.34</li>
<li>Collection de compilateurs GNU 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>Bibliothèque C GNU 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Noyaux Linux série 6.1</li>
<li>Chaîne de compilation LLVM/Clang 13.0.1, 14.0 (par défaut) et 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>

<p>
Avec cette large sélection de paquets, ainsi que sa traditionnelle prise en
charge de nombreuses architectures, Debian, une fois de plus, confirme son but
d'être le <q>système d'exploitation universel</q>. Elle est appropriée pour de
nombreux cas d'utilisation différents : des systèmes de bureau aux miniportables,
des serveurs de développement aux systèmes pour grappe, ainsi que des serveurs
de bases de données aux serveurs web ou de stockage. En même temps, des efforts
supplémentaires d'assurance qualité tels que des tests automatiques
d'installation et de mise à niveau pour tous les paquets de l'archive Debian
garantissent que <q>Bookworm</q> répond aux fortes attentes de nos utilisateurs
lors d'une publication stable de Debian.
</p>

<p>
Un total de neuf architectures sont officiellement gérées par <q>Bookworm</q> :</p>
<ul>
<li>PC 32 bits (i386),</li>
<li>PC 64 bits (amd64),</li>
<li>ARM 64 bits (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (avec unité de calcul flottant EABI, armhf),</li>
<li>MIPS petit-boutiste (mipsel),</li>
<li>MIPS 64 bits petit-boutiste (mips64el),</li>
<li>PowerPC 64 bits petit-boutiste (ppc64el),</li>
<li>IBM System z (s390x).</li> 
</ul>

<p>La prise en charge de PC 32 bits (i386) ne couvre plus aucun processeur i586 ;
les processeurs i686 sont le nouveau minimum requis. <i>Si votre machine n'est
pas compatible avec ce prérequis, il est recommandé de rester sur <q>Bullseye</q>
jusqu'à la fin de son cycle de prise en charge.</i>
</p>

<p>L'équipe pour l'informatique dématérialisée publie <q>Bookworm</q> pour
plusieurs services d'informatique dans le nuage :</p>
<ul>
<li>Amazon EC2 (amd64 et arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (générique) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amg64, arm64, ppc64el)</li>
</ul>

<p>L'image genericcloud devrait être en mesure d'être exécutée dans n'importe quel
environnement virtualisé et il existe également une image nocloud qui permet de
tester le processus de construction.
</p>

<p>
Par défaut, les paquets GRUB n'exécuteront plus <a href="https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.fr.html#grub-os-prober">os-prober
pour rechercher d'autres systèmes d'exploitation.</a>
</p>

<p>Depuis la dernière version, le Comité Technique a décidé que Debian
<q>Bookworm</q> ne devrait <a href="https://wiki.debian.org/UsrMerge">prendre
en charge que l'organisation du système de fichiers racine avec /usr fusionné</a>,
abandonnant la prise en charge de l'organisation avec /usr non fusionné. Pour
les systèmes installés comme Buster ou Bullseye, il n'y aura pas de modification
du système de fichiers, cependant, les systèmes utilisant l'organisation
antérieure seront convertis durant la mise à niveau.</p>


<h3>Vous voulez l'essayer ?</h3>
<p>
Si vous voulez simplement essayer Debian 12 <q>Bookworm</q> sans l'installer,
vous pouvez utiliser une des <a href="$(HOME)/CD/live/">images autonomes</a> « live »
qui chargent et exécutent le système d'exploitation complet, dans un mode en
lecture seule, dans la mémoire de votre ordinateur.
</p>

<p>
Ces images autonomes sont fournies pour les architectures <code>amd64</code> et
<code>i386</code> et sont disponibles pour des installations à l'aide de DVD,
clés USB ou d'amorçage par le réseau. Les utilisateurs peuvent choisir entre
plusieurs environnements de bureau : GNOME, KDE Plasma, LXDE, LXQt, MATE et Xfce.
Debian <q>Bookworm</q> autonome fournit une image autonome standard, ainsi, il
est possible d'essayer un système Debian de base sans interface utilisateur
graphique.
</p>

<p>
Si le système d'exploitation vous plaît, vous avez la possibilité de
l'installer sur le disque dur de votre machine à partir de l'image autonome.
Celle-ci comprend l'installateur indépendant Calamares ainsi que l'installateur
Debian standard. Davantage d'informations sont disponibles dans les
<a href="$(HOME)/releases/bookworm/releasenotes">notes de publication</a>
et sur la page de la <a href="$(HOME)/CD/live/">section des images
d'installation autonomes</a> du site de Debian.
</p>

<p>
Si vous souhaitez installer Debian 12 <q>Bookworm</q> directement sur le
périphérique de stockage de votre ordinateur, vous pouvez choisir parmi les
nombreux types de média d'installation à
<a href="https://www.debian.org/download">télécharger</a> tels que les disques
Blu-ray, CD et DVD et les clefs USB, ainsi que par une connexion réseau. Voir
le <a href="$(HOME)/releases/bookworm/installmanual">manuel d'installation</a>
pour plus de détails.
</p>

# Translators: some text taken from: 

<p>
Debian peut maintenant être installée en 78 langues, dont la plupart sont
disponibles avec des interfaces utilisateur en mode texte et graphique.
</p>

<p>
Les images d'installation peuvent être téléchargées dès à présent au moyen de
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (le moyen recommandé),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> ou
<a href="$(HOME)/CD/http-ftp/">HTTP</a> ; consultez la page
<a href="$(HOME)/CD/">Debian sur CD</a> pour plus d'informations. <q>Bookworm</q>
sera bientôt également disponible sur DVD, CD et disques Blu-ray physiques
chez de nombreux <a href="$(HOME)/CD/vendors">distributeurs</a>.
</p>


<h3>Mise à niveau de Debian</h3>
<p>
La mise à niveau vers Debian 12 <q>Bookworm</q> à partir de la version
précédente, Debian 11 <q>Bullseye</q> est gérée automatiquement par l'outil de
gestion de paquets APT pour la plupart des configurations.
</p>

<p>Avant de mettre à niveau le système, il est fortement conseillé de faire une
sauvegarde complète ou, du moins, une sauvegarde des données et des informations
de configuration que vous ne pouvez pas vous permettre de perdre. Les outils de
mise à niveau sont tout à fait fiables, mais une panne matérielle au milieu de
la mise à niveau peut fortement endommager le système.
</p>

<p>Ce que vous devriez principalement sauvegarder est le contenu des répertoires
/etc et /var/lib/dpkg, du fichier /var/lib/apt/extended_states et la sortie de :
<code>$ dpkg --get-selections '*' # (les guillemets sont importants)</code>.
</p>

<p>Nous recueillons toutes les expériences de nos utilisateurs sur les mises à
niveau de <q>Bullseye</q> vers <q>bookworm</q>. Veuillez soumettre un rapport
de bogue dans le
<a href="https://www.debian.org/releases/bookworm/amd64/release-notes/ch-about.fr.html#upgrade-reports">\
système de suivi des bogues</a> en utilisant le paquet <b>upgrade-reports</b>
avec votre bilan.
</p>


<p>
Il y a eu beaucoup de développements sur l'installateur Debian aboutissant à une
amélioration de la prise en charge matérielle et à d'autres fonctionnalités
telles que la prise en charge graphique d'UTM, des corrections du chargeur de
fontes de GRUB, la suppression de la longue attente à la fin du processus
d'installation et la correction de la détection des systèmes avec amorçage par
le BIOS.</p>


<p>
Le paquet <b>ntp</b> a été remplacé par le paquet <b>ntpsec</b> et le service
d'horloge système par défaut est désormais <b>systemd-timesyncd</b>. Il existe
aussi une prise en charge de <b>chrony</b> et d'<b>openntpd</b>.
</p>


<p>Dans la mesure où
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.fr.html#non-free-split">\
les microprogrammes <b>non-free</b> ont migré vers un composant de l'archive
qui leur est dédié</a>, si vous avez des microprogrammes non libres installés,
il est recommandé d'ajouter <b>non-free-firmware</b> aux fichiers sources-list
d'APT.</p>

<p>Il est recommandé de supprimer les entrées <b>bullseye-backports</b> des
fichiers sources-list d'APT avant la mise à niveau et d'envisager d'ajouter
<b>bookworm-backports</b>après la mise à niveau.</p>

<p>
Pour <q>Bookworm</q>, la suite de sécurité s'appelle <b>bookworm-security</b>
et les utilisateurs devraient adapter leurs fichiers sources-list d'APT en
conséquence. Si la configuration d'APT comprend également l'épinglage ou la
ligne <code>APT::Default-Release</code>, il est vraisemblable qu'elle nécessite
des adaptations pour permettre la nouvelle version stable. Veuillez consulter la
section <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.fr.html#disable-apt-pinning">\
Désactiver l'épinglage d'APT</a>.
</p>

<p>La mise à niveau d'OpenLDAP 2.5 comprend plusieurs
<a href="https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.fr.html#openldap-2.5">
modifications incompatibles qui peuvent nécessiter une intervention manuelle</a>.
En fonction de la configuration, le service <b>slapd</b> peut demeurer arrêté
après la mise à niveau, jusqu'à ce que les mises à jour de la configuration
soient finalisées.
</p>

<p>Le nouveau paquet <b>systemd-resolved</b> ne sera pas installé automatiquement
lors des mises à niveau dans la mesure où il a été
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.fr.html#systemd-resolved">\
séparé dans un paquet indépendant</a>. Si vous utilisez le service système
<b>systemd-resolved</b>, veuillez installer le nouveau paquet manuellement après
la mise à niveau et noter que jusqu'à ce que le paquet soit installé, la
résolution DNS pourrait ne plus fonctionner dans la mesure où le service ne sera
pas installé sur le système.
</p>


<p>Il y a eu quelques
<a href="$(HOME)/releases/bookworm/amd64//release-notes/ch-information.fr.html#changes-to-system-logging">\
modifications dans la journalisation du système.</a>, le paquet <b>rsyslog</b>
n'est plus nécessaire sur la plupart des systèmes, ni installé par défaut. Les
utilisateurs peuvent passer à <b>journalctl</b> ou utiliser les nouveaux
<q>horodatages haute précision</q> qu'utilise maintenant <b>rsyslog</b>.
</p>


<p>Parmi les <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.fr.html#trouble">\
problèmes potentiels durant la mise à niveau</a>, les <q>Conflicts</q> ou les
boucles <q>Pre-Depends</q> peuvent être résolus en retirant et en faisant
disparaître certains paquets ou en forçant la réinstallation d'autres paquets.
D'autres soucis viennent des erreurs <q>Impossible de réaliser une configuration
immédiate ...</q> pour lesquelles il peut être nécessaire de conserver <b>à
la fois</b> <q>Bullseye</q> (qui vient juste d'être supprimée) et <q>Bookworm</q>
(qui vient juste d'être ajoutée) dans le fichier source-list d'APT, et des
conflits de fichiers qui peuvent requérir la suppression forcée de paquets.
Comme mentionné, faire la sauvegarde du système est la clé d'une mise à niveau
sans problème même si des erreurs fâcheuses surviennent.</p> 


<p>Il y a quelques paquets pour lesquels Debian ne peut pas promettre de fournir
un rétroportage pour les problèmes de sécurité, veuillez consulter la page sur les
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.fr.html#limited-security-support">\
limites de prise en charge de sécurité</a>.</p>


<p>
Comme toujours, les systèmes Debian peuvent être mis à niveau sans douleur, sur
place et sans période d'indisponibilité forcée, mais il est fortement
recommandé de lire les
<a href="$(HOME)/releases/bookworm/releasenotes">notes de publication</a> ainsi
que le <a href="$(HOME)/releases/bookworm/installmanual">manuel d'installation</a>
pour d'éventuels problèmes et pour des instructions détaillées sur
l'installation et la mise à niveau. Les notes de publications seront améliorées
et traduites dans les semaines suivant la publication.
</p>


<h2>À propos de Debian</h2>

<p>
Debian est un système d'exploitation libre, développé par plusieurs milliers
de volontaires provenant du monde entier qui collaborent à l'aide d'Internet.
Les points forts du projet sont l'implication basée sur le volontariat,
l'engagement dans le contrat social de Debian et le logiciel libre, ainsi que
son attachement à fournir le meilleur système d'exploitation possible. Cette
nouvelle version représente une nouvelle étape importante dans ce sens.
</p>


<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian à l'adresse <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez
un courrier électronique à &lt;press@debian.org&gt;.
</p>

