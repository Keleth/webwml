#use wml::debian::template title="Примеры"
#use wml::debian::translation-check translation="d1af73e44d054ea9925b972ae2cb196c0365a2bb" maintainer="Lev Lamberov"

<h3>Запуск нового перевода</h3>

<p>В качестве примера будет использоваться русский язык:

<pre>
   git pull
   cd webwml
   mkdir russian
   cd russian
   cp ../english/.wmlrc ../english/Make.* .
   echo '<protect>include $(subst webwml/russian,webwml/english,$(CURDIR))/Makefile</protect>' &gt; Makefile
   mkdir po
   git add Make* .wmlrc po
   cp Makefile po
   make -C po init-po
   git add po/Makefile po/*.ru.po
</pre>

<p>Отредактировать файл <tt>.wmlrc</tt>, изменив:
<ul>
  <li>'-D CUR_LANG=English' на '-D CUR_LANG=Russian'
  <li>'-D CUR_ISO_LANG=en' на '-D CUR_ISO_LANG=ru'
  <li>'-D CUR_LOCALE=en_US' на '-D CUR_LOCALE=ru_RU'
  <li>'-D CHARSET=iso-8859-1' на '-D CHARSET=utf-8.<br>
</ul>

<p>Отредактировать Make.lang, изменив 'LANGUAGE := en'  на 'LANGUAGE := ru'.
В случае, если вы переводите на язык, который использует многобайтовую
кодировку, вам, вероятно, придётся изменить некоторые другие переменные в этом
файле. Более подробную информацию можно найти в ../Makefile.common и,
возможно, уже работающих переводах (например, китайском).

<p>Зайдите в russian/po, и переведите содержимое PO-файлов. Как это делать,
достаточно очевидно.

<p>Не забывайте скопировать файл Makefile в каждый переводимый каталог.
Это необходимо, поскольку для преобразования файлов .wml в HTML используется
программа <code>make</code>. Файлы Makefile необходимы для её работы.

<p>Когда вы завершили добавление и редактирование файлов, выполните из каталога webwml следующие команды:
<pre>
   git commit -m "Сюда добавьте ваше сообщение о коммите"
   git push
</pre>
Теперь вы можете приступать к переводу страниц.

<h3>Перевод страницы</h3>

<p>В качестве примера будет использоваться русский перевод общественного
договора:

<pre>
   cd webwml
   ./copypage.pl english/social_contract.wml
   cd russian
</pre>

<p>Это автоматически добавит заголовок translation-check, указывающий на
версию оригинального файла, который был скопирован. Кроме того, будут созданы каталог
назначения и Makefile, если они отсутствуют.</p>

<p>Отредактируйте social_contract.wml, переведя текст. Ссылки не следует
переводить или изменять. Если вы хотите изменить что-либо, сначала задайте
вопрос в списке debian-www. Закончив работу, выполните

<pre>
   git add social_contract.wml
   git commit -m "Translated social contract to Russian"
   git push
</pre>

<H3>Добавление нового каталога</H3>

<p>В этом примере показано добавление каталога intro/ в русский перевод:

<pre>
   cd webwml/russian
   mkdir intro
   cd intro
   cp ../Makefile .
   git add Makefile
   git commit -m "added the intro dir to git"
   git push
</pre>

Убедитесь, что в новом каталоге есть файл Makefile и что он зафиксирован в git.
В противном случае запуск make приведёт к ошибке.

<h3>Конфликт</h3>

<p>Этот пример демонстрирует коммит, который не сработает из-за того, что копия в
репозитории была изменена после вашего последнего выполнения команды <kbd>git pull</kbd>.

<pre>
    git add foo.wml
    git commit -m "fixed a broken link"
    git push
 </pre>

 Выполнение этих команд приведёт к следующему:

 <pre>
To salsa.debian.org:webmaster-team/webwml.git
 ! [rejected]                master -> master (fetch first)
error: failed to push some refs to 'git@salsa.debian.org:webmaster-team/webwml.git'
 </pre>

 <p>или что-то типа того :)
       <br />
       <br />
    Это означает, что ваши изменения <strong>не</strong> были отправлены в
    git-репозитории из-за конфликта.
       <br />
    Вам следует выяснить, что пошло не так, разрешить конфликт и попытаться
    снова сделать коммит и отправить изменения в репозиторий.</p>
