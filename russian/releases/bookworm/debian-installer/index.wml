#use wml::debian::template title="Информация об установке Debian &ldquo;bookworm&rdquo;" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#use wml::debian::translation-check translation="ab3c0fa63d12dbcc8e7c3eaf4a72beb7b56d9741" maintainer="Lev Lamberov"

<h1>Installing Debian <current_release_bookworm></h1>

<if-stable-release release="trixie">
<p><strong>Debian 12 был заменён на
<a href="../../trixie/">Debian 13 (<q>trixie</q>)</a>. Некоторые из этих
установочных образов могут быть недоступны, или могут не работать,
рекомендуется вместо этого установить trixie.
</strong></p>
</if-stable-release>

<p>
<strong>Чтобы установить Debian</strong> <current_release_bullseye>
(<em>bookworm</em>), загрузите какой-нибудь из следующих образов (любой образ для
архитектур i386 и amd64 может использоваться для записи на USB-накопитель):
</p>

<div class="line">
<div class="item col50">
	<p><strong>образ компакт-диска сетевой установки</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>полный набор CD</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>полный набор DVD</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (через <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (через <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (через <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>другие образы (для загрузки по сети, для USB-накопителей и т. д.)</strong></p>
<other-images />
</div>
</div>


<p>
<strong>Замечания</strong>
</p>
<ul>
    <li>
	Для скачивания полных образов CD и DVD рекомендуется использовать bittorrent
        или jigdo.
    </li><li>
        Для менее распространённых архитектур доступно лишь ограниченное число образов
        из наборов CD и DVD в виде файлов ISO или через bitTorrent.
        Полные наборы доступны только через jigdo.
    </li><li>
        Проверочные файлы (<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt> и другие)
        установочных образов располагаются в том же каталоге, что и
        сами образы.
    </li>
</ul>


<h1>Документация</h1>

<p>
<strong>Если вы хотите прочитать только один документ</strong> перед установкой, прочтите
<a href="../i386/apa">Практическое руководство по установке</a>, быстрый путеводитель
по процессу установки. Другие полезные документы:
</p>

<ul>
<li><a href="../installmanual">Руководство по установке Bookworm</a><br />
detailed installation instructions</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">ЧаВО по Debian-Installer</a>
и <a href="$(HOME)/CD/faq/">ЧаВО по Debian-CD</a><br />
общие вопросы и ответы</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer вики</a><br />
документация, поддерживаемая сообществом</li>
</ul>

<h1 id="errata">Известные ошибки</h1>

<p>
Это список всех известных проблем в системе установки, идущей с
Debian <current_release_bullseye>. Если в процессе установки Debian вы
обнаружили проблему, которой не увидели здесь, отправьте
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">отчёт об установке</a>
с описанием проблемы, или
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">посмотрите в вики</a>
другие известные проблемы.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Известные ошибки выпуска 12.0</h3>

<dl class="gloss">

<!--
     <dt>Установка окружения рабочего стола при наличии только первого компакт-диска может не работать</dt>

     <dd>Из-за ограничений по месту на первом компакт-диске не все
     ожидаемые пакеты окружения GNOME вошли на диск. Для успешной
     установки используйте дополнительные источники пакетов (например, второй компакт-диск или
     сетевое зеркало), либо используйте DVD.

     <br /> <b>Статус:</b> вряд ли что-то ещё можно сделать, чтобы на первый компакт-диск вошли дополнительные
     пакеты.
-->
</dl>

<p>
Улучшенные версии системы установки будут подготовлены
для следующего выпуска Debian, их можно будет использовать и для установки stretch.
Подробности см. на
<a href="$(HOME)/devel/debian-installer/">странице проекта
Debian-Installer</a>.
</p>
