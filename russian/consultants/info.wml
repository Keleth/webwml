#use wml::debian::template title="Информация для консультантов Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="5f70081cb38037d61262d8cb159fb510c9315981" maintainer="Lev Lamberov"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#policy">Политика страницы консультантов Debian</a></li>
  <li><a href="#change">Дополнения, изменения и удаления записей</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Консультанты могут связаться с другими консультантами Debian через <a href="https://lists.debian.org/debian-consultants/">список рассылки debian-consultants</a>. Этот список рассылки на модерируется; отправка сообщений в него разрешена для всех. Также предоставляется публичный архив этого списка рассылки.</p>
</aside>

<h2><a id="policy">Политика страницы консультантов Debian</a></h2>

<p>Если вы хотите, чтобы мы указали вас как консультанта на веб-сайте Debian,
обратите внимание на следующие правила:</p>

<ul>
  <li><strong>Обязательная контактная информация:</strong><br />
    Вы должны предоставить рабочий адрес электронной почты и отвечать на посылаемую вам почту с задержкой
    не дольше, чем четыре недели. Для того, чтобы предотвратить злоупотребления, запросы (дополнения,
    удаления или изменения) должны отправляться с одного и того же адреса. В избежание спама,
    вы можете запросить, чтобы ваш адрес был запутан (напр.,
    <em>debian -dot- consulting -at- example -dot- com</em>). Если это нужно,
    укажите это явно в вашей заявке. Также вы можете указать,
    чтобы ваш адрес электронной почты вообще не был виден на
    веб-странице (хотя нам всё равно нужен рабочий адрес электронной почты для сопровождения списка).
    Если вы хотите, чтобы ваш адрес был скрыт в списке, вы можете предоставить
    URL формы для контактов на вашем веб-сайте, ссылку на которую мы укажем.
    Предполагается, что для этого будет использоваться поле <em>Контактная информация</em>.
  </li>
  <li><strong>Ваш веб-сайт:</strong><br />
    Если вы предоставляете ссылку на веб-сайт, этот веб-сайт должен содержать
    упоминание о ваших консалтинговых услугах, касающихся Debian. Предоставление прямой ссылки
    на эту информацию вместо ссылки на обычную домашнюю страницу не является обязательным, если информация
    легко доступна, но мы будем весьма благодарны за предоставление прямой ссылки.
  </li>
  <li><strong>Несколько городов/регионов/стран</strong><br />
    Вам следует выбрать страну (только одну), в списке которой вы
    хотите разместить о себе информацию. Дополнительные города, регионы или страны следует
    указывать как дополнительную информацию, либо на вашем собственном веб-сайте.
  </li>
  <li><strong>Правила для разработчиков Debian</strong><br />
    Вам не разрешается использовать ваши официальные адреса <em>@debian.org</em>
    в списке консультантов. То же касается и вашего веб-сайта: вам нельзя использовать
    официальный <em>домен *.debian.org</em>, как это описанно в
    <a href="$(HOME)/devel/dmup">правилах использования машин Debian (DMUP)</a>.
  </li>
</ul>

<p>Если приведённые выше критерии более не будут выполняться в какой-то момент в будущем,
консультант получит предупреждающее сообщение о том, что он может быть
удалён из списка, если снова не выполнит все условия.
На исправление информации даётся четыре недели.</p>

<p>Некоторые части (или даже все) информации о консультанте могут быть удалены,
если они более не соответствуют данным правилам, либо по усмотрению
сопровождающих данного списка.</p>

<h2><a id="change">Дополнения, изменения и удаления записей</a></h2>

<p>Если вы хотите добавить информацию о себе в список консультантов,
   вышлите по адресу <a href="mailto:consultants@debian.org">consultants@debian.org</a> письмо
   на английском языке с предоставлением любой информации из нижеследующего перечня, которую
   вы хотели бы указать в списке (адрес электронной почты обязателен, всё остальное
   указывается по вашему усмотрению):</p>

<ul>
  <li>Страна, в списке которой вы хотели бы разместить информацию</li>
  <li>Имя</li>
  <li>Компания</li>
  <li>Адрес</li>
  <li>Телефон</li>
  <li>Факс</li>
  <li>Контактная информация</li>
  <li>Эл. почта</li>
  <li>URL</li>
  <li>Цена</li>
  <li>Дополнительная информация</li>
</ul>

<p>Запрос обновления информации о консультанте должен быть отправлен по адресу
электронной почты <a href="mailto:consultants@debian.org">consultants@debian.org</a>.
Желательно отсылать письмо с адреса электронной почты, указанного
<a href="https://www.debian.org/consultants/">на странице консультанта</a>.</p>

# translators can, but don't have to, translate the rest - yet
# BEGIN future version
# fill out the following submission form:</p>
#
# <form method=post action="https://cgi.debian.org/cgi-bin/submit_consultant.pl">
#
# <p>
# <input type="radio" name="submissiontype" value="new" checked>
# New consultant listing submission
# <br />
# <input type="radio" name="submissiontype" value="update">
# Update of an existing consultant listing
# <br />
# <input type="radio" name="submissiontype" value="remove">
# Removal of an existing consultant listing
# </p>
#
# <p>Name:<br />
# <input type="text" name="name" size="50"></p>
#
# <p>Company:<br />
# <input type="text" name="company" size="50"></p>
#
# <p>Address:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
#
# <p>Phone:<br />
# <input type="text" name="phone" size="50"></p>
#
# <p>Fax:<br />
# <input type="text" name="fax" size="50"></p>
#
# <p>E-mail:<br />
# <input type="text" name="email" size="50"></p>
#
# <p>URL:<br />
# <input type="text" name="url" size="50"></p>
#
# <p>Rates:<br />
# <textarea name="comment" cols=35 rows=4></textarea></p>
#
# <p>Additional information, if any (<em>in English</em>):<br />
# <textarea name="comment" cols=40 rows=7></textarea></p>
#
# </form>
#
# <p>If you are unable to submit the above for any reason whatsoever,
# please send it via e-mail <em>in English</em> to
# END future version
