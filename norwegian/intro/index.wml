#use wml::debian::template title="Introduksjon til Debian" MAINPAGE="true" FOOTERMAP="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Hans F. Nordhaug"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian er et fellesskap av folk</h2>
      <p>Tusensvis av frivillige i hele verden arbeider sammen med fri 
      programvare som mål og for brukernes behov.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Folk:</a>
          Hvem vi er og hva vi gjør
        </li>
        <li>
          <a href="philosophy">Vår filosofi:</a>
          Hvorfor vi gjør det og hvordan vi gjør det
        </li>
        <li>
          <a href="../devel/join/">Bli involvert:</a>
          Hvordan bli en Debian bidragsyter
        </li>
        <li>
          <a href="help">Bidra:</a>
          Hvordan kan du hjelpe Debian?
        </li>
        <li>
          <a href="../social_contract">Debians sosiale kontrakt:</a>
          Vår moralske agenda
        </li>
        <li>
          <a href="diversity">Alle er velkommen:</a>
          Debians mangfoldserklæring
        </li>
        <li>
          <a href="../code_of_conduct">For deltagere:</a>
          Etiske retningslinjer
        </li>
        <li>
          <a href="../partners/">Partnere:</a>
          Bedrifter og organisasjoner som løpende hjelper Debian-prosjektet
        </li>
        <li>
          <a href="../donations">Donasjoner:</a>
          Hvordan støtte Debian-prosjektet
        </li>
        <li>
          <a href="../legal/">Juridisk informasjon</a>
          Lisenser, varemerker, personvernregler osv.
        </li>
        <li>
          <a href="../contact">Kontakt:</a>
          Hvordan nå oss
        </li>
      </ul>
    </div>
  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian er et operativsystem</h2>
      <p>Debian er et fritt operativsystem, utviklet og vedlikeholdt av Debian-prosjektet. 
      En fri Linux-distribusjon med mange tusen applikasjoner for å møte brukernes behov.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Last ned:</a>
          Hvor man får tak i Debian
        </li>
        <li>
          <a href="why_debian">Hvorfor Debian:</a>
          Grunner til å velge Debian
        </li>
        <li>
          <a href="../support">Støtte:</a>
          Hvor man kan få hjelp
        </li>
        <li>
          <a href="../security">Sikkerhet:</a>
          Siste oppdatering <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
             @MYLIST = split(/\n/, $MYLIST);
             $MYLIST[0] =~ s#security#../security#;
             print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Programvare:</a>
          Søk i og utforsk den lange listen med Debian-pakker
        </li>
        <li>
          <a href="../doc">Dokumentasjon:</a>
          Installasjonshåndbok, ofte stilte spørsmål (FAQ), Wiki med mer
        </li>
        <li>
          <a href="../bugs">System for sporing av feil (BTS):</a>
          Hvordan rapportere en feil, BTS-dokumentasjon
        </li>
        <li>
          <a href="https://lists.debian.org/">E-postlister:</a>
          Samling av Debian-lister for brukere, utviklere osv.
        </li>
        <li>
          <a href="../blends">Pure Blends:</a>
          Metapakker for spesifikke behow
        </li>
        <li>
          <a href="../devel">Utviklerhjørnet:</a>
          Informasjon som hovedsaklig er nyttig for Debian-utviklere
        </li>
        <li>
          <a href="../ports">Arkitekturtilpasninger:</a>
          Debian støtter mange CPU-arkitekturer
        </li>
        <li>
          <a href="search">Søk:</a>
          Informasjon om hvordan bruke Debians søkemotor
        </li>
        <li>
          <a href="cn">Språk:</a>
          Språkinnstillinger for Debian-nettstedet
        </li>
      </ul>
    </div>
  </div>

</div>
