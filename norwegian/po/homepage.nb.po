msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-10-06 09:19+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.3.1\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Det universelle operativsystemet"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "DebConf er på vei!"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "DebConf-logo"

#: ../../english/index.def:19
msgid "DC22 Group Photo"
msgstr "DC22-gruppefoto"

#: ../../english/index.def:22
msgid "DebConf22 Group Photo"
msgstr "DebConf22-gruppefoto"

#: ../../english/index.def:26
msgid "Debian Reunion Hamburg 2023"
msgstr "Debian gjenforening Hamburg 2023"

#: ../../english/index.def:29
msgid "Group photo of the Debian Reunion Hamburg 2023"
msgstr "Gruppefoto fra Debian gjenforeningen i Hamburg 2023"

#: ../../english/index.def:33
msgid "Mini DebConf Regensburg 2021"
msgstr "Mini DebConf Hamburg 2021"

#: ../../english/index.def:36
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Gruppefoto fra MiniDebConf i Hamburg 2021"

#: ../../english/index.def:40
msgid "Screenshot Calamares Installer"
msgstr "Skjermbilde Calamares-installasjonsprogram"

#: ../../english/index.def:43
msgid "Screenshot from the Calamares installer"
msgstr "Skjermbilde fra installasjonsprogrammet Calamares"

#: ../../english/index.def:47 ../../english/index.def:50
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian er som en sveitsisk lommekniv"

#: ../../english/index.def:54
msgid "People have fun with Debian"
msgstr "Folk har det gøy med Debian"

#: ../../english/index.def:57
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Debian-folk på Debconf18 i Hsinchu som har det skikkelig gøy"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Bits from Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blogg"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Mikronyheter"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Mikronyheter fra Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planet"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "Debian-planeten"
