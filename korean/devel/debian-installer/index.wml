#use wml::debian::template title="데비안 설치관리자" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5d9ed11b1ac37b0f0417096ff186a5fd178b7fa6" maintainer="Sebul" 
<h1>뉴스</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">지난 뉴스</a>
</p>

<h1>데비안 설치관리자로 설치하기</h1>


<p>
<if-stable-release release="bookworm">
<strong>공식 데비안 <current_release_bookworm> 설치미디어와 정보</strong>는,
<a href="$(HOME)/releases/bookworm/debian-installer">bookworm 페이지</a>를 보세요.
</if-stable-release>
<if-stable-release release="trixie">
<strong>공식 데비안 <current_release_trixie> 설치 미디어와 정보</strong>를 보려면,
<a href="$(HOME)/releases/trixie/debian-installer">bullseye 페이지</a>를 보세요.
</if-stable-release>
</p>

<div class="tip">
<p>아래 링크된 모든 이미지는 다음 데비안 릴리스를 위해 개발중인 데비안 설치관리자 용 버전이며
기본적으로 데비안 테스팅(<q><current_testing_name></q>)을 설치합니다.
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">
<p>

<strong>데비안 테스팅을 설치하려면</strong>, 설치관리자의
<strong>매일 빌드</strong>를 쓸 것을 권장합니다.
아래 이미지가 매일 빌드로 사용가능:

</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">
<p>

<strong>데비안 테스팅을 설치하려면</strong>, <a href="errata">정오표</a>를 체크하고,
<strong><humanversion /></strong> 릴리스를 쓸 것을 권합니다.
<humanversion />에서 가능한 이미지:

</p>

<h2>공식 릴리스</h2>

<div class="line">
<div class="item col50">
<strong>netinst CD 이미지</strong>
<netinst-images />
</div>

<div class="item col50 lastcol">
<strong>netinst CD 이미지 (<a href="$(HOME)/CD/jigdo-cd">jigdo</a> 이용)</strong>
<netinst-images-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>다른 이미지 (netboot, USB 메모리 등.)</strong>
<other-images />
</div>
</div>

<p>다른 방법으로 <b>데비안 테스팅 배포판</b>을 설치할 수 있습니다.
일간 빌드에서는 몇 개의 일부 이미지만 만들지만, 주간 빌드에서는 모든
종류의 이미지 모음을 만듭니다.
</p>

<div class="warning">

<p>
이 스냅샷은 데비안 테스팅(testing)을 설치하지만 설치 관리자는 데비안 불안정(unstable) 기반입니다.
</p>

</div>

<h2>현재 매주 스냅샷</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

<p>
설치 관리자의 향후 릴리스를 테스트하는 데 도움이 되거나 하드웨어 문제 또는 기타 문제로 인해 최신 버전을 사용하려면,
사용 가능한 최신 버전의 설치 관리자 구성 요소가 포함된 <strong>매일 빌드되는 이미지</strong> 중 하나를 사용해 보십시오.
</p>
</if-testing-installer>

<h2>현재 매일 스냅샷</h2>

<div class="line">
<div class="item col50">
<strong>netinst (generally 150-280 MB) <!-- and businesscard (generally 20-50 MB) --> CD images</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>netinst <!-- and businesscard --> CD 이미지 (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>multi-arch CD 이미지</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>다른 이미지 (netboot, USB 메모리 등.)</strong>
<devel-other-images />
</div>
</div>

<hr />

<p>
<strong>주의</strong>
</p>
<ul>
#	<li>Before you download the daily built images, we suggest you check for
#	<a href="https://wiki.debian.org/DebianInstaller/Today">known issues</a>.</li>
	<li>일일 빌드를 (안정적으로) 사용할 수 없는 경우에는 일일 빌드 이미지의 개요에서
        일부 아키텍처가 (일시적으로) 생략될 수도 있습니다.</li>
	<li>설치 이미지는 확인 파일이 (<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt> 등)
        이미지와 같은 디렉터리에 들어 있습니다.</li>
	<li>전체 CD 및 DVD 이미지를 다운로드하려면 jigdo를 사용하는 게 좋습니다.</li>
	<li>전체 DVD 세트의 제한된 수의 이미지만 직접 다운로드를 위해 ISO 파일로
        제공됩니다. 대부분의 사용자는 모든 디스크에 사용 가능한 모든 소프트웨어가 필요하지
        않으므로, 다운로드 서버 및 미러 공간을 절약하기 위해 전체 세트는 jigdo를
        통해서만 가능합니다.</li>
</ul>

<p>
<strong>데비안 설치관리자</strong> 사용 후에는,
<a href="https://d-i.debian.org/manual/ko.amd64/ch05s04.html#submit-bug">설치
보고</a>를 보내주세요. 아무 문제가 없어도 보내주세요.
</p>

<h1>문서</h1>

<p>설치 전에
<strong>단 하나의 문서만 읽는다면</strong>,
<a href="https://d-i.debian.org/manual/ko.amd64/apa.html">설치
Howto</a>, 설치 절차의 빠른 흐름을 보세요. 다른 쓸모 있는 문서:
</p>

<ul>
<li>설치 가이드:
#    <a href="$(HOME)/releases/stable/installmanual">version for current release</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">개발 버전 (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">최근 버전 (git)</a>
<br />
자세한 설치 명령</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">데비안 설치관리자 FAQ</a>
및 <a href="$(HOME)/CD/faq/">데비안 CD FAQ</a><br />
자주 물어보는 질문 및 답</li>
<li><a href="https://wiki.debian.org/DebianInstaller">데비안 설치관리자 위키</a><br />
커뮤니티가 관리하는 문서</li>
</ul>

<h1>우리에게 연락하기</h1>

<p>
<a href="https://lists.debian.org/debian-boot/">debian-boot 메일링 리스트</a>는
데비안 설치관리자에 대한 토론 및 작업용 메인 포럼입니다.
</p>

<p>
IRC 채널, #debian-boot <tt>irc.debian.org</tt>.
이 채널은 주로 개발에 쓰이지만, 때때로 지원에도 쓰입니다.
응답을 받지 못하면, 대신 메일링 리스트를 시도하세요.
</p>
