#use wml::debian::template title="문서" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="0352601c5e0d6e960ffa0c05a4c5dc054d1897ba" maintainer="Sebul"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">빠른 시작</a></li>
  <li><a href="#manuals">매뉴얼</a></li>
  <li><a href="#other">다른 (짧은) 문서</a></li>
</ul>

<p>
고품질 자유 운영 체제를 만드는 것은 또한 프로그램의 작동과 사용을 설명하는 기술 매뉴얼을 작성하는 것을 포함합니다. 
데비안 프로젝트는 모든 사용자에게 쉽게 접근할 수 있는 형태로 좋은 문서를 제공하기 위해 모든 노력을 기울이고 있습니다. 
이 페이지에는 링크 모음, 설치 안내서, 하우투, FAQ, 릴리스 정보, Wiki 등이 나와 있습니다.
</p>


<h2><a id="quick_start">빠른 시작</a></h2>

<p>
여러분이 데비안이 처음이라면 아래 두 문서로 시작하길 권합니다.
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">설치안내서</a></li>
  <li><a href="manuals/debian-faq/">데비안 GNU/리눅스 FAQ</a></li>
</ul>

<p>
데비안을 처음 설치할 때 이러한 정보를 가지고 있으면 아마도 많은 질문에 답하고 새로운 데비안 시스템을 사용하는 데 도움이 될 것입니다.
</p>

<p>
나중에, 이 문서로 가길 바랄 수 있습니다:
</p>
<ul>
  <li><a href="manuals/debian-reference/">데비안 참조</a>: 
  간단한 사용자 안내서, 셸 명령에 집중</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">릴리스 노트</a>: 보통 데비안 업데이트와 함께 게시되며, 배포판을 업그레이드하는 사용자를 목표로 합니다.</li>
  <li><a href="https://wiki.debian.org/">데비안 위키</a>: 공식 데비안 위키 및 공식 데비안 위키와 새로운 사람들에게 좋은 정보원</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">데비안 GNU/리눅스 참조 카드</a></button></p>

<h2><a id="manuals">매뉴얼</a></h2>

<p>
데비안에 포함된 대부분의 문서는 일반적으로 GNU/리눅스용으로 작성되었지만, 데비안을 위해 특별히 작성된 문서도 있습니다. 
기본적으로 문서는 다음과 같은 범주로 정렬됩니다.
</p>

<ul>
  <li>매뉴얼: 그 가이드는 주요 주제를 포괄적으로 묘사하기 때문에 책과 비슷합니다. 
아래에 나열된 많은 매뉴얼은 온라인과 데비안 패키지에서 모두 사용할 수 있습니다. 
사실, 웹사이트에 있는 대부분의 설명서는 각각의 데비안 패키지에서 발췌한 것입니다. 
아래에서 패키지 이름 및 온라인 버전에 대한 링크를 위한 매뉴얼을 선택하십시오.
</li>
  <li>: HOWTO: 이름에서 알 수 있듯이, <a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">HOWTO</a> 문서는 특정 작업을 수행하는 방법을 설명합니다. 즉, 어떤 작업을 수행하는 <em>how to</em>에 대한 상세하고 실용적인 조언을 제공합니다.
  </li>
  <li>FAQ: <em>frequently asked questions</em>에 대한 몇 가지 문서를 작성했습니다. 
  데비안 관련 질문은 <a href="manuals/debian-faq/">데비안 FAQ</a>에서 답변됩니다. 
  또한 <a href="../CD/faq/">데비안 CD/DVD 이미지에 대한 FAQ</a>가 있어 설치 미디어에 대한 모든 질문에 답변합니다.
  </li> 
  <li>다른 (짧은) 문서: 짧은 명령 <a href="#other">목록</a>.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 데비안 매뉴얼 완전한 목록 과 기타 문서는 <a href="ddp">Debian Documentation Project</a> 페이지 참조하세요. 
추가로, 데비안 GNU/리눅스를 위한 여러 사용자 지향 문서가 <a href="books">출판된 책</a>으로 가능합니다.</p>
</aside>

<p>여기 있는 많은 매뉴얼이 온라인과 데비안 패키지에서 가능. 사실, 웹사이트 매뉴얼 대부분은 데비안 패키지에서 추출한 것.
패키지명과 링크 온라인 버전을 아래에서 고르세요.</p>

<h3>데비안 특정 매뉴얼</h3>

<div class="line">
  <div class="item col50">
    
    <h4><a href="user-manuals">사용자 매뉴얼</a></h4>
    <ul>
      <li><a href="user-manuals#faq">데비안 GNU/리눅스 FAQ</a></li>
      <li><a href="user-manuals#install">데비안 설치 안내서</a></li>
      <li><a href="user-manuals#relnotes">데비안 릴리스 노트</a></li>
      <li><a href="user-manuals#refcard">데비안 참조 카드</a></li>
      <li><a href="user-manuals#debian-handbook">데비안 관리자 핸드북</a></li>
      <li><a href="user-manuals#quick-reference">데비안 참조</a></li>
      <li><a href="user-manuals#securing">데비안 보안 매뉴얼</a></li>
      <li><a href="user-manuals#aptitude">aptitude 사용자 매뉴얼</a></li>
      <li><a href="user-manuals#apt-guide">APT 사용자 가이드</a></li>
      <li><a href="user-manuals#apt-offline">APT 오프라인</a></li>
      <li><a href="user-manuals#java-faq">데비안 GNU/리눅스 및 Java FAQ</a></li>
      <li><a href="user-manuals#hamradio-maintguide">데비안 Hamradio 관리자 가이드</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">개발자 매뉴얼</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">데비안 정책 메뉴얼</a></li>
      <li><a href="devel-manuals#devref">데비안 개발자 참조</a></li>
      <li><a href="devel-manuals#debmake-doc">데비안 메인테이너 가이드</a></li>
      <li><a href="devel-manuals#packaging-tutorial">데비안 패키징 소개</a></li>
      <li><a href="devel-manuals#menu">데비안 메뉴 시스템</a></li>
      <li><a href="devel-manuals#d-i-internals">데비안 설치 프로그램 내부</a></li>
      <li><a href="devel-manuals#dbconfig-common">패키지 메인테이너를 쓰는 데이터베이스를 위한 가이드</a></li>
      <li><a href="devel-manuals#dbapp-policy">데이터베이스를 쓰는 패키지를 위한 정책</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">데비안 프로젝트 역사</a></button></p>

<h2><a id="other">다른 (짧은) 문서</a></h2>

<p>다음 문서에는 빠른, 짧은 명령이 있습니다:</p>

<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 위 자원을 보고 데비안 관련 질문에 대한 답을 못 찾았다면,
<a href="../support">지원 페이지</a>를 보세요.</p>
</aside>

  <dt><strong><a href="https://tldp.org/docs.html#man">Manpage</a></strong></dt>
    <dd>전통적으로, 모든 유닉스 프로그램은 
    <em>manpage</em>로 문서화 되며,
        참조 매뉴얼은 <tt>man</tt> 명령을 통해 가능합니다.
        초보자용이 아니며, 명령의 모든 특징과 기능이 있습니다. 
        모든 맨페이지 완전한 저장소는 
        <a
        href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>
        repository of all manpages available in Debian is online: <a
        href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">info 파일</a></strong></dt>
    <dd>많은 GNU 소프트웨어는 맨페이지 대신 <em>info 
        파일</em>로 문서화됩니다. 이 파일은 자세한 정보, 옵션 및 예제를 포함합니다.
	Info 페이지는  <tt>info</tt> 명령을 통해 가능합니다.
    </dd>

  <dt><strong>README 파일</strong></dt>
    <dd>README 파일은 한 아이템(대개 패키지)을 설명하는 단순 텍스트 파일입니다.
이런 파일을 데비안 시스템 <tt>/usr/share/doc/</tt> 서브디렉터리에서 많이 찾을 수 있습니다.
README 파일에 추가적으로, 이 디렉터리에는 설정 예가 있습니다.
큰 프로그램 문서는 전형적으로 별도 패키지로 제공됩니다(원래 패키지와 같은 이름에 <em>-doc</em>으로 끝남)
    </dd>

  <dt><strong><a href="https://www.debian.org/doc/manuals/refcard/refcard">데비안 GNU/리눅스 참조 카드</a></strong></dt>
    <dd>
참조 카드는 특정 (하위) 시스템에 대한 매우 짧은 요약이며 일반적으로 가장 일반적인 명령을 하나의 페이지에 나열합니다. 
데비안 GNU/리눅스 참조 카드는 데비안 시스템의 가장 중요한 명령어 목록을 제공합니다. 
최소한 파일, 디렉터리 및 명령줄에 대한 기본 지식이 필요합니다. 
초보 사용자는 <a href="user-manuals#quick-reference">데비안 참조</a>를 먼저 읽고 싶을 수 있습니다.
    </dd>
</dl>
</p>
