#use wml::debian::cdimage title="Kontrollera äktheten på Debianavbildningar" BARETITLE=true
#use wml::debian::translation-check translation="942f98b47dc32183d181b4c309c038a3e8c33f3d"

<p>
Officiella utgåvor av Debian installations- och liveavbildningar kommer med signerade checksum-filer;
kolla efter dem tillsammans med avbildningarna i mapparna <code>iso-cd</code>,
<code>jigdo-dvd</code>, <code>iso-hybrid</code> osv.
Dessa gör det möjligt för dig att kontrollera att avbildningen du har hämtat är
korrekt. För det första så kan kontrollsumman användas för att kontrollera att
CD-avbildningen inte har skadats under nedladdningen.
För det andra så tillåter signaturen på checksumfilen att kontrollera att
avbildningarna är de som är officiellt utgivna av Debian och att de
inte har manipulerats.
</p>

<p>
För att kontrollera innehållet på en avbildningsfil, säkerställ att du använder
dig av rätt kontrollsummeverktyg.
Kryptografiskt starka kontrollsummealgoritmer
(SHA256 och SHA512) finns tillgängliga för alla utgåvor; du bör använda
verktygen <code>sha256sum</code> eller <code>sha512sum</code> för att arbeta
med dessa.
</p>


<p>
För att säkerställa att själva checksum-filerna är korrekta, använd en
OpenPGP-implementation (så som GnuPG, Sequoia-PGP, PGPainless eller GopenPGP)
för att verifiera dem mot motsvarande signatur-fil (t.ex.
<code>SHA512SUMS.sign</code>).
Nycklarna som används för dessa signaturer finns alla i <a
href="https://keyring.debian.org">Debians OpenPGP-nyckelring</a> och det bästa
sättet att kontrollera dem är att använda den nyckelringen för att validera dem
via "the web of trust".
För att göra livet enklare för person som inte har enkel tillgång till en
Debianmaskin, så hittar du här detaljerna rörande de nycklar som har använts
för att signera utgåvor nyligen, samt länkar för att hämta de publika nycklarna
direkt:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
