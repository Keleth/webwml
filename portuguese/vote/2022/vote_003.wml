#use wml::debian::translation-check translation="9b613d7447827b46baa87229fc9235472e047a76"
<define-tag pagetitle>Resolução Geral: firmwares não livres</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Período de discussão:</th>
	<td>18/08/2022</td>
	<td>15/09/2022</td>
      </tr>
      <tr>
	<th>Período de votação:</th>
	<td>Domingo 18/09/2022 00:00:00 UTC</td>
	<td>Sábado 01/10/2022 23:59:59 UTC</td>
      </tr>
    </table>

    O período de discussão foi estendido por mais 7 dias pelo líder do Projeto Debian.
    [<a href='https://lists.debian.org/debian-vote/2022/09/msg00037.html'>e-mail</a>]</li>

    <vproposera />
    <p>Steve McIntyre [<email 93sam@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00001.html'>texto da proposta</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00194.html'>emenda</a>]
    </p>
    <vsecondsa />
    <ol>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00002.html'>e-mail</a>]</li>
        <li>Luca Boccassi [<email bluca@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00003.html'>e-mail</a>]</li>
        <li>Ansgar [<email ansgar@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00004.html'>e-mail</a>]</li>
        <li>Louis-Philippe Véronneau [<email pollo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00005.html'>e-mail</a>]</li>
        <li>Sebastian Ramacher [<email sramacher@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00006.html'>e-mail</a>]</li>
        <li>Samuel Henrique [<email samueloph@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00007.html'>e-mail</a>]</li>
        <li>Timo Röhling [<email roehling@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00008.html'>e-mail</a>]</li>
        <li>Philip Hands [<email philh@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00009.html'>e-mail</a>]</li>
        <li>Joerg Jaspert [<email joerg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00011.html'>e-mail</a>]</li>
        <li>Cyril Brulebois [<email kibi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00014.html'>e-mail</a>]</li>
        <li>Iain Lane [<email laney@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00015.html'>e-mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00023.html'>e-mail</a>]</li>
        <li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00029.html'>e-mail</a>]</li>
        <li>Anton Gladky [<email gladk@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00030.html'>e-mail</a>]</li>
        <li>Moritz Mühlenhoff [<email jmm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00031.html'>e-mail</a>]</li>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00034.html'>e-mail</a>]</li>
    </ol>
    <vtexta />

<h3>Opção 1: Apenas um instalador, incluindo firmwares não livres</h3>

<p>
Incluiremos pacotes de firmwares não livres da seção "non-free-firmware" do
repositório Debian em nossas mídias oficiais (imagens do instalador e imagens
live). Os binários dos firmwares incluídos serão <b>normalmente</b> ativados por
padrão quando o sistema determinar que eles são necessários, mas, sempre que
possível, incluiremos maneiras para os(as) usuários(as) desativarem isso na
inicialização (opção do menu de inicialização, linha de comando do kernel, etc).
</p>

<p>
Quando o instalador/sistema live estiver em execução, forneceremos
informações ao(à) usuário(a) sobre qual firmware foi carregado (livre e não
livre) e também armazenaremos essas informações no sistema de destino para que
os(as) usuários(as) possam encontrá-las mais tarde.
Onde o firmware não livre for considerado necessário, o sistema de destino
será <b>também</b> configurado para usar o componente de firmware não livre por
padrão no arquivo do apt sources.list.
Nossos(as) usuários(as) devem receber atualizações de segurança e correções
importantes para binários de firmware como qualquer outro software instalado.
</p>

<p>
Publicaremos essas imagens como mídias oficiais do Debian, substituindo o
conjuntos de mídias atuais que não incluem pacotes de firmwares não livres.
</p>

    <vproposerb />
    <p>Gunnar Wolf [<email gwolf@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00046.html'>texto da proposta</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00215.html'>emenda</a>]
    </p>
    <vsecondsb />
    <ol>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00047.html'>e-mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00048.html'>e-mail</a>]</li>
        <li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00053.html'>e-mail</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00055.html'>e-mail</a>]</li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00060.html'>e-mail</a>]</li>
        <li>Tiago Bortoletto Vaz [<email tiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00093.html'>e-mail</a>]</li>
        <li>Laura Arjona Reina [<email larjona@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00120.html'>e-mail</a>]</li>
        <li>Jonathan Carter [<email jcc@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00122.html'>e-mail</a>]</li>
        <li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00168.html'>e-mail</a>]</li>
    </ol>
    <vtextb />

<h3>Opção 2: Recomendamos o instalador contendo firmwares não livres</h3>

<p>
Incluiremos pacotes de firmwares não livres da seção "non-free-firmware" do
repositório Debian em nossas mídias oficiais (imagens do instalador e imagens
live). Os binários dos firmwares incluídos serão <b>normalmente</b> ativados por
padrão quando o sistema determinar que eles são necessários, mas, sempre que
possível, incluiremos maneiras para os(as) usuários(as) desativarem isso na
inicialização (opção do menu de inicialização, linha de comando do kernel, etc).
</p>

<p>
Quando o instalador/sistema live estiver em execução, forneceremos
informações ao(à) usuário(a) sobre qual firmware foi carregado (livre e não
livre) e também armazenaremos essas informações no sistema de destino para que
os(as) usuários(as) possam encontrá-las mais tarde.
Onde o firmware não livre for considerado necessário, o sistema de destino
será <b>também</b> configurado para usar o componente de firmware não livre por
padrão no arquivo do apt sources.list.
Nossos(as) usuários(as) devem receber atualizações de segurança e correções
importantes para binários de firmware como qualquer outro software instalado.
</p>

<p>
Apesar de publicarmos essas imagens como mídias oficiais do Debian, elas
<b>não</b> substituirão os conjuntos de mídias atuais que não incluem pacotes de
firmwares não livres, mas serão oferecidas juntamente. Imagens que incluem
firmwares não livres será apresentadas com mais destaque, para que os(as)
recém-chegados(as) as encontrem mais facilmente; imagens totalmente livres não
ficarão escondidas; elas serão disponibilizadas nas mesmas páginas do projeto,
porém, com menos prioridade visual.
</p>

    <vproposerc />
    <p>Bart Martens [<email bartm@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00106.html'>texto da proposta</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/09/msg00041.html'>emenda</a>]
    </p>
    <vsecondsc />
    <ol>
        <li>Stefano Zacchiroli [<email zack@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00116.html'>e-mail</a>]</li>
        <li>Laura Arjona Reina [<email larjona@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00119.html'>e-mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00123.html'>e-mail</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00128.html'>e-mail</a>]</li>
        <li>Philip Rinn [<email rinni@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00160.html'>e-mail</a>]</li>
        <li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00044.html'>e-mail</a>]</li>
        <li>Paul Wise [<email pabs@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00055.html'>e-mail</a>]</li>
        <li>Simon Josefsson [<email jas@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00086.html'>e-mail</a>]</li>
    </ol>
    <vtextc />

<h3>Opção 3: Permitir a apresentação de instaladores não livres junto com os instaladores livres</h3>

<p>O Projeto Debian está autorizado a fazer mídias da distribuição (imagens do
instalador e imagens live) contendo softwares não livres do repositório Debian
disponível para download junto com as mídias livres, de forma que o(a) usuário(a)
seja informado(a) antes de baixar quais são as mídias livres.
</p>

    <vproposerd />
    <p>Simon Josefsson [<email jas@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00173.html'>texto da proposta</a>]
    </p>
    <vsecondsd />
    <ol>
        <li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00068.html'>e-mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00071.html'>e-mail</a>]</li>
        <li>Hubert Chathi [<email uhoreg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00074.html'>e-mail</a>]</li>
        <li>Guilhem Moulin [<email guilhem@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00167.html'>e-mail</a>]</li>
        <li>Santiago Ruano Rincón [<email santiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00211.html'>e-mail</a>]</li>
        <li>Shengjing Zhu [<email zhsj@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00000.html'>e-mail</a>]</li>
    </ol>
    <vtextd />

<h3>Opção 4: Instalador com softwares não livres não faz parte do Debian</h3>

<p>
Continuamos a defender o espírito do §1 do Contrato Social Debian
que diz:
</p>

<pre>
   O Debian permanecerá 100% livre

   Disponibilizamos as definições que usamos para determinar se um software é
   "livre" no documento intitulado "Definição Debian de Software Livre".
   Prometemos que o sistema Debian e todos seus componentes serão livres de
   acordo com essas definições. Iremos fornecer suporte às pessoas que
   desenvolvem ou usam software livre e não livre no Debian. Nunca faremos o
   sistema depender de um componente não livre.
</pre>

<p>
Portanto, não incluiremos nenhum software não livre no Debian, nem no
repositório principal ou no instalador/live/cloud ou outras imagens oficiais, e
não habilitaremos nada do non-free ou do contrib por padrão.
</p>

<p>
Também continuamos a defender o espírito do §5 do Contrato Social Debian,
que diz:
</p>

<pre>
   Programas que não atendem nossos padrões de software livre

   Reconhecemos que alguns de nossos(as) usuários(as) precisam usar
   softwares que não atendem à Definição Debian de Software Livre. Criamos
   as áreas "contrib" e "non-free"
   em nossos repositórios para estes softwares. Os pacotes contidos
   nessas áreas não são parte do sistema Debian, embora tenham sido
   configurados para funcionar no Debian. Incentivamos os fornecedores
   de CDs a ler as licenças dos pacotes armazenados nessas áreas,
   a fim de determinar se podem distribuí-los em seus CDs. Portanto,
   embora softwares não livres não sejam parte do Debian, oferecemos
   suporte à sua utilização e disponibilizamos infraestrutura para
   pacotes não livres (como nosso sistema de controle de bugs e listas
   de discussão).
</pre>

<p>
Reforçando assim a interpretação de que qualquer instalador ou imagem com
software não livre não faz parte do sistema Debian, mas apoiamos seu uso e
damos boas-vindas a terceiros para distribuir tal trabalho.
</p>

    <vproposere />
    <p>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00038.html'>texto da proposta</a>]
    </p>
    <vsecondse />
    <ol>
        <li>Richard Laager [<email rlaager@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00039.html'>e-mail</a>]</li>
        <li>Ansgar [<email ansgar@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00040.html'>e-mail</a>]</li>
        <li>Simon Richter [<email sjr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00045.html'>e-mail</a>]</li>
        <li>Kunal Mehta [<email legoktm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00046.html'>e-mail</a>]</li>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00047.html'>e-mail</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00053.html'>e-mail</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00071.html'>e-mail</a>]</li>
    </ol>
    <vtexte />

<h3>Opção 5: Alterar o CS para firmwares não livres no instalador, um único instalador</h3>

<p>
Esta opção de votação substitui o Contrato Social Debian (um documento de
fundação) sob o ponto 4.1.5 da constituição e, portanto, requer uma maioria de
3:1.
</p>

<p>
O Contrato Social Debian será substituído por uma nova versão que é idêntica à
versão atual em todos os aspectos, exceto que adiciona a seguinte frase ao final
do ponto 5:
</p>

<pre>
    A mídia oficial do Debian pode incluir firmware que não faz parte do sistema
    Debian para permitir o uso do Debian com hardware que requer tal firmware.
</pre>

<p>
O Projeto Debian também faz a seguinte declaração sobre uma questão do dia:
</p>

<p>
Incluiremos pacotes de firmwares não livres da seção "non-free-firmware" do
repositório Debian em nossas mídias oficiais (imagens do instalador e imagens
live). Os binários dos firmwares incluídos serão <b>normalmente</b> ativados por
padrão quando o sistema determinar que eles são necessários, mas, sempre que
possível, incluiremos maneiras para os(as) usuários(as) desativarem isso na
inicialização (opção do menu de inicialização, linha de comando do kernel, etc).
</p>

<p>
Quando o instalador/sistema live estiver em execução, forneceremos
informações ao(à) usuário(a) sobre qual firmware foi carregado (livre e não
livre) e também armazenaremos essas informações no sistema de destino para que
os(as) usuários(as) possam encontrá-las mais tarde.
Onde o firmware não livre for considerado necessário, o sistema de destino
será <b>também</b> configurado para usar o componente de firmware não livre por
padrão no arquivo do apt sources.list.
Nossos(as) usuários(as) devem receber atualizações de segurança e correções
importantes para binários de firmware como qualquer outro software instalado.
</p>

<p>
Publicaremos essas imagens como mídias oficiais do Debian, substituindo o
conjuntos de mídias atuais que não incluem pacotes de firmwares não livres.
</p>

    <vproposerf />
    <p>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00166.html'>texto da proposta</a>]
    </p>
    <vsecondsf />
    <ol>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00167.html'>e-mail</a>]</li>
        <li>Timo Röhling [<email roehling@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00168.html'>e-mail</a>]</li>
        <li>Tiago Bortoletto Vaz [<email tiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00169.html'>e-mail</a>]</li>
        <li>Étienne Mollier [<email emollier@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00170.html'>e-mail</a>]</li>
        <li>Judit Foglszinger [<email urbec@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00171.html'>e-mail</a>]</li>
        <li>David Prévot [<email taffit@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00173.html'>e-mail</a>]</li>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00174.html'>e-mail</a>]</li>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00176.html'>e-mail</a>]</li>
        <li>Didier Raboud [<email odyx@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00180.html'>e-mail</a>]</li>
    </ol>
    <vtextf />

<h3>Opção 6: Alterar o CS para firmwares não livres no instalador, manter ambos os instaladores</h3>

<p>
Esta opção de votação substitui o Contrato Social Debian (um documento de
fundação) sob o ponto 4.1.5 da constituição e, portanto, requer uma maioria de
3:1.
</p>

<p>
O Contrato Social Debian será substituído por uma nova versão que é idêntica à
versão atual em todos os aspectos, exceto que adiciona a seguinte frase ao final
do ponto 5:
</p>

<pre>
    A mídia oficial do Debian pode incluir firmware que não faz parte do sistema
    Debian para permitir o uso do Debian com hardware que requer tal firmware.
</pre>

<p>
O Projeto Debian também faz a seguinte declaração sobre uma questão do dia:
</p>

<p>
Incluiremos pacotes de firmwares não livres da seção "non-free-firmware" do
repositório Debian em nossas mídias oficiais (imagens do instalador e imagens
live). Os binários dos firmwares incluídos serão <b>normalmente</b> ativados por
padrão quando o sistema determinar que eles são necessários, mas, sempre que
possível, incluiremos maneiras para os(as) usuários(as) desativarem isso na
inicialização (opção do menu de inicialização, linha de comando do kernel, etc).
</p>

<p>
Quando o instalador/sistema live estiver em execução, forneceremos
informações ao(à) usuário(a) sobre qual firmware foi carregado (livre e não
livre) e também armazenaremos essas informações no sistema de destino para que
os(as) usuários(as) possam encontrá-las mais tarde.
Onde o firmware não livre for considerado necessário, o sistema de destino
será <b>também</b> configurado para usar o componente de firmware não livre por
padrão no arquivo do apt sources.list.
Nossos(as) usuários(as) devem receber atualizações de segurança e correções
importantes para binários de firmware como qualquer outro software instalado.
</p>

<p>
Publicaremos essas imagens como mídias oficiais do Debian, juntamente com os
conjuntos de mídias atuais que não incluem pacotes de firmwares não livres.
</p>

    <vquorum />

     <p>
        Com a lista atual de <a href="vote_003_quorum.log">desenvolvedores(as)
        votantes</a>, temos:
     </p>
    <pre>
#include 'vote_003_quorum.txt'
    </pre>
#include 'vote_003_quorum.src'

<p>Obs: Não usamos o formato "desenvolvedores(as)" para não gerar confusão com
os parênteses da fórmula.</p>

    <vstatistics />
    <p>
	Para esta GR, como sempre, serão coletadas
#              <a href="https://vote.debian.org/~secretary/gr_non_free_firmware/">estatísticas</a>
               <a href="suppl_003_stats">estatísticas</a>
               sobre as cédulas recebidas e os reconhecimentos enviados
               periodicamente durante o período de votação.
               Além disso, a lista de <a
               href="vote_003_voters.txt">eleitores(as)</a> será registrada.
               E também, o <a href="vote_003_tally.txt">registro de contagem</a>
               estará disponível para visualização.
         </p>

    <vmajorityreq />
    <p>
      As propostas 5 e 6 precisam de uma maioria qualificada de 3:1
    </p>
#include 'vote_003_majority.src'

    <voutcome />
#include 'vote_003_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Secretário do projeto Debian</a>
      </address>
