#use wml::debian::translation-check translation="2a3fa2ac1b49f491737f3c89828986a43c00a61e"
<define-tag pagetitle>Debian 10 atualizado: 10.9 lançado</define-tag>
<define-tag release_date>2021-03-27</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian tem o prazer de anunciar a nona atualização de sua
versão estável (stable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, junto com alguns ajustes para problemas sérios. Avisos de segurança
já foram publicados em separado e são referenciados quando disponíveis.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de se desfazer das antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p> 

<p>As pessoas que frequentemente instalam atualizações de security.debian.org
não terão que atualizar muitos pacotes, e a maioria dessas atualizações estão
incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
ao apontar o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction avahi "Remove mecanismo avahi-daemon-check-dns, que não é mais necessário">
<correction base-files "Atualiza /etc/debian_version para versão pontual 10.9">
<correction cloud-init "Evita gravar senhas geradas em arquivos de log lidos por qualquer pessoa [CVE-2021-3429]">
<correction debian-archive-keyring "Adiciona chaves bullseye; remove chaves jessie">
<correction debian-installer "Usa ABI do núcleo Linux 4.19.0-16">
<correction debian-installer-netboot-images "Reconstrói contra proposed-updates">
<correction exim4 "Corrige uso concorrente de conexões TLS sob GnuTLS; corrige verificação de certificado TLS com CNAMES; README.Debian: documenta limitação/extensão da verificação de certificado de servidor na configuração padrão">
<correction fetchmail "Não reporta mais <q>System error during SSL_connect(): Success</q>; remove verificação de versão OpenSSL">
<correction fwupd "Adiciona suporte a SBAT">
<correction fwupd-amd64-signed "Adiciona suporte a SBAT">
<correction fwupd-arm64-signed "Adiciona suporte a SBAT">
<correction fwupd-armhf-signed "Adiciona suporte a SBAT">
<correction fwupd-i386-signed "Adiciona suporte a SBAT">
<correction fwupdate "Adiciona suporte a SBAT">
<correction fwupdate-amd64-signed "Adiciona suporte a SBAT">
<correction fwupdate-arm64-signed "Adiciona suporte a SBAT">
<correction fwupdate-armhf-signed "Adiciona suporte a SBAT">
<correction fwupdate-i386-signed "Adiciona suporte a SBAT">
<correction gdnsd "Corrige estouro de pilha com endereços IPv6 excessivamente grandes [CVE-2019-13952]">
<correction groff "Reconstrói contra ghostscript 9.27">
<correction hwloc-contrib "Habilita suporte para arquitetura ppc64el">
<correction intel-microcode "Atualiza vários microcódigos">
<correction iputils "Corrige erros de ping rounding; corrige corrupção de alvo tracepath">
<correction jquery "Corrige vulnerabilidades de execução de código não confiável [CVE-2020-11022 CVE-2020-11023]">
<correction libbsd "Corrige problema de leitura fora do limite [CVE-2019-20367]">
<correction libpano13 "Corrige vulnerabilidade de string de formato">
<correction libreoffice "Não carrega encodings.py do diretório atual">
<correction linux "Nova versão estável original (upstream); atualiza ABI para -16; rotaciona chaves de assinatura de boot; rt: atualiza para 4.19.173-rt72">
<correction linux-latest "Atualiza kernel ABI para -15; atualiza kernel ABI para -16">
<correction linux-signed-amd64 "Nova versão estável original (upstream); atualiza ABI para -16; rotaciona chaves de assinatura de boot; rt: atualiza para 4.19.173-rt72">
<correction linux-signed-arm64 "Nova versão estável original (upstream); atualiza ABI para -16; rotaciona chaves de assinatura de boot; rt: atualiza para 4.19.173-rt72">
<correction linux-signed-i386 "Nova versão estável original (upstream); atualiza ABI para -16; rotaciona chaves de assinatura de boot; rt: atualiza para 4.19.173-rt72">
<correction lirc "Normaliza valor embutido ${DEB_HOST_MULTIARCH} em /etc/lirc/lirc_options.conf para encontrar arquivos de configuração não modificados em todas as arquiteturas; recomenda gir1.2-vte-2.91 em vez do inexistente gir1.2-vte">
<correction m2crypto "Corrige falha de teste com versões OpenSSL recentes">
<correction openafs "Corrige conexões de saída após unix epoch time 0x60000000 (14 de janeiro de 2021)">
<correction portaudio19 "Manipula EPIPE de alsa_snd_pcm_poll_descriptors, corrigindo quebra">
<correction postgresql-11 "Nova versão estável original (upstream); corrige vazamento de informações em mensagens de erro constraint-violation [CVE-2021-3393]; corrige CREATE INDEX CONCURRENTLY para aguardar por transações preparadas concorrentes">
<correction privoxy "Problemas de segurança [CVE-2020-35502 CVE-2021-20209 CVE-2021-20210 CVE-2021-20211 CVE-2021-20212 CVE-2021-20213 CVE-2021-20214 CVE-2021-20215 CVE-2021-20216 CVE-2021-20217 CVE-2021-20272 CVE-2021-20273 CVE-2021-20275 CVE-2021-20276]">
<correction python3.7 "Corrige injeção CRLF em http.client [CVE-2020-26116]; corrige estouro de buffer em PyCArg_repr em _ctypes/callproc.c [CVE-2021-3177]">
<correction redis "Corrige uma série de problemas de estouro de inteiro em sistemas 32-bit [CVE-2021-21309]">
<correction ruby-mechanize "Corrige problema de injeção de comando [CVE-2021-21289]">
<correction systemd "core: certifica-se de restaurar o id de comando de controle e também corrige um segfault; seccomp: permite desligar filtragem seccomp via variável de ambiente">
<correction uim "libuim-data: realiza conversão symlink_to_dir de /usr/share/doc/libuim-data no pacote ressuscitado para atualizações limpas a partir do stretch">
<correction xcftools "Corrige vulnerabilidade de estouro de inteiro [CVE-2019-5086 CVE-2019-5087]">
<correction xterm "Corrige limite superior para buffer de seleção, relativo a caracteres de combinação [CVE-2021-27135]">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
A equipe de Segurança já lançou um aviso para cada uma dessas
atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2021 4826 nodejs>
<dsa 2021 4844 dnsmasq>
<dsa 2021 4845 openldap>
<dsa 2021 4846 chromium>
<dsa 2021 4847 connman>
<dsa 2021 4849 firejail>
<dsa 2021 4850 libzstd>
<dsa 2021 4851 subversion>
<dsa 2021 4853 spip>
<dsa 2021 4854 webkit2gtk>
<dsa 2021 4855 openssl>
<dsa 2021 4856 php7.3>
<dsa 2021 4857 bind9>
<dsa 2021 4858 chromium>
<dsa 2021 4859 libzstd>
<dsa 2021 4860 openldap>
<dsa 2021 4861 screen>
<dsa 2021 4862 firefox-esr>
<dsa 2021 4863 nodejs>
<dsa 2021 4864 python-aiohttp>
<dsa 2021 4865 docker.io>
<dsa 2021 4867 grub-efi-amd64-signed>
<dsa 2021 4867 grub-efi-arm64-signed>
<dsa 2021 4867 grub-efi-ia32-signed>
<dsa 2021 4867 grub2>
<dsa 2021 4868 flatpak>
<dsa 2021 4869 tiff>
<dsa 2021 4870 pygments>
<dsa 2021 4871 tor>
<dsa 2021 4872 shibboleth-sp>
</table>



<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas
na versão estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>informações da versão estável (stable) (notas de lançamento, errata, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional livre Debian.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>
