<define-tag pagetitle>O suporte a longo prazo do Debian 8 chega ao final da vida</define-tag>
<define-tag release_date>2020-07-09</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="9c0ebe940eaf29e78367427aea8e02f46fb70bcd"

##
## Translators:
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>A equipe do Debian suporte de longo prazo (LTS - Long Term Support) anuncia
que o suporte ao Debian 8 <q>jessie</q> atingiu seu fim de vida em 30 de junho
de 2020, cinco anos após seu lançamento inicial em 26 de abril de 2015.</p>

<p>O Debian não fornecerá mais atualizações de segurança para o Debian 8. Um
subconjunto de pacotes do <q>jessie</q> será suportado por terceiros.
Informações detalhadas podem ser encontradas em
<a href="https://wiki.debian.org/LTS/Extended">LTS estendido</a>.</p>

<p>A equipe do LTS preparará a transição para o Debian 9 <q>stretch</q>, que é
a versão atual oldstable. A equipe do LTS assumirá o suporte da equipe de
segurança em 6 de julho de 2020, enquanto a última atualização pontual para o
<q>stretch</q> será lançada em 18 de julho de 2020.</p>

<p>O Debian 9 também receberá suporte de longo prazo por cinco anos após seu
lançamento inicial, com seu suporte terminando em 30 de junho de 2022. As
arquiteturas suportadas permanecem amd64, i386, armel e armhf. Além disso,
temos o prazer de anunciar que, pela primeira vez, o suporte será estendido para
incluir a arquitetura arm64.</p>

<p>Para obter mais informações sobre o uso do <q>stretch</q> LTS, e fazendo
atualização a partir do <q>jessie</q> LTS, consulte
<a href="https://wiki.debian.org/LTS/Using">usando o LTS</a>.</p>

<p>O Debian e sua equipe LTS gostaria de agradecer a todos(as) os(as)
contribuidores(as), usuários(as), desenvolvedores(as) e patrocinadores(as) que
estão possibilitando prolongar a vida útil das versões estáveis anteriores, e
que fizeram deste LTS um sucesso.</p>

<p>Se você confia no Debian LTS, por favor considere
<a href="https://wiki.debian.org/LTS/Development">ingressar na equipe</a>,
fornecendo patches, testando ou
<a href="https://wiki.debian.org/LTS/Funding">financiando os esforços</a>.</p>

<h2>Sobre o Debian</h2>

<p>
O Projeto Debian foi fundado em 1993 por Ian Murdock para ser um verdadeiro
projeto comunitário livre. Desde então, o projeto cresceu para ser um dos
maiores e mais influentes projetos de código aberto. Milhares de
voluntários(as) de todo o mundo trabalham juntos(as) para criar e
manter o software Debian. Disponível em 70 idiomas e
suportando uma enorme variedade de tipos de computadores, o Debian se
autointitula de <q>sistema operacional universal</q>.
</p>

<h2>Mais informações</h2>
<p>Mais informações sobre o Debian suporte de longo prazo (Long Term Support)
podem ser encontradas em
<a href="https://wiki.debian.org/LTS/">https://wiki.debian.org/LTS/</a>.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a> ou envie um email para
&lt;press@debian.org&gt;.</p>
