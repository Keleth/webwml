# From: Marcos Castilho <marcos@inf.ufpr.br>

<define-tag pagetitle>Departamento de Inform&aacute;tica -- Universidade Federal do Paran&aacute;, Brasil</define-tag>
<define-tag webpage>http://www.inf.ufpr.br/</define-tag>

#use wml::debian::users
#use wml::debian::translation-check translation="c270aa0cfc57d6f5f792bfda05c0a3161f8b0690"

<p>
Usamos Debian/GNU Linux em todos os lugares e fornecemos um espelho Debian desde
1999, e um espelho Debian Security desde 2011. Nosso data center gerencia a
filtragem e roteamento de pacotes de uma rede de 20 Gb, um servidor DNS
autoritativo e um servidor de e-mail, todos rodando variantes do Debian estável
e instável variantes. Usando máquinas Debian altamente otimizadas, somos capazes
de fornecer mais de 40 espelhos para projetos de código aberto, sendo o maior
espelho não comercial do Hemisfério Sul e um dos maiores do mundo.
</p>

<p>
Com o Debian também fornecemos infraestrutura de virtualização para projetos de
pesquisa. Temos um armazenamento bruto de 1 PB em múltiplas implementações de
sistemas de arquivos distribuídos, mais de 1.700 núcleos de processamento e mais
de 10 TB de RAM. Gerenciamos mais de 150 máquinas virtuais, todas rodando
Debian, fornecendo uma variedade de serviços para a comunidade, como edição de
documentos colaborativa, plataformas de aprendizagem, videochamadas, controle de
versão e bancos de dados científicos. Para pesquisas de ponta, também
gerenciamos um cluster com mais de 46 mil núcleos CUDA com Debian. Para os(as)
estudantes, oferecemos recursos de inicialização pela rede, habilitando mais de
370 terminais rodando Debian e derivados.
</p>

<p>
O Debian nos fornece estabilidade, segurança e fácil manutenção. Nossa
infraestrutura é gerenciada majoritariamente pelo Centro de Computação
Científica e Software Livre (C3SL), que é um grupo de pesquisa focado no
desenvolvimento de soluções de código aberto, sendo responsável por gerenciar
e hospedar múltiplos projetos governamentais.
</p>
