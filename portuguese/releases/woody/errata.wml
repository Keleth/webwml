#use wml::debian::template title="Debian GNU/Linux 3.0 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="534d1b782cfb92f46dc41fd064f779fffc329b12"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>

<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>A equipe de segurança do Debian emite atualizações para pacotes na versão
estável (stable), nos quais eles(as) identificaram problemas relacionados à
segurança. Por favor consulte as <a href="$(HOME)/security/">páginas de segurança</a>
para obter informações sobre quaisquer problemas de segurança identificados no
<q>woody</q>.</p>

<p>Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://security.debian.org/ woody/updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>

<toc-add-entry name="pointrelease">Lançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão já lançada é atualizada. Geralmente, as atualizações são indicadas
como lançamentos pontuais.</p>

<ul>
  <li>A primeira versão pontual, 3.0r1, foi lançada em
    <a href="$(HOME)/News/2002/20021216">16 de dezembro de 2002</a>.</li>
  <li>A segunda versão pontual, 3.0r2, foi lançada em
    <a href="$(HOME)/News/2003/20031121a">21 de novembro de 2003</a>.</li>
  <li>A terceira versão pontual, 3.0r3, foi lançada em
    <a href="$(HOME)/News/2004/20041026">26 de outubro de 2004</a>.</li>
  <li>A quarta versão pontual, 3.0r4, foi lançada em
    <a href="$(HOME)/News/2005/20050101">1de janeiro de 2005</a>.</li>
  <li>A quinta versão pontual, 3.0r5, foi lançada em
    <a href="$(HOME)/News/2005/20050416">16 de abril de 2005</a>.</li>
  <li>A sexta versão pontual, 3.0r6, foi lançada em
    <a href="$(HOME)/News/2005/20050602">2 de junho de 2005</a>.</li>
</ul>

<ifeq <current_release_woody> 3.0r0 "

<p>Ainda não há lançamentos pontuais para o Debian 3.0.</p>" "

<p>Consulte o <a
href=http://archive.debian.org/debian/dists/woody/ChangeLog>
ChangeLog</a> (e <a
href=http://archive.debian.org/debian-non-US/dists/woody/non-US/ChangeLog>
ChangeLog para non-US</a>) para obter detalhes sobre mudanças entre 3.0r0 e
<current_release_woody/>.</p>"/>

<p>As correções na versão estável (stable) lançada geralmente passam por
um longo período de testes antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://archive.debian.org/debian/dists/woody-proposed-updates/">
dists/woody-proposed-updates</a> de qualquer espelho do repositório Debian
(e na mesma localização do nosso
<a href="http://archive.debian.org/debian-non-US/dists/woody-proposed-updates/">
servidor non-US</a> e em seus espelhos).</p>

<p>Se você usa o APT para atualizar seus pacotes, poderá instalar as
atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 3.0
  deb http://archive.debian.org/debian proposed-updates main contrib non-free
  deb http://archive.debian.org/debian-non-US proposed-updates/non-US main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>

<toc-add-entry name="diskcontroller">Suporte não oficial para controladores de disco não detectados pelo instalador</toc-add-entry>

<p>Poucos sistemas com controladores RAID como o Adaptec-2400A não são
suportados pelo instalador padrão. Você ainda pode instalar o Debian 3.0
usando o a versão bf2.4 carregando previamente os módulos do controlador deste
<a href="https://people.debian.org/~blade/install/preload/">disco</a>.</p>
