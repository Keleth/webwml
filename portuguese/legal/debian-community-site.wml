#use wml::debian::template title="Domínio debian.community"
#use wml::debian::faqs
#use wml::debian::translation-check translation="92233062b10a767a6a4fc85a6e524d91dbe64868"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p style="display: block; border: solid; padding: 1em; background-color: #FFF29F;">
 <i class="fa fa-unlink fa-3x"></i>
 Se você chegou até esta página através de um link para <a
 href="https://debian.community">debian.community</a>, a página solicitada não
 existe mais. Leia para saber a razão.
</p>

<ul class="toc">
 <li><a href="#background">Antecedentes</a></li>
 <li><a href="#statements">Declarações anteriores</a></li>
 <li><a href="#faq">Perguntas frequentes</a></li>
</ul>

<h2><a id="background">Antecedentes</a></h2>

<p>
 Em julho de 2022 a Organização Mundial da Propriedade intelectual
 (World Intellectual Property Organization) determinou que o domínio
 <a href="https://debian.community/">debian.community</a> foi registrado de má
 fé e estava sendo utilizado para macular as marcas registradas do projeto
 Debian. Ela ordenou que o domínio fosse entregue ao projeto.
</p>

<h2><a id="statements">Declarações anteriores</a></h2>

<h3><a id="statement-debian">
 Projeto Debian: o assédio de Daniel Pocock
</a></h3>

<p>
 <i>Publicado primeiramente: <a href="$(HOME)/News/2021/20211117">17 de novembro
 de 2021</a></i>
</p>

<p>
O Debian está ciente de uma série de postagens públicas feitas sobre
Debian e sobre os membros da sua comunidade em uma série de sites web pelo
Sr. Daniel Pocock, que alega ser um Desenvolvedor Debian.
</p>

<p>
O Sr. Pocock não está associado ao Debian. Ele não é um desenvolvedor Debian,
nem um membro da comunidade Debian. Anteriormente ele foi um desenvolvedor
Debian, mas foi expulso do projeto há alguns anos por ter um comportamento
que era destrutivo para a reputação do Debian e para a própria comunidade.
Ele não é um membro do projeto Debian desde 2018. Ele também está banido de
participar da comunidade Debian de qualquer forma, incluindo através de
contribuições técnicas, participando de espaços on-line, ou participando de
conferências e/ou eventos. Ele não tem o direito ou legitimidade para
representar o Debian em qualquer condição, ou para se apresentar como um
desenvolvedor Debian ou membro da comunidade Debian.
</p>

<p>
Desde que foi expulso do projeto, o Sr. Pocock tem se envolvido em uma campanha
contínua e extensa de assédio e retaliação, fazendo uma série de postagens
on-line inflamatórias e difamatórias, particularmente em um site web que se
propõe a ser um site web Debian. O conteúdo dessas postagens envolve não apenas
o Debian, mas também vários de seus(suas) desenvolvedores(as) e voluntários(as).
Ele também continua se apresentando indevidamente como membro da comunidade
Debian em grande parte de sua comunicação e apresentações públicas. Por favor,
veja este artigo para uma lista dos canais oficiais de comunicação do Debian.
Uma ação legal esta sendo tomada contra, entre outras coisas, difamação,
falsidade maliciosa e assédio.
</p>

<p>
O Debian permanece unido como comunidade e contra o assédio. Temos um código de
conduta que guia nossas respostas para comportamento danoso em nossa
comunidade, e continuaremos a agir para proteger nossa comunidade e
voluntários(as). Por favor, não hesite em contatar a equipe Debian Community se
você tem receios ou precisa de ajuda. Enquanto isso, todos os direitos do
Debian e seus(suas) voluntários(as) estão preservados.
</p>

<h3><a id="statement-other">Declarações de outros projetos</a></h3>

<ul>
 <li>
  <a href="https://fsfe.org/about/legal/minutes/minutes-2019-10-12.en.pdf#page=17">
   Free Software Foundation Europe e.V.</a> (originalmente publicado em 12 de
   outubro de 2019)
 </li>
 <li>
  <a href="https://openlabs.cc/en/statement-we-have-been-a-target-of-disinformation-efforts-our-initial-reaction/">
   Open Labs</a> (originalmente publicado em 26 de maio de 2021)
 </li>
 <li>
  <a href="https://communityblog.fedoraproject.org/statement-on-we-make-fedora/">
   Fedora</a> (originalmente publicado em 31 de janeiro de 2022)
 </li>
</ul>

<h2><a id="faq">Perguntas Frequentes</a></h2>

<question>
 Por que o domínio foi transferido para o Debian?
</question>

<answer><p>
 O Debian apresentou uma reclamação junto a OMPI (WIPO) de que o domínio estava
 sendo utilizado para violação de má fé das marcas registradas do Debian. Em
 julho de 2022, o painel da OMPI concordou que a pessoa que fez registro
 anteriormente não tinha direitos ou interesse na marca registrada e estava
 usando-as de má fé, e transferiu o domínio para o projeto Debian.
</p></answer>

<question>
 Qual era a objeção do Debian ao conteúdo?
</question>

<answer><p>
 O conteúdo do site web  <a href="https://debian.community/">debian.community</a>
 maculava as marcas registradas do Debian por meio da associação destas com
 alegações infundadas e links para cultos, insinuações de escravidão e abuso de
 voluntários(as).
</p></answer>

<question>
 Quem estava por trás do site web anterior?
</question>

<answer><p>
 O detentor anterior do domínio era a Free Software Contributors Association,
 uma associação não registrada na Suíça. Em sua reclamação à OMPI, o Debian
 afirmou que a associação é um alter ego para Daniel Pocock.
</p></answer>

<question>
 Posso ler o julgamento da OMPI na íntegra?
</question>

<answer><p>
 Sim, ele está
 <a href="https://www.wipo.int/amc/en/domains/decisions/pdf/2022/d2022-1524.pdf">
 arquivado de forma pública</a>.
</p></answer>

<question>
 O painel descobriu que os artigos eram difamatórios?
</question>

<answer><p>
 O painel não foi autorizado a fazer qualquer forma constatação - está além da
 jurisdição do painel. O painel tinha apenas condições de descobrir que o
 detentor registrou e utilizou o domínio de má fé para macular as marcas
 registradas do Debian
</p></answer>

<question>
 O Debian irá tomar alguma outra ação?
</question>

<answer><p>
 Esta informação não pode ser divulgada publicamente. O projeto Debian
 continua monitorando a situação e consultando sua assessoria jurídica.
</p></answer>

<h2><a id="further-info">Mais informações<a/></h2>

<ul>
 <li>
  <a href="$(HOME)/intro/people">
   O Projeto Debian: as pessoas: quem somos e o que fazemos</a>
 </li>
 <li>
  <a href="$(HOME)/intro/philosophy">
   O Projeto Debian: nossa filosofia: por que fazemos e como fazemos</a>
 </li>
</ul>
