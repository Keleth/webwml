#use wml::debian::template title="Como ingressar no Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Estamos sempre procurando por
novos(as) colaboradores(as) e voluntários(as). Incentivamos fortemente a
<a href="$(HOME)/intro/diversity">participação de todas as pessoas</a>.
Requisitos: interesse em Software Livre e algum tempo livre.</p>
</aside>

<ul class="toc">
<li><a href="#reading">Leitura</a></li>
<li><a href="#contributing">Contribuindo</a></li>
<li><a href="#joining">Ingressando</a></li>
</ul>


<h2><a id="reading">Leitura</a></h2>

<p>
Se ainda não o fez, você deve ler as páginas linkadas na
<a href="$(HOME)">página inicial do Debian</a>. Isso ajudará você a entender
melhor quem somos e o que estamos tentando alcançar. Como um(a) potencial
contribuidor(a) Debian, por favor, preste atenção especial a estas duas páginas:
</p>

<ul>
  <li><a href="$(HOME)/social_contract#guidelines">Definição Debian de Software Livre</a></li>
  <li><a href="$(HOME)/social_contract">Contrato Social Debian</a></li>
</ul>

<p>
Muito da nossa comunicação acontece nas
<a href="$(HOME)/MailingLists/">listas de discussão</a> Debian. Se você quer
ter uma ideia sobre o funcionamento interno do projeto Debian, deve pelo
menos se inscrever nas listas
<a href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce</a>,
<a href="https://lists.debian.org/debian-news/">debian-news</a> e
<a href="https://lists.debian.org/debian-news-portuguese/">debian-news-portuguese</a>.
Todas têm um volume baixo de mensagens e registram o que está acontecendo na
comunidade. O
<a href="https://www.debian.org/News/weekly/">Debian Project News</a>, que
também é publicado na lista debian-news, resume discussões recentes nas
listas de discussão e blogs relacionados ao Debian e fornece os respectivos
links.

<p>
Como um(a) desenvolvedor(a) em potencial, você também deve se inscrever na
<a href="https://lists.debian.org/debian-mentors/">debian-mentors</a>. Aqui
você pode fazer perguntas sobre empacotamento e projetos de infraestrutura, bem
como outras questões relacionadas aos(às) desenvolvedores(as). Por favor
observe que esta lista se destina a novos(as) colaboradores(as), não a
usuários(as). Outras listas interessantes são
<a href="https://lists.debian.org/debian-devel/">debian-devel</a>
(tópicos de desenvolvimento técnico),
<a href="https://lists.debian.org/debian-devel-portuguese/">debian-devel-portuguese</a>
(tópicos de desenvolvimento técnico em português),
<a href="https://lists.debian.org/debian-project/">debian-project</a>
(discussões sobre questões não técnicas no projeto),
<a href="https://lists.debian.org/debian-release/">debian-release</a>
(coordenação de lançamentos do Debian ) e
<a href="https://lists.debian.org/debian-qa/">debian-qa</a> (controle de
qualidade).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> <a href="$(HOME)/MailingLists/subscribe">Inscrição em listas de discussão</a></button></p>

<p>
<strong>Dica:</strong> Se deseja reduzir o número de e-mails que recebe,
especialmente em listas de alto tráfego, oferecemos resumos de e-mail (digests)
em vez de mensagens individuais. Você também pode visitar os
<a href="https://lists.debian.org/">arquivos das listas de discussão</a> para
ler as mensagens de nossas listas em um navegador web.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Você não precisa ser um(a)
desenvolvedor(a) Debian oficial (DD) para contribuir. Em vez disso, um(a) DD
existente pode atuar como <a href="newmaint#Sponsor">padrinho/madrinha</a> e
ajudar a integrar seu trabalho ao projeto.</p>
</aside>

<h2><a id="contributing">Contribuindo</a></h2>

<p>
Está interessado(a) na manutenção de pacotes? Então por favor dê uma
olhada na nossa lista de
<a href="$(DEVEL)/wnpp/">pacotes que precisam de trabalho e futuros pacotes
(WNPP - Work-Needing and Prospective Packages)</a>.
Aqui você encontrará pacotes que precisam de um(a) (novo(a)) mantenedor(a).
Assumir um pacote abandonado é uma ótima maneira de começar como mantenedor(a)
Debian. Isso não apenas ajuda a nossa distribuição, mas também lhe dá a
oportunidade de aprender com o(a) mantenedor(a) anterior.

<p>
Aqui estão algumas outras idéias de como você pode contribuir para o Debian:
</p>

<ul>
  <li>Ajude a escrever nossa <a href="$(HOME)/doc/">documentação</a>.</li>
  <li>Ajude a manter o <a href="$(HOME)/devel/website/">site web do Debian</a>,
      produzindo conteúdo, editando ou traduzindo textos já existentes.</li>
  <li>Ingresse na <a href="$(HOME)/international/">equipe de tradução</a>.</li>
  <li>Melhore o Debian e ingresse na <a href="https://qa.debian.org/">equipe de
      controle de qualidade (QA - Quality Assurance)</a>.
</ul>

<p>
Claro, há muitas outras coisas que você pode fazer e estamos sempre procurando
pessoas para oferecer suporte jurídico ou se juntar a nossa
<a href = "https://wiki.debian.org/Teams/Publicity">equipe de publicidade do Debian</a>.
Tornar-se um(a) membro(a) de uma equipe do Debian é uma excelente maneira de
ganhar alguma experiência antes de iniciar o processo de
<a href="newmaint">novo(a) membro(a)</a>.
Também é um bom ponto de partida se você estiver procurando por um(a)
padrinho/madrinha de pacote. Então, encontre uma equipe e entre imediatamente!
</p>

<p style="text-align:center"><button type="button"><span class="fa fa-users fa-2x"></span> <a href="https://wiki.debian.org/Teams">Lista de equipes do Debian</a></button></p>

<h2><a id="joining">Ingressando</a></h2>

<p>
Então você tem contribuído para o projeto Debian por algum tempo e quer
ingressar no Debian de uma maneira mais oficial? Existem basicamente duas
opções:
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>A função de mantenedor(a) Debian
(DM) foi introduzido em 2007. Até então, a única função oficial era de
desenvolvedor(a) Debian (DD). Atualmente, existem dois processos independentes
para se candidatar a qualquer uma das funções.</p>
</aside>

<ul>
  <li><strong>Mantenedor(a) Debian (DM - Debian Maintainer):</strong> este é
  o primeiro passo – como DM você pode subir os seus próprios pacotes para o
  repositório do Debian (com algumas restrições). Ao contrário dos(as)
  mantenedores(as) apadrinhados(as)/amadrinhados(as), os(as) mantenedores(as)
  Debian podem manter pacotes sem um(a) padrinhho/madrinha. <br>Mais
  informações:
  <a href="https://wiki.debian.org/DebianMaintainer">wiki sobre mantenedores(as) Debian</a></li>

  <li><strong>Desenvolvedor(a) Debian (DD - Debian Developer ):</strong> esta é
  a tradicional função de participação completa no Debian. Um(a) DD pode participar das
  eleições no Debian. Desenvolvedores(as) Debian <em>uploading</em> podem subir
  qualquer pacote para o repositório. Antes de se tornar um(a) DD
  <em>uploading</em>, você deve ter uma trilha de registros de manutenção de
  pacotes por pelo menos seis meses (por exemplo, enviando pacotes como um(a)
  DM, trabalhando dentro de uma equipe, ou mantendo pacotes enviados por
  padrinhos/madrinhas. DDs <em>non-uploading</em> têm os mesmos direitos de
  empacotamento que os(as) mantenedores(as) Debian. Antes de se candidatar como
  um(a) DD <em>non-uploading</em>, você deve ter uma visibilidade e uma
  significante trilha de registros de contribuições dentro do projeto.
  <br>Mais informações: <a href="newmaint">Canto de novos(as) membros(as)</a></li>
</ul>

<p>
Apesar do fato de que muitos dos direitos e responsabilidades de um(a) DM e de
um(a) DD serem idênticos,  atualmente existem processos independentes para se
candidatar para cada função. Veja a
<a href="https://wiki.debian.org/DebianMaintainer">página wiki do(a)
mantenedor(a) Debian</a> para detalhes sobre como se tornar um(a) mantenedor(a)
Debian. E veja a página do <a href="newmaint">canto de novos(as) membros(as)</a>
para pesquisar como se candidatar para ter o status de desenvolvedor(a) oficial
Debian.</p>

<p>
Independentemente de qual função escolher para se candidatar, você deve
estar familiarizado(a) com os procedimentos do Debian. Este é o motivo pelo qual
recomendamos fortemente ler a
<a href="$(DOC)/debian-policy/">política Debian</a> assim como a
<a href="$(DOC)/developers-reference/">referência para desenvolvedores(as)</a>.
</p>

