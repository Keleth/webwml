#use wml::debian::template title="Errata do Debian-Installer"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="9a1725c7a7c2a16470f0814224c0b78cecb2e2fe"

<h1>Errata para <humanversion /></h1>

<p>
Esta é uma lista de problemas conhecidos na versão <humanversion /> do
instalador do Debian. Se você não ver seu problema listado aqui, por favor
nos envie (em inglês) um
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">relatório de
instalação</a> descrevendo o problema.
</p>

<dl class="gloss">
     <dt>Tema usado no instalador</dt>
     <dd>Ainda não temos a arte do Bookworm, e o instalador ainda está usando o
     tema do Bullseye.
     <br />
     <b>Estado:</b> completamente corrigido no RC 1 do Bookworm.</dd>
      
     <dt>Firmwares necessários para algumas placas de som</dt>
     <dd>Parece que há várias placas de som que requerem o carregamento de um
     firmware para poder emitir som
     (<a href="https://bugs.debian.org/992699">#992699)</a>
     <br />
     <b>Estado:</b> corrigido no Alpha 1 do Bookworm.</dd>

     <dt>O LVM criptografado pode falhar em sistemas com pouca memória</dt>
      <dd>É possível que sistemas com pouca memória (por exemplo, 1 GB)
      encontrem uma falha ao configurar o LVM criptografado: o cryptsetup pode
      acionar a interrupção por falta de memória ao formatar a partição LUKS
      (<a href="https:// bugs.debian.org/1028250">#1028250</a>,
      <a href="https://gitlab.com/cryptsetup/cryptsetup/-/issues/802">cryptsetup upstream</a>).
      <br />
      <b>Estado:</b> corrigido no RC 2 do Bookworm.</dd>

      
      <dt>Quebrado "Guiado - use o disco inteiro e configure o LVM" no particionamento no modo UEFI</dt>
      <dd>O uso desse tipo de particionamento guiado resulta em uma mensagem no
      prompt "Forçar a instalação do UEFI?" (mesmo se não houver outros sistemas
      operacionais), padronizado para "Não"
      (<a href="https://bugs.debian.org/1033913">#1033913</a>).
      Manter a resposta padrão provavelmente resultará em uma instalação sem
      inicialização se o Secure Boot estiver habilidado. Mudar para "Sim" deve
      evitar esse problema.
      <br />
      <b>Estado:</b> corrigido no RC 2 do Bookworm.</dd>
</dl>
