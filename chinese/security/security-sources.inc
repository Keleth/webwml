
<ul>
<li><a href="https://security-tracker.debian.org/">Debian 安全追踪网</a>
所有安全相关[CN:信息:][HKTW:資訊:]的主要来源，支持各种搜索选项</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">JSON 列表</a>
  包含 CVE 描述、软件包名、Debian 缺陷编号、已修复的软件包版本，不包含 DSA
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">DSA 列表</a>
  包含 DSA，包括日期、相关的 CVE 编号、已修复的软件包版本
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">DLA 列表</a>
  包含 DLA，包括日期、相关的 CVE 编号、已修复的软件包版本
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
DSA 公告</a>（Debian 安全警报）</li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
DLA 公告</a>（Debian LTS 的 Debian 安全警报）</li>

<li> DSA 的 <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa">RSS</a>
或者包含警报文本的更长版本的 <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa-long">RSS</a></li>

<li> DLA 的 <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla">RSS</a>
或者包含警报文本的更长版本的 <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla-long">RSS</a></li>

<li><a href="oval">Oval [CN:文件:][HKTW:檔案:]</a></li>

<li>查找 DSA（大写不可省略）<br>
例如：<tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>查找 DLA（-1 不可省略）<br>
例如：<tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>查找 CVE<br>
例如：<tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>
