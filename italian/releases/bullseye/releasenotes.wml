#use wml::debian::template title="Debian 11 &mdash; Note di rilascio" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="7727fd8734bae9f8383826260a368f991491f565" maintainer="Luca Monducci"


<p>Per scoprire cosa c'è nuovo in Debian 11, vedere le note di rilascio per
la propria architettura:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Note di rilascio'); :>
</ul>

<p>Le note di rilascio contengono le istruzioni per gli utenti che intendono
aggiornare i propri sistemi.</p>

<p>Se il browser è stato configurato con la lingua corretta, utilizzando
i precedenti collegamenti si dovrebbe arrivare automaticamente alla versione
HTML del documento tradotta nella propria lingua; si veda <a
href="$(HOME)/intro/cn">negoziazione dei contenuti</a>. Altrimenti scegliere
quello relativo all'architettura, alla lingua e al formato che interessa
dalla tabella sottostante.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architettura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Lingua</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
