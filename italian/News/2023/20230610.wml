<define-tag pagetitle>Rilasciata Debian 12 <q>bookworm</q> released</define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::translation-check translation="5fc831c6a77ce47c0c4e44da8a3b594b8280e3dc" maintainer="Giuseppe Sacco"
#use wml::debian::news

<p>Dopo 1 anno, 9 mesi e 28 giorni di sviluppo, il progetto Debian è orgoglioso di
presentare la sua nuova versione stabile 12 (nome in codice <q>bookworm</q>).</p>

<p><q>bookworm</q> sarà supportato per i prossimi 5 anni grazie al lavoro
combinato del <a href="https://security-team.debian.org/">gruppo per la sicurezza di Debian</a>
e del gruppo <a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>.</p>

<p>In accordo con la <a href="$(HOME)/vote/2022/vote_003">risoluzione generale del 2022 sul firmware non libero</a>, 
abbiamo introdotto una nuova area dell'archivio che rende possibile separare i firmware non liberi
dagli altri pacchetti non liberi:</p>
<ul>
<li>non-free-firmware</li>
</ul>

<p>
Molti dei pacchetti con firmware non libero sono stati spostati da <b>non-free</b>
a <b>non-free-firmware</b>. Questa separazione permette di costruire una varietà
di immagini ufficiali per l'installazione.
</p>

<p>Debian 12 <q>bookworm</q> viene distribuita con vari ambienti desktop quali:
</p>
<ul>
<li>Gnome 43,</li>
<li>KDE Plasma 5.27,</li>
<li>LXDE 11,</li>
<li>LXQt 1.2.0,</li>
<li>MATE 1.26,</li>
<li>Xfce 4.18</li>
</ul>

<p>Questo rilascio contiene oltre <b>11.089</b> nuovi pacchetti per un totale di <b>64.419</b>
pacchetti, inoltre più di <b>6.296</b> pacchetti sono stati rimossi in quanto <q>obsoleti</q>.
<b>43.254</b> sono quelli aggiornati in questo rilascio.

Lo spazio disco totale usato per <q>bookworm</q> è <b>365.016.420 kB (365 GB)</b> ed è
realizzato a partire da <b>1.341.564.204</b> linee di codice.</p>

<p><q>bookworm</q> ha più pagine di manuale tradotte che in passato, questo
grazie ai nostri traduttori che hanno reso disponibili le pagine-<b>man</b>
in più lingue, quali: ceco, danese, greco, finlandese, indonesiano, macedone,
norvegese (Bokmål), russo, serbo, svedese, ucraino e vietnamita.
Tutte le pagine di manuale di <b>systemd</b> sono ora disponibili anche in tedesco.</p>

<p>Il Debian Med Blend introduce un nuovo pacchetto: <b>shiny-server</b> che
semplifica le applicazioni web scientifiche che usano <b>R</b>. Siamo
riusciti nel nostro intento di fornire il supporto Continuous Integration
per i pacchetti del gruppo Debian Med. Installare i metapacchetti della
versione 3.8.x per Debian bookworm.</p>

<p>Il Debian Astro Blend continua a fornire una soluzione completa per
gli astronomi professionisti, entusiasti e obbisti, con aggiornamenti
su quasi tutte le versioni dei pacchetti del blend. <b>astap</b> e
<b>planetary-system-stacker</b> aiutano per l'image stacking e per
la riduzione astrometrica. <b> openvlbi</b>, il correlatore libero,
è adesso incluso.</p>

<p>Il supporto per il Secure Boot su ARM64 è stato reintrodotto: gli utenti
di hardware ARM64 con UEFI possono fare l'avvio con il Secure Boot abilitato
e trarre vantaggio sul fronte della sicurezza.</p>

<p>Debian 12 <q>bookworm</q> contiene molti pacchetti software aggiornati
(oltre il 67% di tutti i pacchetti del rilascio precedente), quali:
</p>

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS Server 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (default email server) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU Compiler Collection 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>The GNU C Library 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Linux kernel 6.1 series</li>
<li>LLVM/Clang toolchain 13.0.1, 14.0 (default), and 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>

<p>
Con quest'ampia selezione di pacchetti e col suo tradizionalmente ampio supporto
delle architetture, Debian mantiene ancora il suo scopo di essere
il <q>sistema operativo universale</q>. È adatto a molti e vari casi
d'uso: dalle postazioni da tavolo ai netbook, dai server di sviluppo
ai sistemi cluster, e per server di database, web e archiviazione.
Allo stesso tempo, ulteriori sforzi sul controllo della qualità, come
i test di installazione e aggiornamento automatici per tutti i pacchetti
nell'archivio Debian, assicurano che <q>bookworm</q> soddisfi
le più alte aspettative che gli utenti hanno per un rilascio stabile di Debian.
</p>

<p>
In tutto sono supportate ufficialmente da <q>bookworm</q> nove architetture:
</p>
<ul>
<li>32-bit PC (i386) e 64-bit PC (amd64),</li>
<li>64-bit ARM (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (EABI hard-float ABI, armhf),</li>
<li>little-endian MIPS (mipsel),</li>
<li>64-bit little-endian MIPS (mips64el),</li>
<li>64-bit little-endian PowerPC (ppc64el),</li>
<li>IBM System z (s390x)</li> 
</ul>

<p>
32-bit PC (i386) non supporta più nessun processore i586; il nuovo requisito
per il processore minimo è i686. <i>Se la propria macchina non soddisfa questo
requisito allora viene consigliato di rimanere con bullseye fino alla fine
del suo ciclo di supporto.</i>
</p>

<p>Il gruppo Debian Cloud pubblica <q>bookworm</q> per vari servizi di calcolo
in cloud:
</p>
<ul>
<li>Amazon EC2 (amd64 e arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (generico) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amd64, arm64, ppc64el)</li>
</ul>
<p>
L'immagine genericcloud dovrebbe essere in grado di funzionare in un qualsiasi
ambiente virtuale, e c'è anche una immagine nocloud utile per i test
del procedimento di costruzione.
</p>

<p>In maniera predefinita, i pacchetti GRUB <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#grub-os-prober">non
eseguiranno os-prober per altri sistemi operativi.</a></p>

<p>Tra i rilasci, il comitato tecnico ha deciso che Debian <q>bookworm</q>
dovrebbe <a href="https://wiki.debian.org/UsrMerge">supportare solo il formato di file system root merged-usr</a>,
ignorando il supporto per il formato non-merged-usr. Per i sistemi installati come buster o bullseye
non ci saranno modifiche al file system; ma i sistemi precedenti che utilizzano 
il vecchio formato verranno convertiti durante l'aggiornamento.</p>


<h3>Facciamo una prova?</h3>
<p>
Se si vuole solo provare Debian 12 <q>bookworm</q> senza installarlo,
si può usare una delle tante <a href="$(HOME)/CD/live/">immagini live</a>
che caricano ed eseguono il sistema opertivo completo in una modalità in sola
lettura che sfrutta la memoria del computer.
</p>

<p>
Queste immagini live sono fornite per le architetture <code>amd64</code>
e <code>i386</code> e sono disponibili in DVD, chiavi USB e netboot.
L'utente può scegliere tra vari ambienti desktop: GNOME, KDE Plasma,
LXDE, LXQt, MATE e Xfce.
Debian Live <q>bookworm</q> ha una immagine live standard per
provare Debian senza nessuna interfaccia grafica.
</p>

<p>
Nel caso il sistema operativo piacesse, nelle immagini live c'è l'opzione
per installare tutto sul disco fisso. L'immagine live contiene l'installatore
indipendente Calamares oltre a quello Debian. Maggiori informazioni sono
disponibili nelle <a href="$(HOME)/releases/bookworm/releasenotes">note
di rilascio</a> e nella sezione <a href="$(HOME)/CD/live/">immagini
live installabili</a> del sito web Debian.
</p>

<p>
Per installare direttamente Debian 12 <q>bookworm</q> sul proprio computer
si può scegliere tra molti supporti di installazione che possono essere
<a href="https://www.debian.org/download">scaricati</a> quali:
Blu-ray Disc, DVD, CD, USB stick oppure tramite connessione di rete.

Vedere la <a href="$(HOME)/releases/bookworm/installmanual">guida d'installazione</a> per maggiori dettagli.
</p>

# Translators: some text taken from: 

<p>
Debian può essere installata in 78 lingue, molte delle quali disponibili
sia nell'interfaccia testuale che in quella grafica.
</p>

<p>
Le immagini per l'installazione possono essere scaricate ora tramite
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (il metodo raccomandato),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, o
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; vedere
<a href="$(HOME)/CD/">Debian su CDs</a> per maggiori informazioni. <q>bookworm</q> sarà
inoltre disponibile presto su dischi DVD, CD-ROM e Blu-ray dai
numerosi <a href="$(HOME)/CD/vendors">rivenditori</a>.
</p>


<h3>Aggiornare Debian</h3>
<p>
Gli aggiornamenti a Debian 12 <q>bookworm</q> dalla versione precedente,
Debian 11 <q>bullseye</q>, sono gestiti automaticamente, per molte
configurazioni, dallo strumento per la gestione dei pacchetti APT.
</p>

<p>Prima di aggiornare il sistema, è fortemente consigliato fare una copia
completa di sicurezza , o almeno salvare i dati e le configurazioni che non
si può rischiare di perdere. Gli strumenti e il procedimento per l'aggiornamento
sono molto affidabili, ma un guasto hardware durante l'aggiornamento
potrebbe portare ad un sistema gravemente danneggiato.

Le cose principali da salvare sono il contenuto delle
directory /etc, /var/lib/dpkg, /var/lib/apt/extended_states e l'output di:

<code>$ dpkg --get-selections '*' # (le virgolette sono importanti)</code>

<p>Raccogliamo molto volentieri qualsiasi informazione dagli utenti a proposito
dell'aggiornamento da <q>bullseye</q> a <q>bookworm</q>. Per condividerla è
necessario aprire una segnalazione tramite il
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.en.html#upgrade-reports">sistema di
tracciamento dei bug Debian</a> usando il pacchetto <b>upgrade-reports</b> con
il proprio esito.
</p>

<p>
C'è stato molto lavoro sull'installatore Debian, che ha portato ad un
migliore supporto hardware e altre caratteristiche come la correzione
di problemi sul supporto grafico su UTM, o sul caricamento dei font in GRUB,
la rimozione delle lunghe attese alla fine dell'installazione, correzioni
al rilevamento dei sistemi avviabili da BIOS. Questa versione dell'installatore
Debian può abilitare non-free-firmware dove necessario.</p>

<p>
Il pacchetto <b>ntp</b> è stato sostituito da <b>ntpsec</b>, con il servizio
predefinito per l'orologio che adesso è <b>systemd-timesyncd</b>; c'è anche
il supporto per <b>chrony</b> e <b>openntpd</b>.
</p>

<p>Poiché <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split">il
firmware <b>non-free</b> è stato spostato in un componente dedicato dell'archivio</a>,
se si sta usando firmware non libero si consiglia di aggiungere <b>non-free-firmware</b>
al proprio sources-list di APT.</p>

<p>È consigliato rimnuovere bullseye-backports dai file source-list di APT
prima dell'aggiornamento; e aggiungere eventualmente <b>bookworm-backports</b>
alla fine.</p>

<p>
Per <q>bookworm</q>, gli aggiornamenti della sicurezza sono chiamati
<b>bookworm-security</b>; gli utenti dovrebbero adattare i file source-list
di APT in maniera opportuna durante l'aggiornamento.

Se la propria configurazione di APT usa il «pinning» o <code>APT::Default-Release</code>,
è molto probabile che dovranno essere fatti degli aggiustamenti per permettere
l'aggiornamento ai nuovi pacchetti del rilascio stabile. Valutare se
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#disable-apt-pinning">disabilitare
il pinning di APT</a>.
</p>


<p>L'aggiornamento di OpenLDAP 2.5 include <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#openldap-2.5">modifiche
incompatibili che possono richiedere l'intervento manuale</a>. A seconda dalla
configurazione il servizio <b>slapd</b> potrebbe rimanere fermo dopo
l'aggiornamento finché non si aggiusta la configurazione.</p>


<p>Il nuovo pacchetto <b>systemd-resolved</b> non viene installato automaticamente
durante l'aggiornamento perché <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#systemd-resolved">è
stato spostato in un pacchetto separato</a>. Se si usa il servizio systemd-resolved,
installare il nuovo pacchetto a mano dopo l'aggiornamento e tenere presente che dopo
l'aggiornamento la risoluzione DNS potrebbe non funzionare poiché questo
servizio non sarà presente sul sistema.</p>


<p>Ci sono alcuni <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#changes-to-system-logging">cambiamenti
nella raccolta dei log di sistema</a>; il pacchetto <b>rsyslog</b> non è più necessario
su molti sistemi, e non è installato in maniera predefinita. Gli utenti possono
utilizzare <b>journalctl</b> oppure l'<q>orologio ad alta precisione</q> che
adesso viene usato da <b>rsyslog</b>.
</p>


<p><a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#trouble">I
possibili problemi durante l'aggiornamento</a> includono cicli dovuti a
Conflicts o Pre-Depends che possono essere risolti togliendo alcuni pacchetti
o forzando la reinstallazione di altri. Altri problemi sono:
errori del tipo <q>Could not perform immediate configuration ...</q> per i quali
si dovrà mantenere <b>sia</b> la riga <q>bullseye</q> (appena rimossa)
sia quella <q>bookworm</q> (appena aggiunta) nei file source-list di APT, e
<q>File Conflicts</q> che potrebbe costringere a rimuovere pacchetti.
Come già detto, fare la copia di sicurezza del sistema è la chiave per un
aggiornamento che potrebbe presentare degli errori.</p>


<p>Ci sono alcuni pacchetti per i quali Debian non può promettere di 
fornire nessun «backport» per motivi di sicurezza. Vedere i
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#limited-security-support">limiti
del supporto della sicurezza</a>.</p>


<p>
Come sempre, i sistemi Debian possono essere aggiornati senza problemi, sul posto,
senza nessun blocco forzoso, ma è fortemente raccomandato di leggere
le <a href="$(HOME)/releases/bookworm/releasenotes">note di rilascio</a>
oltre che la <a href="$(HOME)/releases/bookworm/installmanual">guida
d'installazione</a> per eventuali problemi e per istruzioni dettagliate
su installazione e aggiornamento. Le note di rilascio verranno migliorate
e tradotte in altre lingue nelle settimane successive al rilascio.
</p>


<h2>Su Debian</h2>

<p>
Debian è un sistema opertivo libero, sviluppato da migliaia di
volontari in tutto il mondo, che collaborano tramite Internet.
I punti di forza del progetto Debian sono la sua base volontaria,
la sua dedizione al contratto sociale e al software libero, il suo
sforzo per fornire il miglior sistema operativo possibile. Questo
nuovo rilascio è un importante passo in questa direzione.
</p>


<h2>Contatti</h2>

<p>
Per maggiori informazioni visitare le pagine del sito web
Debian <a href="$(HOME)/">https://www.debian.org/</a> o mandare un email
a &lt;press@debian.org&gt;.
</p>

